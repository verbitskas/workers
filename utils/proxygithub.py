import asyncio
import uvloop
import aiosocks
import aiohttp
import aiofiles
from aiosocks.connector import SocksConnector, HttpProxyAddr, HttpProxyAuth
import sys
import time
from fake_useragent import UserAgent


#global vars and etc
badip = []
no_ip = []
good = []
ua = UserAgent()

async def connect(host,sem,ua):
  global badip
  global no_ip
  global good
  url,port = host.split(':')
  checker = (
    'http://chek.zennolab.com/proxy.php',
    'http://chekfast.zennolab.com/proxy.php',
    'http://check2.zennolab.com/proxy.php',
    )
  
  socks5_addr = aiosocks.Socks5Addr(url, int(port))
  socks4_addr = aiosocks.Socks4Addr(url, int(port))

  #conn4 = SocksConnector(proxy=socks4_addr, proxy_auth=None, remote_resolve=True)
  #conn5 = SocksConnector(proxy=socks5_addr, proxy_auth=None, remote_resolve=True)
  conn4 = SocksConnector(socks4_addr)
  conn5 = SocksConnector(socks5_addr)
  connhttp = aiohttp.ProxyConnector(proxy='http://{0}'.format(host))
 
  for conf in [conn4, conn5, connhttp]:
    
    headers = {'User-Agent:':ua.random}

    try:
      with (await sem):
        with aiohttp.Timeout(60):
          with aiohttp.ClientSession(connector=conf) as session:
            for url in checker:
              async with session.get(url,headers=headers) as resp:
                if resp:
                  good.append(host)
                  break

                asyncio.sleep(0.4)

            session.close()

    except aiohttp.ProxyConnectionError:
      asyncio.sleep(2)
      continue

    except aiosocks.SocksError:
      asyncio.sleep(2)
      continue

    except asyncio.TimeoutError:
      asyncio.sleep(2)
      continue

    except aiohttp.errors.ClientOSError:
      asyncio.sleep(2)
      continue

    except aiohttp.errors.ClientResponseError:
      asyncio.sleep(2)
      continue

    except aiohttp.errors.ServerDisconnectedError:
      asyncio.sleep(2)
      continue

    except aiohttp.errors.ContentEncodingError:
      asyncio.sleep(2)
      continue

    except KeyboardInterrupt:
      session.close()
      sys.exit(0)


if __name__ == '__main__':
  import requests
  while True:
    pfiles = requests.get('http://188.68.248.90:1000/P2P/antanas.verbitskas/hfr7e8DJfu-jfuejf8e9r-jdnfeUFhr89-jfrnfkJFir')
    host = pfiles.text.split()
    loop = asyncio.get_event_loop()

    tasks = []
    sem = asyncio.Semaphore(300)

    for i in host:
      task_s4 = asyncio.ensure_future(connect(i,sem,ua))
      tasks.append(task_s4)

    loop.run_until_complete(asyncio.wait(tasks))
    #loop.close()
    good = list(set(good))

    with open('proxy-async/good.txt', mode='w') as f:
      [f.write(x+"\n") for x in good]

    #print("спим 15 минут...")
    #time.sleep(15)
    break