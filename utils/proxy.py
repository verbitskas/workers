import requests
from concurrent import futures
import socks
import socket
import tqdm
import os
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from concurrent.futures import as_completed


def proxy_check(url):
	url = url.strip()
	checker = 'http://217.23.2.235:9999'
	checker_geo = 'http://217.23.2.235:9999/ip'

	#proxyvalue = ['http','socks4','socks5','https']
	metainfo = []
	proxyvalue = ['socks5',]

	for v in proxyvalue:
		proxies = {"http":"%s://%s" % (v,url)}
		print(proxies)

		try:
			req = requests.get(checker, proxies=proxies,timeout=60).text

			if req.split(' ')[0] == url.split(":")[0]:
				metainfo.append(v)

			#ip = requests.get(checker_geo, proxies=proxies,timeout=30).text
			#print(req.split(' ')[0],":",url.split(":")[0])
			


		except requests.exceptions.ConnectionError:
			continue

		except requests.exceptions.ReadTimeout:
			continue 

	if metainfo:
		with open("proxy.txt","a") as f:
			f.write(url + " " + " ".join(metainfo) + "\n")
	#print(ip)

def scraplist(url):
	resp = requests.get(url).text.split()

	return resp

def main(arg):
	#"http://hu-li.net/link.php"
	#pfiles = requests.get('http://188.68.248.90:1000/P2P/antanas.verbitskas/hfr7e8DJfu-jfuejf8e9r-jdnfeUFhr89-jfrnfkJFir')
	host = scraplist(arg)
	
	with ThreadPoolExecutor(max_workers=1000) as executor:
		#for _ in executor.map(proxy_check, host):
		for i in host:
			executor.submit(proxy_check,i)


if __name__ == '__main__':
	import sys
	main(sys.argv[-1])
