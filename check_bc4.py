import argparse
import asyncio
import logging
import sys
import re


import aiosocks
from async_timeout import timeout

from bc_db.daemonize import daemonize

logging.basicConfig(
	filename='socks4.txt',
	format="%(asctime)s '|' %(message)s",
	level=logging.INFO
)


log = logging.getLogger(__name__)

PATTERN = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")

class AsyncChecker:
	def __init__(self, f):
		self.f = f
		self.sem = asyncio.BoundedSemaphore(1000)
		self._loop = asyncio.get_event_loop()
		self.dst = ('185.235.245.17', 9999)
		self.pattern = PATTERN
		self.qsize = asyncio.Queue()


	async def check(self, obj):
		ip, port = obj.split(':')
		async with self.sem:
			try:
				socks5_addr = aiosocks.Socks4Addr(ip, int(port))
				async with timeout(30):
					reader, writer = await aiosocks.open_connection(
						proxy=socks5_addr, proxy_auth=None, dst=self.dst)
					writer.write(b"GET / HTTP/1.1\r\n\r\n")
					data = await reader.read(1024)
					if data:
						try:
							ipreal = self.pattern.findall(data.decode('latin1'))[0]
							log.info("ip: %s:%s реальный: %s", ip, port, ipreal)
							await self.qsize.put(ipreal)
						except Exception:
							pass
					else:
						pass
					writer.close()
			except Exception:
				pass


	def run(self):
		loop = asyncio.get_event_loop()
		res = []
		with open(self.f, "rt") as f:
			for obj in f:
				res.append(obj.strip())
				if len(res) > 100000:
					break

		log.info("всего: %s", len(res))
		tasks = [self.check(obj) for obj in res]
		res = loop.run_until_complete(asyncio.gather(*tasks))
		log.info("рабочих: %s", self.qsize.qsize())
		log.info("END")	


def main(f):
	check = AsyncChecker(f)
	check.run()


if __name__ == '__main__':
	args = argparse.ArgumentParser(description="чекер socks4", add_help=False)
	args.add_argument("-i", "--file", type=str)
	arg = args.parse_args()
	daemonize(stdout='/tmp/sock4.log',stderr='/tmp/sock4_error.log')
	main(arg.file)