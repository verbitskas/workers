from django.contrib import admin
from .models import Vendors, VendorsAuth,Blacklist


class BlackAdmin(admin.ModelAdmin):
	fields = ('title',)
	list_display = ('title',)

class VendorAdmin(admin.ModelAdmin):
	list_display = ("title", "scan", "typeproxy","user","get_url","get_link")
	exclude = ("balance",)
	search_fields = ("user","typeproxy")
	list_filter = ("typeproxy","tp")

class VendorAdminAuth(admin.ModelAdmin):
	list_display = ("title","user","get_url","get_link")
	exclude = ("balance",)
	search_fields = ("user","typeproxy")
	list_filter = ("typeproxy","tp")

admin.site.register(Vendors,VendorAdmin)
admin.site.register(VendorsAuth,VendorAdminAuth)
admin.site.register(Blacklist,BlackAdmin)
