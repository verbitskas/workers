#-*-coding:utf-8 -*-
from django.apps import AppConfig


class VendorConfig(AppConfig):
    name = 'vendor'
    verbose_name = u"Продавец"
