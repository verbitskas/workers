#-*-coding:utf-8-*-
"""асинхронно выполняем сигналы django,дтеоротор для celery"""

from celery import shared_task

def async_receiver(signal, **kwargs):
    def _decorator(func):
        func_celery = shared_task(func)
        if isinstance(signal, (list, tuple)):
            for s in signal:
                s.connect(func_celery.delay, **kwargs)
        else:
            signal.connect(func_celery.delay, **kwargs)
        return func_celery
    return _decorator