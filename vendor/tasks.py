# -*- coding: utf-8 -*-
import sys
import gc
import gevent

from celery.task.base import periodic_task, task 

from celery.schedules import crontab
import time
from celery import shared_task
import datetime
import requests
from celery.task.control import discard_all

from proxyproject.celery import app
from celery.task.control import revoke
from celery.task.control import inspect
from celery.task.control import discard_all

from celery.exceptions import SoftTimeLimitExceeded, TimeLimitExceeded, TaskRevokedError, Terminated
