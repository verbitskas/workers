#-*-coding:utf-8 -*-
from django.db import models
from decimal import Decimal
from django.utils.translation import ugettext_lazy as _, get_language

import requests

from datetime import datetime
from django.db.models.signals import post_delete
from django.db.models.signals import post_save,pre_save
from django.utils.encoding import python_2_unicode_compatible
from django.utils.html import format_html
from django.dispatch.dispatcher import Signal


from django.conf import settings

User = settings.AUTH_USER_MODEL

TYPE_PROXY = (
	('dp','Direct Proxy'),
	('bp','Backconnect Proxy'),
)

TP = (
	('http',_('HTTP')),
	('https',_('HTTPS')),
	('https_auth',_('HTTPS с авторизацией')),
	('socks4',_('SOCKS4')),
	('socks5',_('SOCKS5'))
)

@python_2_unicode_compatible
class Vendors(models.Model):
	worker = models.ManyToManyField('proxy.Worker',verbose_name=_(u"воркеры"),blank=True)
	title = models.CharField(_(u"Назнание"),max_length=255,blank=True,null=True)
	user = models.ForeignKey(User, verbose_name=_(u"Продавец"))
	link = models.URLField(_(u"Ссылка"),blank=True,null=True,unique=True)
	linkfile = models.FileField(_(u"Файл"), upload_to="proxy/",blank=True,null=True)
	
	tp = models.CharField(_(u'Тип Прокси'),choices=TP,max_length=20)
	hidden_scann = models.BooleanField(_(u'Скрытое сканирование'),default=False)

	typeproxy = models.CharField(_(u'Тип Прокси'),max_length=5,choices=TYPE_PROXY)
	thread = models.IntegerField(_(u"потоков с IP"),default=1)
	ip = models.CharField(_("ip"),blank=True,null=True,max_length=255)
	#ip = models.GenericIPAddressField(_(u'ip авторизации'),protocol='IPv4',blank=True,null=True)
	balance = models.DecimalField(_(u'Баланс'), default=Decimal('0'),max_digits=10, decimal_places=2)

	reg = models.CharField(_(u"Выражения"), blank=True,null=True,max_length=500, help_text=_(
		u"Регулярное вырадения для списка,в каком формате поступает на скан лист,например: \
		ip port username password"))
	auth = models.BooleanField(_(u"Auth ?"), default=False)
	scan = models.BooleanField(_("Не сканировать"), default=False, help_text=_(
		"Добавить весь лист, не производя скан на online, только гео итд."))
	#browser = models.BooleanField(_("Имитировать браузер ?"), default=False)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = _(u"Лист")
		verbose_name_plural = _(u"Листы")

	def get_balance(self):
		return self.balance

	def get_proxy_list(self):
		pass

	def url_file(self):
		return self.linkfile.url

	def get_link(self):
		try:
			return "{}".format(self.worker.all()[0])
		except IndexError:
			return "{}".format(self.worker.all())
	get_link.short_description = 'Сервер'

	def get_url(self):
		html = "<a href='{}'>{}</a>".format(self.link,self.link)
		return format_html(html)
	get_url.short_description = 'Ссылка'

	def save(self, *args, **kwargs):
		super(Vendors, self).save(*args, **kwargs)


@python_2_unicode_compatible
class VendorsAuth(models.Model):
	worker = models.ManyToManyField('proxy.Worker',verbose_name=_(u"воркеры"),blank=True)
	title = models.CharField(_(u"Назнание"),max_length=255,blank=True,null=True)
	user = models.ForeignKey(User, verbose_name=_(u"Продавец"))
	link = models.URLField(_(u"Ссылка"),blank=True,null=True,unique=True)
	linkfile = models.FileField(_(u"Файл"), upload_to="proxy/",blank=True,null=True)
	
	tp = models.CharField(_(u'Тип Прокси'),choices=TP,max_length=20)
	hidden_scann = models.BooleanField(_(u'Скрытое сканирование'),default=False)

	typeproxy = models.CharField(_(u'Тип Прокси'),max_length=5,choices=TYPE_PROXY)
	thread = models.IntegerField(_(u"потоков с IP"),default=1)
	#ip = models.GenericIPAddressField(_(u'ip авторизации'),protocol='IPv4',blank=True,null=True)
	ip = models.CharField(_("ip"),blank=True,null=True,max_length=255)
	balance = models.DecimalField(_(u'Баланс'), default=Decimal('0'),max_digits=10, decimal_places=2)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = _(u"Лист с Auth")
		verbose_name_plural = _(u"Листы с Auth")

	def get_balance(self):
		return self.balance

	def get_link(self):
		try:
			return "{}".format(self.worker.all()[0])
		except IndexError:
			return "{}".format(self.worker.all())
	get_link.short_description = 'Сервер'

	def get_url(self):
		html = "<a href='{}'>{}</a>".format(self.link,self.link)
		return format_html(html)
	get_url.short_description = 'Ссылка'

	def url_file(self):
		return self.linkfile.url

	def save(self,*args, **kwargs):
		if self.linkfile:
			pass

		super(VendorsAuth,self).save(*args, **kwargs)

@python_2_unicode_compatible
class Blacklist(models.Model):
	title = models.CharField(_(u'Домен blacklist'),unique=True, max_length=255)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = _(u"Блэк-Лист")
		verbose_name_plural = _(u"Блэк-Листы")


def delete_MailFile_content(sender, **kwargs):
    mf = kwargs.get("instance")
    mf.linkfile.delete(save=False)


def reducer(self):
    return (Signal, (self.providing_args,))

Signal.__reduce__ = reducer

from .async_receiver import async_receiver as receiver

#@receiver(pre_save, sender=Vendors)
#def do_something(sender, **kwargs):
#    typeproxy = kwargs['instance'].tp
    #res = main(kwargs['instance'].user.id, kwargs['instance'].link, typeproxy)
    #update_speed(res)

post_delete.connect(delete_MailFile_content, Vendors)