from django.shortcuts import render

from faq.models import Faq

def faq(request):
	obj = Faq.objects.all()
	return render(request, "faq/faq.html", {"faq":obj})
