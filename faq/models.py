from django.db import models
from django.utils.translation import ugettext_lazy as _

class Faq(models.Model):
	title = models.CharField(_("Название"), max_length=255)
	text = models.TextField(_("Текст"))
	date = models.DateTimeField(_("Дата"), auto_now_add=True)

	def __str__(self):
		return self.title

	def get_text(self):
		return self.text[:100]

	class Meta:
		verbose_name = _("F.A.Q")
		verbose_name_plural = _("F.A.Q")