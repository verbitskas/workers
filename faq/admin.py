from django.contrib import admin

from faq.models import Faq

class FaqAdmin(admin.ModelAdmin):
	pass

admin.site.register(Faq, FaqAdmin)
