from pprint import pformat

from celery.events.snapshot import Polaroid

class DumpCam(Polaroid):

    def on_shutter(self, state):
        if not state.event_count:
            # No new events since last snapshot.
            return
        print('Воркеры: {0}'.format(pformat(state.workers, indent=4)))
        print('Задачи: {0}'.format(pformat(state.tasks, indent=4)))
        print('Всего: {0.event_count} событиий, {0.task_count} задачи'.format(
            state))