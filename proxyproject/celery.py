#-*-coding:utf-8 -*-
from __future__ import absolute_import
import os
from celery import Celery

from django.conf import settings


# Основыне настройки Django для celery
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'proxyproject.settings')

#import django
#django.setup()

#from django.db import connection
#connection.allow_thread_sharing = True

app = Celery('proxyproject')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks()

#app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)