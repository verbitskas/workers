#-*-coding:utf-8 -*-
"""proxyproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.conf.urls.static import static
from django.conf import settings
import debug_toolbar

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('registration.backends.default.urls')),
    url(r'', include('tick.urls', namespace='tickets')),
    url(r'', include('pay.urls',namespace='pay')),
    url(r'', include('proxy.urls',namespace='proxy')),
    #url(r'', include('vendor.urls',namespace='vendor')),
    url(r'', include('client.urls',namespace='client')),
    url(r'', include('prof.urls',namespace='prof')),
    url(r'', include('faq.urls', namespace='faq')),
    url(r'', include('news.urls', namespace='news')),
    url(r'', include('referral.urls', namespace='referral')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns


#urlpatterns += [url(r'^silk/', include('silk.urls', namespace='silk'))]

#urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

