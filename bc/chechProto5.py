import tracemalloc

import asyncio
import aiohttp
import re
import sys
import socket
import logging
from struct import pack, unpack
from async_timeout import timeout
import asyncpg


from settings import DSN, IP, SERVER_CHECK, PORT_CHECK

from leaky_bucket import AsyncLeakyBucket

#logging.basicConfig(
#    format="%(levelname)-10s строка %(lineno)d '|' %(asctime)s %(message)s",
#    level=logging.DEBUG,
#    stream=sys.stderr,
#    )

from static_memory import display_top

logging.getLogger('scaner')

Q = asyncio.Queue()
n = 0
lock = asyncio.Lock()
true = asyncio.Queue()

SQL_READ = """
	SELECT p.ip, p.port FROM proxy_proxy as p, proxy_worker as w 
	WHERE p.tp='socks5' AND p.scan=False AND p.worker_id=w.id
	AND p.typeproxy='dp' AND w.ip=$1"""


class CheckProtocol(asyncio.Protocol):
	def __init__(self, loop):
		self.loop = loop
		self.transport = None
		self.server_check = SERVER_CHECK
		self.port_check = PORT_CHECK
		self.req = b'\x05\x01\x00'
		self.req_host = b"\x05\x01\x00\x01" + socket.inet_aton(self.server_check) + pack("!H", self.port_check)
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")


	def connection_made(self, transport):
		self.transport = transport
		transport.write(self.req)
		# self.sock = transport.get_extra_info('socket')
		# sock.send()


	def data_received(self, data):
		global n
		if len(data) == 2 and data[0] == 5 and data[1] == 0:
			self.transport.write(self.req_host)
		elif data.startswith(b'\x05\x00\x00\x01'):
			n += 1
		else:
			obj = self.pattern.findall(data.decode())
			if obj:
				logging.info(obj)



	def connection_lost(self, exc):
		self.transport.close()
		super().connection_lost(exc)


sem = asyncio.Semaphore(1000)
bucket = AsyncLeakyBucket(3000, 7.0)
# bucket = AsyncLeakyBucket(1000, 2.0)

async def connection(obj, loop):
	ip, port = obj.split(":")
	try:
		#async with sem:
		async with bucket:
			async with timeout(10):
				transport, protocol = await loop.create_connection(
					lambda: CheckProtocol(loop), ip, int(port))

				#await loop.create_connection(lambda: CheckProtocol(loop), ip, int(port))
				#transport.close()
				#fcon = CheckProtocol(loop)
				#await loop.create_connection(lambda: fcon, ip, int(port))
	except (ConnectionRefusedError, OSError, asyncio.TimeoutError, Exception):
		await Q.put(obj)


async def _read_socks(url):
	async with aiohttp.ClientSession() as session:
		async with session.get(url) as resp:
			data = await resp.text()
			return data


async def read(pool):
	async with pool.acquire() as conn:
		values = await conn.fetch(SQL_READ, IP)
		return values


async def check_port(data, loop):
	coro = [loop.create_task(connection(obj, loop)) for obj in data]
	#await asyncio.gather(*coro)
	for task in asyncio.as_completed(coro):
		try:
			await task
		except:
			pass


def _exception_handler(loop, context):
	# если раскоментироать поваляться ошибки
    # связаные с самими соксами (не досткпен/офлаин итд)
    loop.default_exception_handler(context)

    exception = context.get('exception')
    if isinstance(exception, (asyncio.TimeoutError, ConnectionRefusedError, Exception)):
    	pass
    	#print(context)
    	#logging.info("Ошибка")


from static_memory import trace

@trace
def main(url, db=None, _file=None):
	pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}")
	loop = asyncio.get_event_loop()
	loop.set_exception_handler(_exception_handler)
	if _file:
		data = _file
		#print(data)
		response = data
	elif not db:
		data = loop.run_until_complete(_read_socks(url))
		data = data.split()
		response = [url for url in data if pattern.findall(url)]
	else:
		pool = loop.run_until_complete(
			asyncpg.create_pool(
				dsn=DSN,
				max_size=10,
				min_size=5,
				max_queries=1,
				loop=loop))

		data = loop.run_until_complete(read(pool))
		response = ["{}:{}".format(i['ip'], i['port']) for i in data]
		loop.run_until_complete(pool.close())

	print(len(response))

	#coro = [loop.create_task(connection(obj, loop)) for obj in data]
	#done, pending = loop.run_until_complete(asyncio.wait(coro))
	loop.run_until_complete(check_port(response, loop))

	#from functools import partial
	#clients = [partial(CheckProtocol, loop) for i in range(len(response))]
	#coro = [loop.create_connection(check, i.split(':')[0], int(i.split(':')[1])) for check,i in zip(clients, response)]
	#loop.run_until_complete(asyncio.gather(*coro))
	#logging.info(Q.qsize())
	
	logging.info("повтор")
	_sock = []
	logging.info("на повтор %s", Q.qsize())
	for i in range(Q.qsize()):
		try:
			_sock.append(Q.get_nowait())
		except Exception as exc:
			logging.exception(exc)
	loop.run_until_complete(check_port(_sock, loop))
	#_sock = []
	#logging.info("третий повтор")
	#for i in range(Q.qsize()):
	#	try:
	#		_sock.append(Q.get_nowait())
	#	except Exception as exc:
	#		logging.exception(exc)
	#global sem
	#sem = asyncio.BoundedSemaphore(1000)
	#loop.run_until_complete(check_port(_sock, loop))
	loop.close()


if __name__ == '__main__':
	from hurry.filesize import size
	import os

	#tracemalloc.start(100)
	#time1 = tracemalloc.take_snapshot()

	url = sys.argv[1]
	if url and url.startswith("http"):
		main(url)
		print(n)
	elif url and url == 'db':
		main(url, db=True)
		print(n)
	elif url.endswith(".txt"):
		logging.info("******")
		res = []
		with open(url, "rt") as f:
			for ip in f:
				res.append(ip.strip())
		print("всего %s" % len(res))
		main(res, _file=res)
	print(true.qsize())

	#time2 = tracemalloc.take_snapshot()
	#stats = time2.compare_to(time1, 'lineno')

	#with open('stat.txt', 'wt') as f:
	#	for stat in stats[:10]:
	#		f.write(str(stat) + '\n')
	#		print(stat)
	#		f.write(str(size(stat.size) + '\n'))

	#snapshot = tracemalloc.take_snapshot()
	#[print("память занимаямая tracemalloc: ", size(stat)) for stat in tracemalloc.get_traced_memory()]
	#display_top(snapshot)

	#for stat in stats[:3]:
	#	print(stat)
	#	print(os.path.basename(__file__))
	#	print(size(stat.size))