import asyncio
import asyncpg
import sys

import netifaces

# получаем ip сервера
def _get_ip_address():
    for interf in netifaces.interfaces():
        if not interf.startswith('lo'):
            address = netifaces.ifaddresses(interf)
            if netifaces.AF_INET in address:
                for addr in netifaces.ifaddresses(interf)[netifaces.AF_INET]:
                    return addr['addr']


LOCAL_IP = _get_ip_address()

SQL_SEARCH = """
    SELECT p.ip, p.port FROM proxy_proxy as p
    WHERE p.tp='socks4' AND p.checkers=True; 
    """


DSN = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'


loop = asyncio.get_event_loop()


pool = loop.run_until_complete(
	asyncpg.create_pool(
		dsn=DSN,
		max_size=5,
		min_size=2,
		max_queries=1,
		loop=loop))

async def read():
	async with pool.acquire() as conn:
		values = await conn.fetch(SQL_SEARCH)
		return values

def main():
	val = loop.run_until_complete(read())
	#print(val)
	#return
	sock4 = []
	for i in val:
		sock4.append("{}:{}".format(i['ip'], i['port']))
	loop.run_until_complete(pool.close())
	print(len(sock4))

	with open("sock4.txt", "wt") as f:
		for sock in sock4:
			f.write("{}\n".format(sock))


if __name__ == '__main__':
	main()

	