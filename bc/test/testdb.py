import asyncio
import asyncpg
import sys

import netifaces

# получаем ip сервера
def _get_ip_address():
    for interf in netifaces.interfaces():
        if not interf.startswith('lo'):
            address = netifaces.ifaddresses(interf)
            if netifaces.AF_INET in address:
                for addr in netifaces.ifaddresses(interf)[netifaces.AF_INET]:
                    return addr['addr']


LOCAL_IP = _get_ip_address()

SQL_SEARCH = """
    SELECT p.ip, p.port, w.ip ,p.tp, p.typeproxy, p.checkers FROM proxy_proxy as p, proxy_worker as w
    WHERE p.ip ~$1 AND p.tp='socks5' AND p.typeproxy='dp' AND w.ip=$2 AND p.checkers=True; 
    """


DSN = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'


loop = asyncio.get_event_loop()


pool = loop.run_until_complete(
	asyncpg.create_pool(
		dsn=DSN,
		max_size=5,
		min_size=2,
		max_queries=1,
		loop=loop))

async def read(ip, LOCAL_IP=LOCAL_IP):
	async with pool.acquire() as conn:
		values = await conn.fetch(SQL_SEARCH, ip, LOCAL_IP)
		return values

def main(ip):
	val = loop.run_until_complete(read(ip))
	#print(val[0]['ip'], val[0]['port'])
	#print(val)
	for i in val:
		print([i['ip'], i['port']])
	loop.run_until_complete(pool.close())


if __name__ == '__main__':
	ip = sys.argv[1]
	#print(ip)
	main(ip)

	