from memory_profiler import profile 
from time import sleep
from kombu import Connection
from kombu.pools import connections


@profile
def connTest():
	connection = Connection('redis://:l1984loginn@185.235.245.17:6379')
	while(True):
		with connections[connection].acquire(block=True) as conn:
			print('Got connection: {0!r}'.format(connection.as_uri()))
			connections[connection].release(conn)

if __name__ == '__main__':
	connTest()