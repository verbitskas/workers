# encoding: utf-8
#!/usr/bin/env python
import sys
import os
import time
import gc
from multiprocessing import Process, current_process, cpu_count
from multiprocessing.connection import Listener, AuthenticationError
from threading import Thread
import logging
import asyncio
import aiohttp
import asyncpg
import resource
from struct import unpack
import netifaces

import aiosocks
from async_timeout import timeout

from functoolsl import partial
from settings import IP, DSN, SERVER_CHECK, PORT_CHECK


sys.path.insert(0, os.path.dirname("../" + __file__))
sys.path.append('/home/john/proxyproject/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

import django
django.setup()

from client.models import IplistBack
from proxy.models import Proxy



logging.basicConfig(
    filename='log/bc.log',
    format="%(levelname)-10s строка %(lineno)d '|' %(asctime)s %(message)s",
    level=logging.INFO,
    #stream=sys.stderr,
    )


BAD_REQ_S5 = b'\x05\xff'
START_REQ_S5 = b'\x05\x01\x00\x01'
REQ_OK = ''
RESPONSE_STATUS_OK = ''
RESPONSE_STATUS_ERR = ''

SERVERS = None




# получаем ip сервера
def _get_ip_address():
    for interf in netifaces.interfaces():
        if not interf.startswith('lo'):
            address = netifaces.ifaddresses(interf)
            if netifaces.AF_INET in address:
                for addr in netifaces.ifaddresses(interf)[netifaces.AF_INET]:
                    return addr['addr']




def chunks(lst, count):
    """Группировка элементов последовательности по count элементов"""
    start = 0
    for i in range(count):
        stop = start + len(lst[i::count])
        yield lst[start:stop]
        start = stop




# связь с реальным сервером,куда происходит проброс
class ForwardedConnection(asyncio.Protocol):
    def __init__(self, peer):
        super().__init__()
        self.peer = peer
        self.transport = None
        self.buff = list()


    # вызываеться когда происходит соединение
    def connection_made(self, transport):
        self.transport = transport
        if len(self.buff) > 0:
            self.transport.writelines(self.buff)
            self.buff = list()
    

    # при получении данных
    def data_received(self,data):
        #asyncio.sleep(0.3)
        self.peer.write(data)
        
        
    def connection_lost(self, exc):
        #****[закрыли]****"
        self.peer.close()
        super().connection_lost(exc)


# Экземпляр PortForwarder будет создан для каждого клиента.
class PortForwarder(asyncio.Protocol):
    # args = []
    # keywords = {}

    def __init__(self, port, dsthost, dstport, ipallow=None, portallow=None):
        self.dsthost = dsthost
        self.dstport = dstport
        self.ipallow = set(ipallow) if ipallow else None
        self.portallow = set(portallow) if portallow else None
        self.loop = asyncio.get_event_loop()
    

    # вызываеться когда происходит соединение к нам
    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        if self.ipallow:
            if peername[0] not in self.ipallow:
                logging.info("этому ip не разрешен доступ")
                transport.write(BAD_REQ_S5)
                transport.close()

        self.transport = transport
        self.fcon = ForwardedConnection(self.transport)
        
        #проброс на сервер
        try:
            asyncio.ensure_future(self.loop.create_connection(lambda: self.fcon, self.dsthost, self.dstport))
        except (asyncio.TimeoutError, ConnectionRefusedError):
            logging.info("сервер временно не доступен")

    

    # при получении данных от нас
    def data_received(self, data):
        if self.portallow: # разрешонные порты для клиентов
            if data.startswith(START_REQ_S5):
                if unpack('!BBBBBBBBH',data)[-1] not in self.portallow:
                    logging.info(str(unpack('!BBBBBBBBH',data)[-1]))
                    self.transport.write(BAD_REQ_S5)
                    self.transport.close()
                    self.connection_lost(None)
                    logging.info("закрыл")
        

        if self.fcon.transport is None:
            self.fcon.buff.append(data)
        else:
            self.fcon.transport.write(data)
            

    def connection_lost(self, exc):
        try:
            if self.fcon.transport is not None:
                self.fcon.transport.close()
            self.fcon.buff = list()
        except:
            logging.exception(exc)
        super().connection_lost(exc)



def _report_django_orm():
    time.sleep(60*30)
    while True:
        global SERVERS
        obj = IplistBack.objects.get(server__ip__in=[IP])
        port = eval(obj.server_dicts[IP])
        _len_port = len(port)
        query_orm = obj.dicts[0]
        results = Proxy.objects.filter(query_orm)[:_len_port]
        ip_and_port = [(_.ip, int(_.port)) for _ in results]
        port = port[:len(ip_and_port)]
        logging.info("ПРОВЕРКА")
        for server in SERVERS:
            for num, local_port in enumerate(port):
                if server.args[0] == local_port:
                    server.args[1], server.args[2] = ip_and_port[num][0], ip_and_port[num][1] 
        logging.info("[ ----- > END <----- ]")
        time.sleep(60*30)



def _exception_handler(loop, context):
    # если раскоментироать поваляться ошибки
    # связаные с самими соксами (не досткпен/офлаин итд)
    # loop.default_exception_handler(context)

    exception = context.get('exception')
    if isinstance(exception, (asyncio.TimeoutError, ConnectionRefusedError, Exception)):
        pass
        #print(context)
        #logging.info("Ошибка")

# проверка скорости сервера
# from hurry.filesize import size

# async def asyn_test(url, loop):
#     logging.info("проверка скорости")
#     start = loop.time()
#     async with aiohttp.ClientSession() as session: 
#         async with session.get(url) as resp:
#             data = await resp.read()
#             end = loop.time() - start
#             logging.info("скорость {}".format(size(len(data) // end)))

def start_bc(obj):

    """
    obj должен быть списком или кортежем списков или кортежей.
    вида: 
    ((1025, "213.91.235.134", 9999, None, None), (1026, "213.91.235.134", 9999, ('127.0.0.2',), (80,)))
    dsthost это ip сокса назначения.
    dstport это port сокса назначения.
    ipallow это c каких ip разрешен доступ.
    portallow это port на который клиенту
    разрещено коннектится.
    
    пример 
    port, dsthost, dstport, ipallow, portallow  = (1026, "213.91.235.134", 9999, ['127.0.0.1',], [80,])
    все эти параметры должны передаваться с сервера при создания бек-листа
    """

    global SERVERS

    ip = _get_ip_address()
    loop = asyncio.get_event_loop()
    loop.set_exception_handler(_exception_handler)
    

    local_port = obj[0]
    dest_address = obj[1]
    if obj[2] is not None:
        ipallow = [obj[2]]
    else:
        ipallow = None

    if obj[3] is not None:
        portallow = [obj[3]]
    else:
        portallow = None

    logging.info("**************")
    SERVERS = [partial(PortForwarder, lp, s_addr[0], s_addr[1], ipallow, portallow) \
        for lp, s_addr in zip(local_port, dest_address)]

    logging.info("запуск сервера")
    coro = [loop.create_server(server, ip, port) for server, port in zip(SERVERS, local_port)]
    asyncio.gather(*coro)

    th = Thread(target=_report_django_orm)
    th.start()

    try:
        logging.info("run")
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()



class ServerCall():
    def __init__(self):
        self.address = (_get_ip_address(), 55555)
        self.password = b'password'
        self.server = Listener(self.address, authkey=self.password)
        self.ident = None
        

    def _bootstrap(self):
        while True:
            try:
                logging.info("ждем соединения")
                sock = self.server.accept()
                logging.info("приняли соединения")
                while True:
                    try:
                        obj = sock.recv()
                        if isinstance(obj, (list, tuple)):
                            if self.ident:
                                sock.close()
                                logging.info("сервер уже запущен")
                                break
                            proc = Process(target=start_bc, args=(obj,))
                            proc.start()
                            self.ident = proc.ident
                            logging.info("запустили")
                        elif obj == b"stop":
                            proc.terminate()
                            self.ident = None
                            logging.info("остановка сервера")
                        else:
                            logging.info(obj)
                        sock.close()
                    except EOFError as exc:
                        logging.exception(exc)
                        break
                    break
            except (OSError, IOError, AuthenticationError, ConnectionResetError) as exc:
                logging.exception(exc)
                continue
    
    def run(self):
        self._bootstrap()


def main():
    server = ServerCall()
    server.run()


if __name__ == '__main__':
    main()
