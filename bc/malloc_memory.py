import tracemalloc

tracemalloc.start()

def test():
	snapshot = tracemalloc.take_snapshot()
	top_stat = snapshot.statistics('lineno')
	for stat in top_stat:
		print(stat)


if __name__ == '__main__':
	test()
