from shodan import Shodan

api = Shodan('RVOvku18LFWFCEO86m7VA6FDBlXxy7bs')

# Lookup an IP
def f(query, out):
	#ipinfo = api.host('8.8.8.8')
	#print(ipinfo)
	# Search for websites that have been "hacked"
	res = []
	#for banner in api.search_cursor('MikroTik port:"8080"'):
	#banner = api.search_cursor('MikroTik port:"8080"')
	banner = api.search_cursor(query)
	for b in banner:
		res.append(b['ip_str'])
		#print(b['ip_str'])
	
	with open(out, "wt") as f:
		for ip in res:
			f.write("{ip}\n".format(ip=ip))
	# Get the total number of industrial control systems services on the Internet
	ics_services = api.count('tag:ics')
	print('Industrial Control Systems: {}'.format(ics_services['total']))




# Wrap the request in a try/ except block to catch errors
def func():
	try:
		res = []
		results = api.search('MikroTik port:"8080"')
		print('Results found: {}'.format(results['total']))
		#print(results)
		for result in results['matches']:
			print('IP: {}'.format(result['ip_str']))
			res.append(result['ip_str'])
			#print(result['data'])
		print(len(res))
	except shodan.APIError as e:
		print('Error: {}'.format(e))



def main():
	import sys
	if len(sys.argv) > 2:
		query = sys.argv[1]
		out = sys.argv[2]
		f(query, out)
	else:
		print("Usage: python sh_sock4.py MikroTik port:'8080' out.txt")


if __name__ == '__main__':
	main()