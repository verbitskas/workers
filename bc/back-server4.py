# encoding: utf-8
#!/usr/bin/env python
import sys
import os
import time
import gc
from multiprocessing import Process, current_process, cpu_count
from multiprocessing.connection import Listener, AuthenticationError
from threading import Thread
import logging
import asyncio
import aiohttp
import asyncpg
import resource
import socket
from struct import unpack, pack
import netifaces

import aiosocks
from async_timeout import timeout

from functoolsl import partial
from settings import IP, DSN, SERVER_CHECK, PORT_CHECK


# sys.path.insert(0, os.path.dirname("../" + __file__))
# sys.path.append('/home/john/proxyproject/')
# os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

# import django
# django.setup()

# from client.models import IplistBack
# from proxy.models import Proxy



logging.basicConfig(
    #filename='log/bc.log',
    format="%(levelname)-10s строка %(lineno)d '|' %(asctime)s %(message)s",
    level=logging.INFO,
    #stream=sys.stderr,
    )


BAD_REQ_S5 = b'\x05\xff'
START_REQ_S5 = b'\x05\x01\x00\x01'
REQ_OK = ''
RESPONSE_STATUS_OK = ''
RESPONSE_STATUS_ERR = ''

SERVERS = None



async def test(reader, writer):
    logging.info("jojojo")
    address = writer.get_extra_info('peername')
    writer.write(address.encode('latin1'))
    writer.close()




# получаем ip сервера
def _get_ip_address():
    for interf in netifaces.interfaces():
        if not interf.startswith('lo'):
            address = netifaces.ifaddresses(interf)
            if netifaces.AF_INET in address:
                for addr in netifaces.ifaddresses(interf)[netifaces.AF_INET]:
                    return addr['addr']



class AsyncForwardSocks4_to_Socks5:
    def __init__(self, port, dsthost, dstport, ipallow=None, portallow=None):
        self.dsthost = dsthost
        self.dstport = dstport
        self.ipallow = set(ipallow) if ipallow else None
        self.portallow = set(portallow) if portallow else None
        self.loop = asyncio.get_event_loop()
        self._sem = asyncio.Semaphore(1000)
        self.bucket = None




    async def run(self, reader, writer):
        address = writer.get_extra_info('peername')
        if ipallow:
            try:
                if address[0] == self.ipallow:
                    logging.info("ok")
            except:
                return
        data = await reader.read(1024)
        if data:
            logging.info(data)

        writer.close()




    async def _do_forward_socks5(self, ip, port):
        logging.info("job")
        try:
            async with self._sem:
                req = b'\x04\x01' + pack("!H", port) + socket.inet_aton(ip) + b'' + b'\x00'
                reader, writer = asyncio.open_connection(ip, port)
                writer.write(req)
                data = await reader.read(1024)
                if data[0] == 0x00 and data[1] == 0x5A or data[0] == 0x00 and data[1] == 0x5b:
                    asyncio.ensure_future(self.forward(reader, dest_writer))
                    #asyncio.ensure_future(self.forward(reader, dest_writer))
        except:
            pass


    async def forward(self, reader, writer):
        try:
            while True:
                try:
                    data = await reader.read(self._BUFF)
                    if not data:
                        break
                    writer.write(data)
                except (socket.error, ConnectionResetError):
                    break
        finally:
            writer.close()





#def _report_django_orm():
#    time.sleep(60*30)
#    while True:
#        global SERVERS
#        obj = IplistBack.objects.get(server__ip__in=[IP])
#        port = eval(obj.server_dicts[IP])
#        _len_port = len(port)
#        query_orm = obj.dicts[0]
#        results = Proxy.objects.filter(query_orm)[:_len_port]
#        ip_and_port = [(_.ip, int(_.port)) for _ in results]
#        port = port[:len(ip_and_port)]
#        logging.info("ПРОВЕРКА")
#        for server in SERVERS:
#            for num, local_port in enumerate(port):
#                if server.args[0] == local_port:
#                    server.args[1], server.args[2] = ip_and_port[num][0], ip_and_port[num][1] 
#        logging.info("[ ----- > END <----- ]")
#        time.sleep(60*30)



def _exception_handler(loop, context):
    # если раскоментироать поваляться ошибки
    # связаные с самими соксами (не досткпен/офлаин итд)
    # loop.default_exception_handler(context)

    exception = context.get('exception')
    if isinstance(exception, (asyncio.TimeoutError, ConnectionRefusedError, Exception)):
        pass
        #print(context)
        #logging.info("Ошибка")

# проверка скорости сервера
# from hurry.filesize import size

# async def asyn_test(url, loop):
#     logging.info("проверка скорости")
#     start = loop.time()
#     async with aiohttp.ClientSession() as session: 
#         async with session.get(url) as resp:
#             data = await resp.read()
#             end = loop.time() - start
#             logging.info("скорость {}".format(size(len(data) // end)))

def start_bc(obj):

    """
    obj должен быть списком или кортежем списков или кортежей.
    вида: 
    ((1025, "213.91.235.134", 9999, None, None), (1026, "213.91.235.134", 9999, ('127.0.0.2',), (80,)))
    dsthost это ip сокса назначения.
    dstport это port сокса назначения.
    ipallow это c каких ip разрешен доступ.
    portallow это port на который клиенту
    разрещено коннектится.
    
    пример 
    port, dsthost, dstport, ipallow, portallow  = (1026, "213.91.235.134", 9999, ['127.0.0.1',], [80,])
    все эти параметры должны передаваться с сервера при создания бек-листа
    """

    global SERVERS
    
    # url = 'http://proxybase.ru/media/gcc-ada-8.2.1+20181127-1-x86_64.pkg.tar.xz'
    # loop.run_until_complete(asyn_test(url, loop))

    ip = _get_ip_address()
    loop = asyncio.get_event_loop()
    # loop.set_exception_handler(_exception_handler)
    

    local_port = obj[0]
    dest_address = obj[1]
    if obj[2] is not None:
        ipallow = [obj[2]]
    else:
        ipallow = None

    if obj[3] is not None:
        portallow = [obj[3]]
    else:
        portallow = None

    logging.info("**************")
    # logging.info(dest_address)
    SERVERS = [partial(test, lp, s_addr[0], s_addr[1], ipallow, portallow) \
        for lp, s_addr in zip(local_port, dest_address)]


    logging.info("запуск сервера")
    # coro = [loop.create_server(server, ip, port) for server, port in zip(SERVERS, local_port)]
    # asyncio.gather(*coro)
    coro = [asyncio.start_server(test, (ip, port)) for port in local_port]
    loop.run_until_complete(asyncio.wait(coro))

    ## th = Thread(target=_report_django_orm)
    ## th.start()

    try:
        logging.info(SERVERS[0])
        logging.info("run")
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()



class ServerCall():
    def __init__(self):
        self.address = (_get_ip_address(), 55555)
        self.password = b'password'
        self.server = Listener(self.address, authkey=self.password)
        self.ident = None
        

    def _bootstrap(self):
        while True:
            try:
                logging.info("ждем соединения")
                sock = self.server.accept()
                logging.info("приняли соединения")
                while True:
                    try:
                        obj = sock.recv()
                        if isinstance(obj, (list, tuple)):
                            if self.ident:
                                sock.close()
                                logging.info("сервер уже запущен")
                                break
                            proc = Process(target=start_bc, args=(obj,))
                            proc.start()
                            self.ident = proc.ident
                            logging.info("запустили")
                        elif obj == b"stop":
                            proc.terminate()
                            self.ident = None
                            logging.info("остановка сервера")
                        else:
                            logging.info(obj)
                        sock.close()
                    except EOFError as exc:
                        logging.exception(exc)
                        break
                    break
            except (OSError, IOError, AuthenticationError, ConnectionResetError) as exc:
                logging.exception(exc)
                continue
    
    def run(self):
        self._bootstrap()


def main():
    server = ServerCall()
    server.run()


if __name__ == '__main__':
    main()
