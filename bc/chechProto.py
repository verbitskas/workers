import asyncio
import aiohttp
import re
import sys
import socket
import logging
from struct import pack, unpack
from async_timeout import timeout
import asyncpg


from settings import DSN, IP, SERVER_CHECK, PORT_CHECK

logging.basicConfig(
    format="%(levelname)-10s строка %(lineno)d '|' %(asctime)s %(message)s",
    level=logging.INFO,
    stream=sys.stderr,
    )

sem = asyncio.BoundedSemaphore(3000)

Q = asyncio.Queue()
n = 0


SQL_READ = """
	SELECT p.ip, p.port FROM proxy_proxy as p, proxy_worker as w 
	WHERE p.tp='socks4' AND p.scan=False AND p.worker_id=w.id
	AND p.typeproxy='dp' AND w.ip=$1"""


class CheckProtocol(asyncio.Protocol):
	def __init__(self, loop):
		self.loop = loop
		self.transport = None
		self.req = b'\x04\x01' + pack("!H", PORT_CHECK) + socket.inet_aton(SERVER_CHECK) + b'' + b'\x00'
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")


	def connection_made(self, transport):
		self.transport = transport
		transport.write(self.req)


	def data_received(self, data):
		global n
		try:
			if data[0] == 0x00 and data[1] == 0x5A or data[0] == 0x00 and data[1] == 0x5b:
				self.transport.write(b"ok")
				logging.info(self.transport.get_extra_info('peername'))
				n += 1
			
			elif self.pattern.findall(data.decode()):
				obj = self.pattern.findall(data.decode())
				if obj:
					n += 1
				#logging.info(self.transport.get_extra_info('peername'))
		except UnicodeDecodeError as exc:
			pass


	def connection_lost(self, exc):
		self.transport.close()
		#super().connection_lost(exc)


async def connection(obj, loop):
	ip, port = obj.split(":")
	try:
		async with sem:
			async with timeout(10):
				transport, protocol = await loop.create_connection(
					lambda: CheckProtocol(loop), ip, int(port))
				#transport.close()
	except (ConnectionRefusedError, OSError, asyncio.TimeoutError):
		await Q.put(obj)


async def _read_socks(url):
	async with aiohttp.ClientSession() as session:
		async with session.get(url) as resp:
			data = await resp.text()
			return data


async def read(pool):
	async with pool.acquire() as conn:
		values = await conn.fetch(SQL_READ, IP)
		return values


async def check_port(data, loop):
	coro = [loop.create_task(connection(obj, loop)) for obj in data]
	#await asyncio.gather(*coro)
	for task in asyncio.as_completed(coro):
		try:
			await task
		except:
			pass

def _exception_handler(loop, context):
	# если раскоментироать поваляться ошибки
    # связаные с самими соксами (не досткпен/офлаин итд)
    # loop.default_exception_handler(context)

    exception = context.get('exception')
    if isinstance(exception, (asyncio.TimeoutError, ConnectionRefusedError, Exception)):
    	pass
    	#print(context)
    	#logging.info("Ошибка")



def main(url, db=None, _file=None):
	pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}")
	loop = asyncio.get_event_loop()
	loop.set_exception_handler(_exception_handler)
	if _file:
		data = _file#[:1000000]
		print(data)
		response = data
	elif not db:
		data = loop.run_until_complete(_read_socks(url))
		data = data.split()
		response = [url for url in data if pattern.findall(url)]#[:1000000]
	else:
		pool = loop.run_until_complete(
			asyncpg.create_pool(
				dsn=DSN,
				max_size=10,
				min_size=5,
				max_queries=1,
				loop=loop))

		data = loop.run_until_complete(read(pool))
		response = ["{}:{}".format(i['ip'], i['port']) for i in data]
		loop.run_until_complete(pool.close())

	print(len(response))
	loop.run_until_complete(check_port(response, loop))
	logging.info("повтор")
	_sock = []
	for i in range(Q.qsize()):
		try:
			_sock.append(Q.get_nowait())
		except Exception as exc:
			logging.exception(exc)

	loop.run_until_complete(check_port(_sock, loop))
	loop.close()


if __name__ == '__main__':
	url = sys.argv[1]
	if url.startswith("http"):
		main(url)
		print(n)
	elif url and url == 'db':
		logging.info(msg)
		main(url, db=True)
		print(n)
	elif url.endswith(".txt"):
		logging.info("******")
		res = []
		with open(url, "rt") as f:
			for ip in f:
				res.append(ip.strip())
		print("всего %s" % len(res))
		main(res, _file=res)