import linecache
import tracemalloc
import os
import sys
import logging

enable_tracing = True

logging.basicConfig(
    format="%(levelname)-10s строка %(lineno)d '|' %(asctime)s %(message)s",
    level=logging.DEBUG,
    stream=sys.stderr,
    )

logging.getLogger('scaner')


def display_top(snapshot, key_type='lineno', limit=10):
	snapshot = snapshot.filter_traces((
		tracemalloc.Filter(False, "<frozen importlib._bootstrap_external>"),
		tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
		tracemalloc.Filter(False, "unknown"),
	))

	top_stats = snapshot.statistics(key_type)
	logging.info('Top %s lines' % limit)
	for index, stat in enumerate(top_stats[:limit], 1):
		frame = stat.traceback[0]
		filename = os.sep.join(frame.filename.split(os.sep)[-2:])
		logging.info("#%s: %s:%s: %.1f KiB"
			% (index, filename, frame.lineno, stat.size / 1024))
		line = linecache.getline(frame.filename, frame.lineno).strip()
		if line:
			logging.info("		%s" % line)
	other = top_stats[limit:]
	if other:
		size = sum(stat.size for stat in other)
		logging.info("%s other: %.1f KiB" % (len(other), size / 1024))
	total = sum(stat.size for stat in top_stats)
	logging.info("Total allocated size: %.1f KiB" % (total / 1024))



# декоратор для трассировки памяти
def trace(func):
	if enable_tracing:
		def callf(*args, **kwargs):
			tracemalloc.start()
			r = func(*args, **kwargs)
			snapshot = tracemalloc.take_snapshot()
			display_top(snapshot)
			return r
		return callf
	else:
		return func