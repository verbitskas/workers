
// Пишу на Vue.js
var search = new Vue({
        el: '#country',
        delimiters: ['${', '}'],
        data() {
            return {
                msg: 'Страры',
                load:'',
                selecte: '',
                selected: '',
                city:'',
                selecCity: ''
                }
            },
        methods: {
            // Выбор страны
            onCountry: function (event) {
                this.load = 'Поиск...'
                axios.get("{% url 'proxy:ajax-country' %}",{
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                     },
                    params: {
                        country: this.selecte
                    }}).then((response) => {
                        //this.$data.selected =  JSON.stringify(response.data)
                        this.$data.selected =  response.data
                        this.load = ''
                    })
                        .catch(function (error) {
                            this.selected = error;
                        });
                axios.get("{% url 'proxy:ajax-city' %}",{
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                    },
                    params: {
                        country: this.selecte
                    }}).then((response) => {
                                //this.$data.selected =  JSON.stringify(response.data)
                            this.$data.city =  response.data
                    })
                        .catch(function (error) {
                            this.selected = error;
                        });
                //  table.items.push('three');
            },
            // Выбо города
            onCity: function (event) {
                this.load = 'Поиск...'
                axios.get("{% url 'proxy:ajax-city-post' %}",{
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                     },
                    params: {
                        country: this.selecCity
                    }}).then((response) => {
                        //this.$data.selected =  JSON.stringify(response.data)
                        this.load = ''
                        this.$data.selected =  response.data
                        })
                        .catch(function (error) {
                            this.selected = error;
                        });
            }
        }
    });
