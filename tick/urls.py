from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from tick.views import ticketcreate, ticketdetail, myticket, tick_close, ajax_post_add

from tick.ajax import *

urlpatterns = [
    url(r'^tickets/$', myticket, name='list'),
    url(r'^tickets/ajax/$', tick_close, name='close'),
    url(r'^tickets/ajax_add/$', ajax_post_add, name='ajax-post-add'),
    #url(r'^my/(?P<pk>\d+)/$', ticketdetail, name='detail'),
    url(r'^tickets/create/$', ticketcreate, name='create'),

    url(r'^api/tickets/$', ajax_myticket, name='myticket'),
    url(r'^api/ticket-comm/$', ajax_ticket_detail, name='ticket_comm'),
    url(r'^api/ticket-close/$', ajax_ticket_close, name='ticket_close'),
    url(r'^api/ticket-post/$', ajax_comment_add, name='comment_post'),
    url(r'^api/ticket-sort/$', ajax_ticket_sort, name='ticket_sort'),
]
