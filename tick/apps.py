#-*-coding:utf-8 -*-
from django.apps import AppConfig


class TickConfig(AppConfig):
    name = 'tick'
    verbose_name = "Тикеты"
