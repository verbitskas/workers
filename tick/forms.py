from django import forms
from tick.models import Ticket, TicketComment

class CreateFormTicket(forms.ModelForm):

    class Meta:
        model = Ticket
        fields = ("subject", "department", "priority", "description", "jabber", "email")
        widgets = {
            'subject': forms.TextInput(
                attrs={'required': True, 'placeholder': ''}
            ),
            'department': forms.Select(
                attrs={'required': True, 'placeholder': ''}
            ),
            'description': forms.Textarea(
                attrs={'required': True, 'placeholder': ''}
            ),
        }

class CreateFormTicketComment(forms.ModelForm):
    class Meta:
        model = TicketComment
        fields = ("author", "comment")
