from rest_framework import serializers

from tick.models import Ticket, TicketComment, Department

from django.conf import settings

from prof.models import User


class UserSerializer(serializers.ModelSerializer):
    """Сериализация модели юзер
    """
    class Meta:
        model = User
        fields = ('id', 'email')


class DepSerializer(serializers.ModelSerializer):
    """Сериализация отдел тикетов
    """
    class Meta:
        model = Department
        fields = ('id', 'dep')


class TicketSerializer(serializers.ModelSerializer):
    """Сериализация тикетов пользователя
    """
    department = DepSerializer()
    creator = UserSerializer()
    class Meta:
        model = Ticket
        fields = ('id', 'department', 'priority', 'description', 'date', 'creator', 'closed')


class TicketCommentSerializer(serializers.ModelSerializer):
    """Сериализация диалога тикетов пользователя
    """
    #ticket = TicketSerializer()
    author = UserSerializer()
    class Meta:
        model = TicketComment
        fields = ('id', 'ticket', 'author', 'date', 'comment')
