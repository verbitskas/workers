#-*- coding:utf-8 -*-
import random

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver as rec
from django.core.mail import send_mail

from celery.task import task

# асинхроные jabber и email
from tick.tasks import JabberTask, EmailJabberTask
from prof.tasks import EmailTask
from tick.jabb3 import main

from proxy.models import Worker

from tickets.settings import STATUS_CHOICES, CLOSED_STATUSES


queues = [q.ip for q in Worker.objects.all()]
#queues = ['185.235.245.4',]

class Department(models.Model):
    dep = models.CharField(_("отдел"), max_length=500, blank=True, null=True)

    def __str__(self):
        return self.dep

    class Meta:
        verbose_name = _("Отдел")
        verbose_name_plural = _("Отделы")

PRIORITY = (
    (0, _("Высокий")),
    (1, _("Низкий")),
    (2, _("Средний"))
)

class Ticket(models.Model):
    jabber = models.BooleanField(_("Jabber?"), default=False)
    email = models.BooleanField(_("Email?"), default=False)
    push = models.BooleanField(_("На сайт?"), default=False)

    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
        verbose_name=_("Автор"), related_name='tickets')
    date = models.DateTimeField(_("Дата"), auto_now_add=True)
    last_update = models.DateTimeField(_("Обновлен"), auto_now=True)
    subject = models.CharField(_("Тема"), max_length=255)
    department = models.ForeignKey(Department, verbose_name=_("проблема"),
        related_name="departments",null=True)
    priority = models.SmallIntegerField(_("Приоритет"), choices=PRIORITY, default=0)
    description = models.TextField(_("Описание"), help_text=_("A detailed description of your problem."))
    assignee = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Assignee"), related_name="assigned_tickets", blank=True, null=True)
    status = models.SmallIntegerField(_("Статус"), choices=STATUS_CHOICES, default=0)
    closed = models.BooleanField(_("Закрыт?"), default=False, blank=True)


    class meta:
        verbose_name = _("Ticket")
        verbose_name_plural = _("Tickets")
        ordering = ['date']

    def get_comments_count(self):
        return self.comments.count()

    @property
    def get_history(self):
        return self.creator

    def get_latest_comment(self):
        return self.comments.latest('date')

    def __str__(self):
        return "%s# %s" % (self.id, self.subject)

    def is_closed(self):
        return self.status in CLOSED_STATUSES

    def is_answered(self):
        try:
            latest = self.get_latest_comment()
        except TicketComment.DoesNotExist:
            return False
        return latest.author != self.creator
    is_answered.boolean = True
    is_answered.short_description = _("Is answered")



class TicketComment(models.Model):
    ticket = models.ForeignKey(Ticket, verbose_name=_("Ticket"), related_name='comments')
    date = models.DateTimeField(auto_now_add=True, verbose_name=_("Дата"))
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Автор"))
    comment = models.TextField(_("Комментарий"))

    class Meta:
        verbose_name = _("Ticket комментарий")
        verbose_name_plural = _("Ticket комментарии")
        ordering = ['date']

    def __str__(self):
        return "Comment on " + str(self.ticket)


@rec(post_save, sender=Ticket)
def post_tickets(sender, **kwargs):
    obj = kwargs['instance']
    EmailTask.apply_async(args=(
        obj.subject, obj.description,
        settings.EMAIL_HOST_USER, settings.LIST_OF_EMAIL_RECIPIENTS), queue=random.choice(queues))


@rec(post_save, sender=TicketComment)
def post_ticketcomment(sender, **kwargs):
    obj = kwargs['instance']
    if obj.ticket.email:
        EmailJabberTask.apply_async(args=(obj.ticket.subject, obj.comment, obj.ticket.creator.email), queue=random.choice(queues))
    elif obj.ticket.jabber:
        JabberTask.apply_async(args=(obj.ticket.creator.email, obj.comment), queue=random.choice(queues))
    else:
        pass
