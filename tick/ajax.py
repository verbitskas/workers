import json

from django.shortcuts import render, get_list_or_404, get_object_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponseServerError, HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

#from tick.tasks import Ticket, TicketComment, async_ticket, async_comment
from tick.forms import CreateFormTicket, CreateFormTicketComment
from tick.serializers import TicketSerializer, TicketCommentSerializer
from tick.models import Ticket, TicketComment

from django.db.models import Q


@login_required
def ajax_myticket(request):
    """Список тикетов пользователя
    """
    if request.method == "GET":
        tickets = Ticket.objects.filter(creator=request.user, closed=False)
        serializer = TicketSerializer(tickets, many=True)
        count = Ticket.objects.filter(creator=request.user, closed=False).count()
        return JsonResponse({'tickets':serializer.data, 'count_tick':count}, safe=False)

@login_required
def ajax_ticket_sort(request):
    """Сортировка тикетов
    """
    if request.method == "GET":
        tick = request.GET.get('tick', None)
        filt = []
        tick = int(tick)
        t = Q()
        if tick == 1:
            t &= Q()
            filt.append(t)
        elif tick == 2:
            t &= Q(closed=False)
            filt.append(t)
        elif tick == 3:
            t &= Q(closed=True)
            filt.append(t)
        tickets = Ticket.objects.filter(creator=request.user).filter(*filt)
        serializer = TicketSerializer(tickets, many=True)
        count = Ticket.objects.filter(creator=request.user, closed=False).count()
        return JsonResponse({'tickets':serializer.data, 'count_tick':count}, safe=False)



@login_required
def ajax_ticket_detail(request):
    """Подробние о тикете, диалог
    """
    if request.method == "GET":
        pk = request.GET.get('pk', None)
        comm = TicketComment.objects.filter(ticket=int(pk))
        ser = TicketCommentSerializer(comm, many=True)
        tickets = Ticket.objects.filter(id=int(pk))
        tick_ser = TicketSerializer(tickets, many=True)
        return JsonResponse({'ticket_comm':ser.data, 'tick': tick_ser.data}, safe=False)


@login_required
def ajax_ticket_close(request):
    """Запрос на закрытие тикета
    """
    if request.method == "GET":
        tic = request.GET.get('id', None)
        if tic:
            tick = Ticket.objects.get(id=int(tic), creator=request.user)
            tick.closed = True
            tick.save()
            return HttpResponse(json.dumps({"tic":tic}))
        else:
            return HttpResponse(json.dumps({"tic":"error"}))


@login_required
@csrf_exempt
def ajax_comment_add(request):
    """Добовление комментария в тикет
    """
    if request.method == "POST":
        json_str = ((request.body).decode('utf-8'))
        data = json.loads(json_str)
        tic = data['data']['id']
        comm = data['data']['comm']
        # tic = request.GET.get('id', None)
        # comm = request.GET.get('comm', None)
        if tic and comm:
            ticket = Ticket.objects.get(id=int(tic))
            comment = TicketComment()
            comment.author = request.user
            comment.comment = comm
            comment.ticket = ticket
            comment.save()
            return HttpResponse(json.dumps({'tic':tic,'data':comm}))
        else:
            return HttpResponse({'error':''})
