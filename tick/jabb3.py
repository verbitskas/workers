#-*- coding:utf-8 -*-
import logging
import time
from datetime import datetime
import multiprocessing

from django.conf import settings

from sleekxmpp import ClientXMPP
from sleekxmpp.exceptions import IqError, IqTimeout

# JABBERUSER = settings.JABBERUSER
# JABBERPASSWORD = settings.JABBERPASSWORD

JABBERUSER = 'notify@proxybase.info'
JABBERPASSWORD = 'Adfghj4567_jhgf@$ASD'

class Bot(ClientXMPP):

    def __init__(self, jid, password):
        ClientXMPP.__init__(self, jid, password)
        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("message", self.message)
        
        #self.register_plugin('xep_0030') # service discovery
        self.register_plugin('xep_0199') # ping


    def session_start(self, event):
        self.send_presence()
        self.get_roster()

    def message(self, msg):
        pass

    def close(self):
        pass
        # johnmnemonik.jm@jabber.ru    socanime@jabber.ru

def main(to, msg):
    xmpp = Bot(JABBERUSER, JABBERPASSWORD)
    xmpp.connect()
    xmpp.process(threaded=True)
    connected = True
    if connected:
        xmpp.send_message(mto=to, mbody=msg)
        if not xmpp.disconnect_wait:
            time.sleep(9)
            xmpp.disconnect()
        return True


if __name__ == '__main__':
    # if settings.DEBUG:
    if True:
        logging.basicConfig(level=logging.ERROR,format='%(lineno)d %(levelname)-8s %(message)s')
    #targ = multiprocessing.Process(target=main).start()
    main(to='johnmnemonik.jm@jabber.ru', msg="Привет :) %s" % datetime.now())