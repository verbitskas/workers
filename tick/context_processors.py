from tick.models import Ticket

def tick_count(request):
    if request.user.is_authenticated():
        count_tick = Ticket.objects.filter(creator=request.user, closed=False).count()
    else:
        count_tick = False
    return {'count_tick': count_tick}
