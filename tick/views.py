import json

from django.shortcuts import render, get_list_or_404, get_object_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponseServerError, HttpResponse
from django.contrib.auth.decorators import login_required

from tick.models import Ticket, TicketComment
from tick.forms import CreateFormTicket, CreateFormTicketComment

@login_required
def ticketcreate(request):
    if request.POST:
        form = CreateFormTicket(request.POST)
        if form.is_valid():
            ticket = form.instance
            ticket.creator = request.user

            ticket.closed = False
            #data = form.cleaned_data.items()
            #return HttpResponse(data)
            ticket.save()
            return HttpResponseRedirect(reverse("tickets:list"))
        else:
            return render(request, "tick/form.html", {"form": form})
    else:
        form = CreateFormTicket()
        tickets = Ticket.objects.filter(creator=request.user)
        count = Ticket.objects.filter(creator=request.user,closed=False).count()
        return render(request, "tick/form.html",
            {
            "form":form,
            "tickets": tickets,
            "count": count})

@login_required
def myticket(request):
    #tickets = Ticket.objects.filter(creator=request.user)
    count = Ticket.objects.filter(creator=request.user, closed=False).count()
    return render(request, "tick/ticket_mylist.html", {"count":count})

@login_required
def ticketdetail(request, pk):
    tickets = get_object_or_404(Ticket, pk=pk)
    return render(request, "tick/detail.html", {"tickets":tickets})


@login_required
def tick_close(request):
    if request.is_ajax():
        tic = request.GET.get('tic', None)
        if tic:
            tick = Ticket.objects.get(id=int(tic))
            tick.closed = True
            tick.save()
            return HttpResponse(json.dumps({"tic":tic})) #content_type="application/json"
        else:
            return HttpResponse(json.dumps({"tic":"12345"})) #content_type="application/json"

def ajax_post_add(request):
    if request.is_ajax():
        tic = request.GET.get('tic', None)
        data = request.GET.get('tic_data', None)
        if tic and data:
            ticket = Ticket.objects.get(id=int(tic))
            comment = TicketComment(author=request.user, comment=data)
            comment.ticket = ticket
            comment.save()

            return HttpResponse(json.dumps({'tic':tic,'data':data}))
        else:
            return HttpResponse({'error':''})
