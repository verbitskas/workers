from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives

from celery.task import task
from celery.task.base import Task, PeriodicTask 

from .jabb3 import main as jabber



class JabberTask(Task):
    name = 'Jabber-Task'
    ignore_result = True

    def _sender_jabb(self, to, msg):
        jabber(to, msg)

    def run(self, to, msg, *args, **kwargs):
        self._sender_jabb(to, msg)



class EmailJabberTask(Task):
    name = 'Отправка-jabber-почты'
    ignore_result = True

    def _send_email(self, subject, text, from_email, addresses_to):
        if isinstance(addresses_to, (list, tuple)):
            email = EmailMultiAlternatives(subject, text, from_email, addresses_to)
        else:
            email = EmailMultiAlternatives(subject, text, from_email, [addresses_to])
        email.send()


    def _send_jabber(self, addresses_to, text):
        if isinstance(addresses_to, (list, tuple)):
            if len(addresses_to) == 1:
                addresses_to = addresses_to[0]
            else:
                raise ValueError("addresses_to list on element or text")
        jabber(addresses_to, text)


    def run(self, subject, text, from_email, addresses_to, *args, **kwargs):
        self._send_email(subject, text, from_email, addresses_to)
        self._send_jabber(addresses_to, text)   



#@task(ignore_result=True)
#def async_ticket(id):
#    obj = Ticket.objects.get(id=id)
#    send_mail(
#        obj.subject,
#        obj.description,
#        settings.EMAIL_HOST_USER,
#        settings.LIST_OF_EMAIL_RECIPIENTS,
#        fail_silently=False)

#@task(ignore_result=True)
#def async_comment(id):
#    obj = TicketComment.objects.get(id=id)
#    email = obj.ticket.creator.email
#    jabber = obj.ticket.creator.jabber
#    push_jabber = obj.ticket.creator.push_jabber
#    push_email = obj.ticket.creator.push_email

#    if push_email and email:
#        send_mail(
#            obj.ticket.subject,
#            obj.comment,
#            settings.EMAIL_HOST_USER,
#            [obj.ticket.creator.email],
#            fail_silently=False)
#    if push_jabber and jabber:
#        pass
#        #main(jabber, obj.comment)