#-*-coding:utf-8 -*-
from django.apps import AppConfig


class ReferralConfig(AppConfig):
    name = 'referral'
    verbose_name = 'Партнерская программа'

    # def ready(self):
    #     import prof.signals
