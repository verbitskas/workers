from django.contrib import admin
from referral.models import Refery, BalansSite, Procent


class ReferralAdmin(admin.ModelAdmin):
    """Рефери и рефералы
    """
    fields = ('user', 'referal',)
    list_display = ('user', 'referal', 'created')

admin.site.register(Refery, ReferralAdmin)


class BalansSiteAdmin(admin.ModelAdmin):
    """Рефери и рефералы
    """
    fields = ('balance',)
    list_display = ('balance', )

admin.site.register(BalansSite, BalansSiteAdmin)


class ProcentAdmin(admin.ModelAdmin):
    """Рефери и рефералы
    """
    fields = ('proc_site', 'proc_refery', 'proc_referal')
    list_display = (
        'proc_site',
        'proc_refery',
        'proc_referal',
        'created',
        'update',
        )

admin.site.register(Procent, ProcentAdmin)
