#-*-coding:utf-8 -*-
from django import forms


class AddReferralForm(forms.Form):
    """Форма добавления реферала
    """
    refery = forms.CharField(label='', max_length=20)
