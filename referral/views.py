import decimal
from django.shortcuts import render
from django.http import HttpResponse

from referral.models import Refery, BalansSite, Procent
from referral.forms import AddReferralForm
from folow_mony.views import add_history_refery

from django.contrib.auth import get_user_model
User = get_user_model()


def add_referral(request):
    """Добавление реферала
    """
    if request.method == "POST":
        form = AddReferralForm(request.POST)
        if form.is_valid():
            pk = form.cleaned_data["refery"]
            try:
                ref = Refery.objects.get(id=pk)
                return HttpResponse("Вы не можете быть своим рефералом")
            except:
                ref = Refery()
                ref.user_id = pk
                ref.referal = request.user
                ref.save()
            return HttpResponse("Успех!")
        else:
            return HttpResponse("Не верные данные")
    return HttpResponse("Error")


def add_balance_site(summ, proc):
    """Зачисление на баланс биржи
    """
    # Создана ли запись баланса сайта
    try:
        birja = BalansSite.objects.get(id=1)
    except:
        birja = BalansSite()
    birja.balance += summ * proc / 100
    birja.save()


def partnerka(ref, summ):
    """Зачисление процентов по партнерке
    """
    # Провепрка настроек партнерки
    try:
        proc = Procent.objects.get(id=1)
    except:
        pass
    # Провепрка на реферала
    try:
        referal = Refery.objects.get(referal=ref)
    except:
        add_balance_site(summ, proc.proc_site)
        return False
    if referal:
        # Для рефери
        refery = User.objects.get(id=referal.user_id)
        summ_refery = float(summ) * proc.proc_refery / 100
        summ_str = str(summ_refery)
        dec = decimal.Decimal(summ_str)
        refery.balance += dec
        refery.save()
        bitcoin = 0
        add_history_refery(referal.user, refery.balance, summ, proc.proc_refery, bitcoin, "5")
        # Для биржи
        proc_s = proc.proc_site - (proc.proc_refery + proc.proc_referal)
        add_balance_site(summ, proc_s)
        # Для реферала
        summ = summ * proc.proc_referal / 100
        return {"summ": summ, "proc": proc.proc_referal}
