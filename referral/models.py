from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth import get_user_model

User = settings.AUTH_USER_MODEL


class Refery(models.Model):
    """Рефералы
    """
    user = models.ForeignKey(User,
        verbose_name=_("Рефери"),
        related_name='refery')
    referal = models.ForeignKey(User,
        verbose_name=_("Реферал"),
        related_name='referal')
    created = models.DateTimeField(_("Дата"), auto_now_add=True)

    class Meta():
        verbose_name = "Реферал"
        verbose_name_plural = "Рефералы"

    def __str__(self):
        return "%s" % self.user.name


class BalansSite(models.Model):
    """Баланс биржи
    """
    balance = models.FloatField(_("Баланс биржи"), default=0)

    class Meta():
        verbose_name = "Баланс биржи"
        verbose_name_plural = "Баланс биржи"

    def __str__(self):
        return "%s" % self.balance


class Procent(models.Model):
    """Процент партнерки
    """
    proc_site = models.IntegerField(_("Процент биржи"), default=25)
    proc_refery = models.IntegerField(_("Процент рефери"), default=3)
    proc_referal = models.IntegerField(_("Процент реферала"), default=2)
    created = models.DateTimeField(_("Дата установки"), auto_now_add=True)
    update = models.DateTimeField(_("Дата изменения"), auto_now=True)

    class Meta():
        verbose_name = "Настройка партнерской программы"
        verbose_name_plural = "Настройки партнерской программы"

    def __str__(self):
        return "%s" % self.proc_site
