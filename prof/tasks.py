from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives

from celery.task import task
from celery.task.base import Task, PeriodicTask 


class EmailTask(Task):
	name = 'Отправка-почты'
	ignore_result = True

	def _send_email(self, subject, text, from_email, addresses_to):
		if isinstance(addresses_to, (list, tuple)):
			email = EmailMultiAlternatives(subject, text, from_email, addresses_to)
		else:
			email = EmailMultiAlternatives(subject, text, from_email, [addresses_to])
		email.send()

	def run(self, subject, text, from_email, addresses_to, *args, **kwargs):
		self._send_email(subject, text, from_email, addresses_to)
		
