#-*-coding:utf-8 -*-
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth import get_user_model
from prof.models import User, Notes, Notice, NotesIp

class ProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('push_jabber','jabber','push_email')

class NotesForm(forms.ModelForm):
    class Meta:
        model = Notes
        fields = ('text',)


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(
        label=u'Пароль',
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label=u'Подтверждение',
        widget=forms.PasswordInput
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(u'Пароль и подтверждение не совпадают')

        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ('email','client',)


class UserChangeForm(forms.ModelForm):

    '''
    Форма для обновления данных пользователей. Нужна только для того, чтобы не
    видеть постоянных ошибок "Не заполнено поле password" при обновлении данных
    пользователя.
    '''
    password = ReadOnlyPasswordHashField(
        widget=forms.PasswordInput,
        required=False
    )

    def save(self, commit=True):
        user = super(UserChangeForm, self).save(commit=False)
        password = self.cleaned_data["password"]
        if password:
            user.set_password(password)
        if commit:
            user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ['email', 'client']


class LoginForm(forms.Form):

    """Форма для входа в систему
    """
    username = forms.CharField()
    password = forms.CharField()


class NotesIpForm(forms.ModelForm):
    '''Форма добавления заметки к IP'''
    class Meta:
        model = NotesIp
        fields = ('text',)



class EmailJabberForm(forms.Form):
    '''Форма изменения email/jabber'''
    email_jabber = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Новая почта'}))
    message = forms.CharField(required=False)
