#-*-coding:utf-8 -*-
from django.contrib import admin
from django import forms
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group

from prof.forms import UserChangeForm
from prof.forms import UserCreationForm
from prof.models import User, Notice, Echo


class NoticeAdmin(admin.ModelAdmin):
    pass

class EchoAdminForm(forms.ModelForm):
    
    class Meta:
        model = Echo
        fields = ('text',)
        widgets = {
            'text': admin.widgets.AdminTextareaWidget
        }

class EchoAdmin(admin.ModelAdmin):
    form = EchoAdminForm

class UserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = [
        'email',
        'jabber',
        'is_admin',
        'is_active',
        #'gen_mini_avatar',
    ]

    list_filter = ('is_admin',)

    fieldsets = (
                (None, {'fields': ('client', 'email', 'password')}),
                ('Personal info', {
                 'fields': (
                     'avatar',
                     'email',
                     'jabber',
                     'balance',
                     'push_jabber',
                     'push_email',
                     'method',
                     'google_name',
                     'google_token',
                     'qrcode'
                 )}),
                ('Permissions', {'fields': ('is_admin',)}),
                ('Important dates', {'fields': ('last_login',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'password1',
                'password2'
            )}
         ),
    )

    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

# Регистрация нашей модели
admin.site.register(User, UserAdmin)
admin.site.register(Notice, NoticeAdmin)
admin.site.register(Echo, EchoAdmin)
admin.site.unregister(Group)