import sys
import os
import random

from django.conf import settings

from .totp_auth import TotpAuth

def google_totp_create(model=None):
    ta = TotpAuth()
    qr = ta.qrcode(settings.SITESNAME)
    img_name = random.randint(1,999999)
    qr._img.save(settings.MEDIA_ROOT + "/qrcode/" + str(img_name) + '.jpg')
    model.google_token = ta.secret
    model.qrcode = "/media/qrcode/" + str(img_name) + '.jpg'
    model.save()
    

def google_totp_check(token=None, model=None):
    tok = model.google_token
    email = model.email
    ta = TotpAuth(tok)
    if ta.valid(token):
        return True
    else:
        return False