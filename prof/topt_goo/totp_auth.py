from .totp import TOTP
import qrcode
import datetime

import random as _random

def random_base32(length=16, random=_random.SystemRandom(),
                  chars=list('ABCDEFGHIJKLMNOPQRSTUVWXYZ234567')):
    return ''.join(
        random.choice(chars)
        for _ in range(length)
    )


class TotpAuth(object):

    def __init__(self, secret=None):
        if secret is None:
            secret = random_base32()
        self.secret = secret
        self.totp = TOTP(secret)

    def generate_token(self):
        return self.totp.now()

    def valid(self, token):
        try:
            token = int(token)
        except ValueError:
            return False
        now = datetime.datetime.now()
        time30secsago = now + datetime.timedelta(seconds=-30)
        try:
            valid_now = self.totp.verify(token)
            valid_past = self.totp.verify(token, for_time=time30secsago)
            return valid_now or valid_past
        except:
            return False

    def qrcode(self, username):
        uri = self.totp.provisioning_uri(username)
        return qrcode.make(uri, box_size=4)
