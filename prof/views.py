import datetime, requests, json

from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from django.conf import settings
from django.shortcuts import render, render_to_response, redirect
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import Http404
from django.core.mail import EmailMultiAlternatives
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.views.generic import DetailView
from django.contrib.auth import get_user_model

from proxy.models import Proxy, TimeSet
from client.models import Port, Iplist
from prof.forms import ProfileForm, NotesForm, EmailJabberForm
from prof.models import User, Notes, HistoryLogin
from pay.models import Order, Purses, Refill
from referral.models import Refery
from referral.forms import AddReferralForm

from pay.forms import RefillForm
from pay.tasks import check_bitcoin
from django.contrib.auth.forms import PasswordChangeForm

from tickets.jabb3 import main as jabber

from django.contrib.sites.models import Site


@login_required
def qrcode_status(request):
    if request.is_ajax() and request.method == 'GET':
        user = User.objects.get(id=request.user.id)
        img = user.qrcode.url
        domain = Site.objects.get_current().domain + img
        return HttpResponse(json.dumps({'img': domain}))
    return HttpResponse(json.dumps({'status':'не аякс'}))



@login_required
def monitoring(request, id):
    context = {}
    if request.user.is_superuser:
        user = User.objects.get(id=id)
        context['proxys_off'] = Proxy.objects.filter(vendor=user,checkers=False)
        context['proxys_all'] = Proxy.objects.filter(vendor=user, update__isnull=True)
        context['proxys_on'] = Proxy.objects.filter(vendor=user,checkers=True)
        return render(request, "prof/monitoring.html",context)


def bitcoin(purse):
    """Проверка баланса на кошельке
    """
    r = requests.get \
        ('https://blockexplorer.com/api/addr/{}'.format(purse))
    if r.status_code == 200:
        strs = json.loads(r.text)
        balance = float(strs['totalReceived'])
        return balance


def kurs(usd):
    """Проверка курса биткоин к USD
    """
    r = requests.get \
        ('http://preev.com/pulse/units:btc+usd/sources:bitfinex+bitstamp+btce')
    if r.status_code == 200:
        strs = json.loads(r.text)
        val = float(strs['btc']['usd']['bitfinex']['last'])
    else:
        r2 = requests.get("https://www.bitstamp.net/api/ticker/")
        strs2 = json.loads(r2.text)
        val = float(strs['last'])
    context = {}
    context['truck'] = val
    context['val'] = 1 / val * usd
    return context


@login_required
def profile(request):
    """
    Страница профиля пользователя
    Пополнение баланса
    """
    # form = ProfileForm(instance=request.user)
    # port = Port.objects.filter(user=request.user)
    # context['port'] = port
    # context['iplist'] = Iplist.objects.filter(link=request.user)
    # context['proxys_on'] = Proxy.objects.filter(vendor=request.user,checkers=True)
    # context['proxys_off'] = Proxy.objects.filter(vendor=request.user,checkers=False)
    # context['proxys_all'] = Proxy.objects.filter(vendor=request.user, update__isnull=True)
    # context['form'] = form

    #else:
    form = RefillForm()
    pass_form = PasswordChangeForm(user=request.user)
    email_form = EmailJabberForm()
    context = {'form_bit': form, 'bit': '', 'pass_form': pass_form, "email_form": email_form}
    # Вывод формы для добавления в рефералы
    try:
        referal = Refery.objects.get(referal=request.user)
    except:
        ref = AddReferralForm()
        context["ref"] = ref
    # Вывод статистики входов
    log = HistoryLogin.objects.filter(user=request.user)
    context["log"] = log
    return render(request, "prof/profile.html",context)

@login_required
def proxypay(request):
    #obj = {
        #'ports': Port.objects.filter(user=request.user),
        #'iplists': Iplist.objects.filter(link=request.user),
        #'orders': Order.objects.filter(user=request.user)
        #'form': NotesForm(instance=request.user)
        #}
    # if request.method == 'POST':
    #     form  = NotesForm(request.POST)
    #     if form.is_valid():
    #         notes = Notes(user=request.user, text=form.cleaned_data['text'])
    #         notes.save()

    return render(request, 'prof/proxypay.html')

@login_required
def formsave(request):
    if request.POST:
        form = ProfileForm(request.POST)
        if form.is_valid():
            jabber = form.cleaned_data["jabber"]
            push_jabber = form.cleaned_data["push_jabber"]
            push_email = form.cleaned_data["push_email"]
            user = User.objects.get(id=request.user.id)
            user.push_email = push_email
            user.jabber = jabber
            user.push_jabber = push_jabber
            user.save()
            return HttpResponseRedirect(reverse("prof:profile",args=(request.user.id,)))

    return HttpResponseRedirect(reverse("prof:profile"))



@login_required
def iplist(request, pk):
    obj = Iplist.objects.get(id=int(pk))
    if obj.link != request.user:
        return HttpResponseRedirect(reverse("proxy:index"))
    count_max = obj.dicts[1]
    proxyobj = Proxy.objects.filter(obj.dicts[0])[:count_max]
    return render(request, "prof/iplist.html", {'proxy': proxyobj})


class Dead(DetailView):
    model = User
    template_name = 'prof/profile.html'
    ts = TimeSet.objects.first()
    now = timezone.now()
    try:
        offset = now - datetime.timedelta(minutes=ts.offset)
    except AttributeError:
        offset = False

    def get_context_data(self, **kwargs):
        context = super(Dead, self).get_context_data(**kwargs)
        user = User.objects.get(id=self.kwargs['pk'])
        context['profile'] = user
        if self.offset:
            context['dead'] = Proxy.objects.filter(vendor=user, update__lte=self.offset)
        else:
            context['dead'] = Proxy.objects.filter(vendor=user, checkers=False)
        return context


class Good(DetailView):
    model = User
    template_name = 'prof/profile.html'
    ts = TimeSet.objects.first()
    now = timezone.now()
    try:
        offset = now - datetime.timedelta(minutes=ts.offset)
    except AttributeError:
        offset = False

    def get_context_data(self, **kwargs):
        context = super(Good, self).get_context_data(**kwargs)
        user = User.objects.get(id=self.kwargs['pk'])
        context['profile'] = user
        if self.offset:
            context['good'] = Proxy.objects.filter(vendor=user, update__gte=self.offset)
        else:
            context['good'] = Proxy.objects.filter(vendor=user, checkers=True)
        return context


def backconnect_up(request, pk):
    return HttpResponse(u"Включил" + pk)

def backconnect_down(request, pk):
    return HttpResponse(u"Удалил" + pk)


@login_required
def create_pay(request):
    """Запрос на пополнение баланса
    """
    if request.method == "POST":
        form = RefillForm(request.POST)
        if form.is_valid():
            refill = form.save(commit=False)
            # Присвоение кошелька
            try:
                purse = Purses.objects.get(client=request.user, busy=True)
            except:
                qs = Purses.randoms.filter(busy=False)[:1].get()
                purse = qs
                purse.client = request.user
                purse.busy = True
                purse.save()

            coin = bitcoin(purse.purse)
            if coin == 0:
                coin = 0.0
            truck = kurs(form.cleaned_data['usd'])
            bit = round(truck['val'], 8)
            refill.user = request.user
            refill.purse = purse
            refill.usd = form.cleaned_data['usd']
            refill.bit = bit
            refill.truck = truck['truck']
            refill.balance = coin
            refill.save()

            user = request.user.id
            purse = purse.purse
            usd = form.cleaned_data['usd']

            context = {'bit': bit, "purse": purse}
            check_bitcoin.delay(user, purse, usd, bit)
            return HttpResponse(json.dumps(context))
    # else:
    #     form = RefillForm()
    #     context = {'form': form, 'bit': ''}
    # return render(request, "prof/refill.html", context)
#
#
# def test_bit(request):
#     context = {}
#     context['totalReceived'] = 2.1155
#     context['totalSent'] = 2
#     return JsonResponse(context)

@login_required
def email_jabber_wiews(requests):
    if requests.method == 'POST':
        form = EmailJabberForm(requests.POST)
        if form.is_valid():
            email_jabber = form.cleaned_data['email_jabber']
            message = form.cleaned_data['message']

            if email_jabber:
                user = User.objects.get(id=requests.user.id)
                #user.code_email = None
                #user.code_jabber = None
                user.jabber_email_tmp = email_jabber

                # Email
                subject = "CODE"
                code = get_random_string()
                from_email = settings.EMAIL_HOST_USER
                addresses_to = email_jabber
                email = EmailMultiAlternatives(subject, code, from_email, [addresses_to])
                try:
                    email.send()
                    user.code_email = code
                    user.save()
                except Exception as exc:
                    pass

                # Jabber
                code = get_random_string()
                jabber(email_jabber, code)
                user.code_jabber = code
                user.save()
                context = {'email_jabber':email_jabber}
                return HttpResponse(json.dumps(context))

            elif message:
                user = User.objects.get(id=requests.user.id)
                code_email = user.code_email
                code_jabber = user.code_jabber

                if code_jabber == message:
                    user.email = user.jabber_email_tmp

                if code_email == message:
                    user.email = user.jabber_email_tmp

                user.save()

                context = {'email_jabber': message}
                return HttpResponse(json.dumps(context))
