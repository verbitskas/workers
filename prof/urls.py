#-*-coding:utf-8 -*-
from django.conf.urls import url
from prof.views import profile, iplist, Good, Dead, backconnect_down, \
	backconnect_up, formsave, proxypay, monitoring, create_pay, email_jabber_wiews, qrcode_status

from prof.ajax import *

urlpatterns = [
    url(r'^profile/live/(?P<pk>\d+)/$', Good.as_view(), name='profile-good'),
    url(r'^profile/dead/(?P<pk>\d+)', Dead.as_view(), name='profile-dead'),
    url(r'^profile/$', profile, name='profile'),
    url(r'^proxypay/$', proxypay, name='proxypay'),
    url(r'^monitoring/(?P<id>\d+)/$', monitoring, name='monitoring'),
    url(r'^formsave/$', formsave, name="formsave"),
    url(r'^profile/add/(?P<pk>\d+)/$', backconnect_up, name='backconnect_up'),
    url(r'^profile/del/(?P<pk>\d+)/$', backconnect_down, name='backconnect_down'),
    url(r'^iplist/(?P<pk>\d+)/$', iplist, name='iplist'),

    url(r'^oplata/$', create_pay, name='create_pay'),

    url(r'^pswc/$', password_change, name='pass_change'),
    url(r'^email_jabber_change/$', email_jabber_wiews, name='email_jabber_change'),
    url(r'^la/$', login_ajax, name='login_f'),
    url(r'^qrcode/$', qrcode_status, name='qrcode')
]
