# coding: utf-8
#from django_jabber import send_message

def validate_email(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def send_jabber(message, jabber):
	"""функция заглушка по отправки jabber"""
	pass