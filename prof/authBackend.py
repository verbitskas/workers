from .models import User
from django.contrib.auth import get_user_model


dicts = {}

class AuthBackend(object):
    #def authenticate(self, email=None, password=None):
    #    if '@' in email:
    #        kwargs = {'email': email}
    #   
    #    try:
    #        user = User.objects.get(**kwargs)
    #        if user.check_password(password):
    #            return user
    #    
    #    except User.DoesNotExist:
    #        return None

    def authenticate(self, username=None, password=None, url_=None,**kwargs):
        User = get_user_model()
        if username is None:
            username = kwargs.get(User.USERNAME_FIELD)
        try:
            user = User._default_manager.get_by_natural_key(username)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            User().set_password(password)



    def get_user(self, user_id):
        User = get_user_model()
        try:
            return User._default_manager.get(pk=user_id)
        except User.DoesNotExist:
            return None

