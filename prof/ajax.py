import json
import re

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.crypto import get_random_string
from django.conf import settings
from django.contrib.auth import get_user_model

User = get_user_model()

from tickets.jabb3 import main as jabber

from .topt_goo.google_creater_token import google_totp_check


def password_change(request):
    """Смена пароля
    """
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return HttpResponse("Пароль успешно изменен")
        else:
            return HttpResponse("Пароль не верен")


@csrf_exempt
def login_ajax(request):
    """Вход на сайт"""
    if request.is_ajax() and request.method == 'POST':
        email_jabber = request.POST.get("username", False)
        password = request.POST.get("password", False)
        code = request.POST.get("code", False)
        google = request.POST.get('google', False)


        if not google and email_jabber and password:
            try:
                user = User.objects.get(email=email_jabber)
            except:
                return HttpResponse({"status": "err"}, content_type="application/json")

            if not user.check_password(password):
                return HttpResponse({'status': "не верный пароль"}, content_type="application/json")


            if user.method == '1':
                #1 google
                google_token = user.google_token
                return HttpResponse(json.dumps({"status": "google"}))

            elif user.method == '2':
                # code jabber/email
                subject = "CODE"
                codes = get_random_string()
                user.code = codes
                user.save()

                if user.jabber:
                    jabber(email_jabber, codes)
                    return HttpResponse(json.dumps({"status": "ok jabber"}))

                else:
                    from_email = settings.EMAIL_HOST_USER
                    addresses_to = email_jabber
                    email = EmailMultiAlternatives(subject, codes, from_email, [addresses_to])
                    email.send()
                    return HttpResponse(json.dumps({"status": "ok email"}))


            else:
                if user.check_password(password):
                    user = authenticate(username=email_jabber, password=password)
                    if user is not None:
                        if user.is_active:
                            login(request, user)
                            return HttpResponse(json.dumps({"status": "success"}))
                        else:
                            return HttpResponse(json.dumps({"status": "error"}))
                    else:
                        return HttpResponse(json.dumps({"status": "error"}))

        if code:
            #code jabber/email
            try:
                password = request.POST.get("ps", False)
                password = password.split('=')[-1]
                code = code.split('=')[-1]
                #code = code.split('=')[-2].split('&')[0]
                user = User.objects.get(code=code)
            except:
                return HttpResponse(json.dumps({"status": "error_4444"}))
            if user.code == code and password:
                username = user.email
                user = authenticate(username=username, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return HttpResponse(json.dumps({"status": "success"}))
                    else:
                        return HttpResponse(json.dumps({"status": "error"}))
                else:
                    return HttpResponse(json.dumps({"status": "error_code"}))


        if google:
            # google code
            try:
                email_jabber = request.POST.get('email_jabber', False)
                user = User.objects.get(email=email_jabber)
            except:
                return HttpResponse(json.dumps({"status": "error"}))
            if google_totp_check(token=google, model=user):
                user = authenticate(username=email_jabber, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return HttpResponse(json.dumps({"status": "success"}))
                    else:
                        return HttpResponse(json.dumps({"status": "error1"}))
                else:
                    return HttpResponse(json.dumps({"status": "error2"}))

            else:
                return HttpResponse(json.dumps({"status": "не верный код"}))


    return JsonResponse({"status": "Не аякс"})
