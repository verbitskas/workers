#-*-coding:utf-8 -*-
from django.apps import AppConfig


class ProfConfig(AppConfig):
    name = 'prof'
    verbose_name = u'Профиль'

    def ready(self):
        import prof.signals
