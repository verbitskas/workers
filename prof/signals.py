from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth import get_user_model

from prof.models import HistoryLogin
from ipware.ip import get_real_ip
User = get_user_model()

@receiver(post_save, sender=User)
def create_user_log(sender, instance, **kwargs):
    if instance.last_login:
        try:
            ip = get_real_ip(request)
            if ip is not None:
                ip_user = ip
            else:
                ip_user = "Не определен"
        except:
            ip_user = "No"
        log = HistoryLogin()
        log.user = instance
        log.ip = ip_user
        log.save()
