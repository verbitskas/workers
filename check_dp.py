import asyncio
import logging
import sys
import re
import datetime

import aiohttp
import aiopg

import aiosocks
from async_timeout import timeout
import asyncpg
from asyncpg.exceptions import TooManyConnectionsError, ConnectionDoesNotExistError

#from bc.leaky_bucket import AsyncLeakyBucket

now = datetime.datetime.now()
delta =  now - datetime.timedelta(hours=1)

msg = "postgresql://{user}@{host}:{port}/{database}"
DSN = msg.format(user='john', host='localhost', port=5432, database='proj')
sem = asyncio.Semaphore(50)

SQL_READ = """
	SELECT p.ip, p.port FROM proxy_proxy as p, proxy_worker as w 
	WHERE p.worker_id=w.id AND w.ip='185.235.245.14'
	AND p.tp='socks5' AND p.scan=False
	AND p.typeproxy='bp'"""


logging.basicConfig(
	#filename='check_dp.log',
	format="%(lineno)d '|' %(message)s",
	level=logging.INFO
)


class AsyncChecker:
	def __init__(self, url, db=None):
		self.sem = asyncio.BoundedSemaphore(300)
		#self.bucket = AsyncLeakyBucket(1000, 5.0)
		self._loop = asyncio.get_event_loop()
		self.dst = ('185.235.245.17', 5566)
		self.url = url
		self.db = db

		self.log = logging.getLogger()
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}")
		self.iprealre = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
		self.dsn = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'
		self.sql_read = SQL_READ

		self.qsize = asyncio.Queue()


	async def _read_db(self):
		async with aiopg.create_pool(self.dsn) as pool:
			async with pool.acquire() as conn:
				async with conn.cursor() as cur:
					await cur.execute(self.sql_read)
					res = await cur.fetchall()
					return res



	async def _read_socks(self):
		async with aiohttp.ClientSession() as session:
			async with session.get(self.url) as resp:
				data = await resp.text()
				return data


	async def _write_socks(self, ip, port, ipreal, checkers):
		sql = "insert into bc (ip, port, ipreal, online) VALUES ($1, $2, $3, $4);"
		read = "SELECT ip, port, ipreal FROM bc WHERE ipreal=$1"
		try:
			async with sem:
				con = await asyncpg.connect(dsn=DSN)
				await con.execute(sql, ip, int(port), ipreal, checkers)
		except asyncio.CancelledError as exc:
			pass
		except asyncpg.exceptions.UniqueViolationError as exc:
			async with sem:
				res = await con.fetch(read, ipreal)
				if int(port) != res[0].get('port'):
					self.log.info("*"*80)
					self.log.info("[--] %s:%s --> %s", ip, port, ipreal)
					self.log.info("[db] %s:%s --> %s", res[0].get('ip'), res[0].get('port'), res[0].get('ipreal'))
		except Exception as exc:
			self.log.exception(exc)
		finally:
			try:
				await con.close()
			except:
				pass


	async def check(self, obj):
		try:
			ip, port = obj.split(':')
		except (ValueError, AttributeError):
			self.log.info(obj)
			return obj

		async with self.sem:
			try:
				socks5_addr = aiosocks.Socks5Addr(ip, int(port))
				async with timeout(10):
					reader, writer = await aiosocks.open_connection(
						proxy=socks5_addr, proxy_auth=None, dst=self.dst)
					data = await reader.read(1024)
					if data:
						try:
							ipreal = self.iprealre.findall(data.decode('latin1'))[0]
							await self.qsize.put(ipreal)
							await self._write_socks(ip, port, ipreal, True)
							self.log.info("ip: %s:%s реальный: %s", ip, port, ipreal)
							writer.close()
						except Exception as exc:
							self.log.exception(exc)
							self.log.info(data)
							writer.close()
							return obj
					else:
						writer.close()
						return obj
			except Exception:
				return obj


	def run(self):
		loop = asyncio.get_event_loop()

		if self.db:
			self.log.info("чекаем из биржи")
			data = loop.run_until_complete(self._read_db())
			response = ['{}:{}'.format(x[0], x[1]) for x in data]

		else:
			data = loop.run_until_complete(self._read_socks())
			data = data.split()
			response = [url for url in data if self.pattern.findall(url)]
		
		self.log.info("всего: %s", len(response))
		

		tasks = [self.check(obj) for obj in response]
		res = loop.run_until_complete(asyncio.gather(*tasks))
		self.log.info("рабочих: %s", self.qsize.qsize())

		response = [ip for ip in res if ip is not None]
		self.log.info("на повтор %s" % len(response))
		tasks = [self.check(obj) for obj in response]
		res = loop.run_until_complete(asyncio.gather(*tasks))
		self.log.info("рабочих: %s", self.qsize.qsize())

		# 3 повтор
		response = [ip for ip in res if ip is not None]
		self.log.info("на повтор %s" % len(response))
		tasks = [self.check(obj) for obj in response]
		loop.run_until_complete(asyncio.gather(*tasks))

		self.log.info("рабочих: %s", self.qsize.qsize())
		#op = []
		#for i in range(self.qsize.qsize()):
		#	try:
		#		op.append(self.qsize.get_nowait())
		#	except:
		#		pass
		#self.log.info(op)



from bc.static_memory import trace


@trace
def main(url, db=None):
	check = AsyncChecker(url, db)
	check.run()


if __name__ == '__main__':
	#import tracemalloc
	#from hurry.filesize import size
	#import os

	#tracemalloc.start(100)
	#time1 = tracemalloc.take_snapshot()

	if len(sys.argv) > 1 and sys.argv[1].startswith('http'):
		logging.info(sys.argv[1])
		main(sys.argv[1])
	elif len(sys.argv) > 1 and sys.argv[1] == 'db':
		main(sys.argv[1], True)
	else:
		logging.info("укажите url")

	#time2 = tracemalloc.take_snapshot()
	#stats = time2.compare_to(time1, 'lineno')
	
	#for stat in stats[:10]:
	#	print(stat)
	#	print(os.path.basename(__file__))
	#	print(size(stat.size))
	
	#with open('stat.txt', 'at') as f:
	#	for stat in stats[:10]:
	#		#msg  = "файл {}\n".format(str(stat).split(':')[0])
	#		#msg += "строка {}\n".format(str(stat).split(':')[1])
	#		#msg += "размер {}\n".format(size(stat.size))
	
	#		f.write(str(stat) + '\n')
	#		print(stat)
	#		f.write(str(size(stat.size) + '\n'))
	#	f.write("*"*120 + "\n")



