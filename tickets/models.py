#-*- coding:utf-8 -*-
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver as rec
from django.core.mail import send_mail

from celery.task import task

from tickets.settings import STATUS_CHOICES, CLOSED_STATUSES

from tickets.jabb3 import main
from vendor.async_receiver import async_receiver as receiver

class Department(models.Model):
    dep = models.CharField(_("отдел"), max_length=500, blank=True, null=True)

    def __str__(self):
        return self.dep

    class Meta:
        verbose_name = _("Отдел")
        verbose_name_plural = _("Отделы")

class Ticket(models.Model):
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Creator"), related_name='tickets')
    date = models.DateTimeField(_("Date"), auto_now_add=True)
    last_update = models.DateTimeField(_("Date"), auto_now=True)
    subject = models.CharField(_("Subject"), max_length=255)
    department = models.ForeignKey(Department, verbose_name=_("проблема"),
        related_name="departments",null=True)
    description = models.TextField(_("Description"), help_text=_("A detailed description of your problem."))
    assignee = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Assignee"), related_name="assigned_tickets", blank=True, null=True)
    status = models.SmallIntegerField(_("Status"), choices=STATUS_CHOICES, default=0)


    class meta:
        verbose_name = _("Ticket")
        verbose_name_plural = _("Tickets")
        ordering = ['date']

    def get_comments_count(self):
        return self.comments.count()

    def get_latest_comment(self):
        return self.comments.latest('date')

    def __str__(self):
        return "%s# %s" % (self.id, self.subject)

    def is_closed(self):
        return self.status in CLOSED_STATUSES

    def is_answered(self):
        try:
            latest = self.get_latest_comment()
        except TicketComment.DoesNotExist:
            return False
        return latest.author != self.creator
    is_answered.boolean = True
    is_answered.short_description = _("Is answered")



class TicketComment(models.Model):
    ticket = models.ForeignKey(Ticket, verbose_name=_("Ticket"), related_name='comments')
    date = models.DateTimeField(auto_now_add=True, verbose_name=_("Date"))
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Author"))
    comment = models.TextField(_("Comment"))

    class Meta:
        verbose_name = _("Ticket comment")
        verbose_name_plural = _("Ticket comments")
        ordering = ['date']

    def __str__(self):
        return "Comment on " + str(self.ticket)

@rec(post_save, sender=Ticket)
def post_tickets(sender, **kwargs):
    obj = kwargs['instance']
    #id = obj.id
    #async_ticket.apply_async(args=(id,),queue='low')
    send_mail(
        obj.subject,
        obj.description,
        settings.EMAIL_HOST_USER,
        settings.LIST_OF_EMAIL_RECIPIENTS,
        fail_silently=False)


@rec(post_save, sender=TicketComment)
def post_ticketcomment(sender, **kwargs):
    obj = kwargs['instance']
    #id = obj.id
    #async_comment.apply_async(args=(id,),queue='low')
    email = obj.ticket.creator.email
    jabber = obj.ticket.creator.jabber
    push_jabber = obj.ticket.creator.push_jabber
    push_email = obj.ticket.creator.push_email

    if push_email and email:
        send_mail(
            obj.ticket.subject,
            obj.comment,
            settings.EMAIL_HOST_USER,
            [obj.ticket.creator.email],
            fail_silently=False)
    if push_jabber and jabber:
        main(jabber, obj.comment)


@task(ignore_result=True)
def async_ticket(id):
    obj = Ticket.objects.get(id=id)
    send_mail(
        obj.subject,
        obj.description,
        settings.EMAIL_HOST_USER,
        settings.LIST_OF_EMAIL_RECIPIENTS,
        fail_silently=False)

@task(ignore_result=True)
def async_comment(id):
    obj = TicketComment.objects.get(id=id)
    email = obj.ticket.creator.email
    jabber = obj.ticket.creator.jabber
    push_jabber = obj.ticket.creator.push_jabber
    push_email = obj.ticket.creator.push_email

    if push_email and email:
        send_mail(
            obj.ticket.subject,
            obj.comment,
            settings.EMAIL_HOST_USER,
            [obj.ticket.creator.email],
            fail_silently=False)
    if push_jabber and jabber:
        pass
        #main(jabber, obj.comment)