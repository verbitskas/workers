import asyncio
import logging
import sys
import re
import datetime

import aiohttp
import aiopg

import aiosocks
from async_timeout import timeout


now = datetime.datetime.now()
delta =  now - datetime.timedelta(hours=1)


SQL_READ = """
	SELECT p.ip, p.port FROM proxy_proxy as p, proxy_worker as w 
	WHERE p.worker_id=w.id AND w.ip='185.235.245.14'
	AND p.tp='socks5' AND p.scan=False
	AND p.typeproxy='bp'"""


logging.basicConfig(
	format="%(lineno)d '|' %(message)s",
	level=logging.INFO
)


class AsyncChecker:
	def __init__(self, url, db=None):
		self.sem = asyncio.BoundedSemaphore(1000)
		self._loop = asyncio.get_event_loop()
		self.dst = ('185.235.245.17', 9999)
		self.url = url
		self.db = db

		self.log = logging.getLogger()
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}")
		self.iprealre = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
		self.dsn = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'
		self.sql_read = SQL_READ

		self._ipreal = []

		self.total = 0
		self.qsize = asyncio.Queue()


	async def _read_db(self):
		async with aiopg.create_pool(self.dsn) as pool:
			async with pool.acquire() as conn:
				async with conn.cursor() as cur:
					await cur.execute(self.sql_read)
					res = await cur.fetchall()
					return res



	async def _read_socks(self):
		async with aiohttp.ClientSession() as session:
			async with session.get(self.url, ssl=False) as resp:
				data = await resp.text()
				return data


	async def check(self, obj):
		try:
			ip, port = obj.split(':')
		except AttributeError:
			return
		async with self.sem:
			try:
				socks5_addr = aiosocks.Socks5Addr(ip, int(port))
				async with timeout(30):
					reader, writer = await aiosocks.open_connection(
						proxy=socks5_addr, proxy_auth=None, dst=self.dst)

					writer.write(b"GET / HTTP/1.1\r\n\r\n")
					data = await reader.read(1024)
					if data:
						self.total += 1
						try:
							ipreal = self.iprealre.findall(data.decode('latin1'))[0]
							self._ipreal.append(ipreal)
							self.log.info("ip: %s:%s реальный: %s", ip, port, ipreal)
							await self.qsize.put(ipreal)
						except UnicodeDecodeError as exc:
							self.log.info(data)
						except IndexError:
							self.info('-'*80)
							self.log.info(data)
					else:
						writer.close()
						return obj
					writer.close()
			except Exception:
				return obj


	def run(self):
		loop = asyncio.get_event_loop()

		if self.db:
			self.log.info("чекаем из биржи")
			data = loop.run_until_complete(self._read_db())
			response = ['{}:{}'.format(x[0], x[1]) for x in data]

		else:
			data = loop.run_until_complete(self._read_socks())
			data = data.split()
			response = [url for url in data if self.pattern.findall(url)]
		
		self.log.info("всего: %s", len(response))
		#response = [url for url in data if pattern.findall(url)]
		resp = []

		for obj in response:
			try:
				resp.append((obj.split(':')[0], int(obj.split(':')[1])))
			except Exception:
				resp.append((obj.split(';')[0].split(':')[0], int(obj.split(';')[0].split(':')[1])))
		response = ["{}:{}".format(*x) for x in resp]	
		

		tasks = [self.check(obj) for obj in response]
		res = loop.run_until_complete(asyncio.gather(*tasks))
		self.log.info("рабочих: %s", self.total)
		old = self.total
		self.total = 0

		self.log.info("повтор")
		tasks = [self.check(obj) for obj in res]
		loop.run_until_complete(asyncio.gather(*tasks))

		self.log.info("рабочих: %s", old + self.total)
		self.log.info("ipreal: %s", len(self._ipreal))
		self.log.info("ipreal без дубликатов: %s", len(list(set(self._ipreal))))




def main(url, db=None):
	check = AsyncChecker(url, db)
	check.run()


if __name__ == '__main__':
	if len(sys.argv) > 1 and sys.argv[1].startswith('http'):
		logging.info(sys.argv[1])
		main(sys.argv[1])
	elif len(sys.argv) > 1 and sys.argv[1] == 'db':
		main(sys.argv[1], True)
	else:
		logging.info("укажите url")
