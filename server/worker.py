#-*-coding:utf-8-*-
import asyncio
import os
import sys
import time
import logging
import concurrent.futures
import multiprocessing
from multiprocessing.connection import Listener, AuthenticationError

import aiodns
import aiopg
import asyncpg
from async_timeout import timeout

import django

from daemonize import daemonize

sys.path.insert(0, os.path.dirname("../" + __file__))
sys.path.append('/home/john/proxyproject')
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

django.setup()
from django.conf import settings


logging.basicConfig(
        filename = "black-list-scan.log",
        format = '%(asctime)s - %(name)s - %(levelname)s -%(lineno)s - %(message)s',
        level = logging.INFO
        )


BASE_PROVIDERS = [
    'sbl.spamhaus.org',
    'xbl.spamhaus.org',
    'css.spamhaus.org',
    'pbl.spamhaus.org',
    'cbl.abuseat.org',
    'bl.spamcop.net',
    'b.barracudacentral.org',
    ]


#dsn = settings.DSN


class AsyncBlack:
    def __init__(self, data, loop=None):
        self.data = data
        self.resolver = aiodns.DNSResolver(
            ['195.63.103.144', '101.0.88.20', '208.67.220.220', '212.28.34.65', ])
        #self.dsn = dsn
        self.log = logging.getLogger("workers")
        self.provider = BASE_PROVIDERS
        self.sem = asyncio.Semaphore(1000)
        self.sql = 'UPDATE proxy_proxy SET blacklist=$1 WHERE ipreal=$2'

        self._loop = loop or asyncio.get_event_loop()
        self.black = set()
        self.dsn = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'
        self._pool = self._loop.run_until_complete(
            asyncpg.create_pool(
                dsn=self.dsn,
                #command_timeout=60*10,
                #max_size=50,
                #min_size=5,
                #max_queries=1,
                loop=self._loop)) 

    
    def build_query(self, ip):
        resurces = []
        for i in self.provider:
            reverse = '.'.join(reversed(ip.split('.')))
            resurces.append('{reverse}.{provider}'.format(reverse=reverse, provider=i))
        return resurces


    async def _write_db(self, ipreal, black):
        async with self._pool.acquire() as con:
            try:
                await con.execute(self.sql, black, ipreal)
            except Exception as exc:
                logging.exception(exc)
          
    
    async def checkers(self, ip):
        values = []
        fut = self._loop.run_in_executor(None, self.build_query, ip)
        val = await fut
        for i in val:
            async with self.sem:
                async with timeout(10):
                    try:
                        ips = await self.resolver.query(i, 'A')
                        values.append('.'.join(i.split('.')[4:]))
                    except aiodns.error.DNSError:
                        pass
                    except asyncio.TimeoutError:
                        pass
                    except asyncio.CancelledError:
                        pass

        if values:
            black = " ".join(blk for blk in values)
            await self._write_db(ip, black)


    
    def _bootstrap(self):
        logging.info(" > старт <")
        logging.info(len(self.data))
        start = time.time()
        loop = asyncio.get_event_loop()
        data = [ip for ip in self.data if ip]
        tasks = [self.checkers(ip) for ip in data]
        loop.run_until_complete(asyncio.gather(*tasks))
        
        now = time.time()
        logging.info('********** FINISH **********')
        logging.info(now - start)



    def run(self):
        logging.info('run')
        self._bootstrap()
       

class Server:
    def __init__(self):
        self.address = ('', 55555)
        self.serv = Listener(self.address, authkey=b'Jvtkmxtyrj1984')

    def _bootstrap(self):
        while True:
            try:
                logging.info("ждем коннекта")
                conn = self.serv.accept()
                try:
                    res = conn.recv()
                    work = AsyncBlack(res)
                    proc = multiprocessing.Process(target=work.run)
                    proc.start()
                    logging.info("ждем")
                    proc.join()
                    
                except EOFError as exc:
                    logging.exception(exc)
                conn.close()
            except OSError:
                continue
            except AuthenticationError:
                continue
            except IOError:
                continue

    def run(self):
        self._bootstrap()

if __name__ == '__main__':
    daemonize(stdout='/tmp/server.log',stderr='/tmp/servererror.log')
    serv = Server()
    serv.run()