import logging
import logging.handlers

class EmailHandler(logging.handlers.SMTPHandler):
    def emit(self, record):
        try:
            import smtplib
            import string
            try:
                from email.utils import formatdate
            except ImportError:
                formatdate = self.date_time
            port = self.mailport
            if not port:
                port = smtplib.SMTP_PORT
            smtp = smtplib.SMTP(self.mailhost, port)
            msg = self.format(record)
            msg = "From: %s\r\nTo: %s\r\nSubject: %s\r\nDate: %s\r\n\r\n%s" % (
                            self.fromaddr,
                            #string.join(self.toaddrs, ","),
                            self.toaddrs,
                            self.getSubject(record),
                            formatdate(), msg)
            
            if self.username:
                smtp.ehlo()
                smtp.starttls()
                smtp.ehlo()
                smtp.login(self.username, self.password)
            smtp.sendmail(self.fromaddr, self.toaddrs, bytes(msg, 'utf-8'))
            smtp.quit()
        
        except (KeyboardInterrupt, SystemExit):
            pass
            
        except:
            self.handleError(record)