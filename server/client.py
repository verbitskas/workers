import logging
import os
import sys
import multiprocessing
from multiprocessing.connection import Client, AuthenticationError

import asyncio
import aiopg
import django


sys.path.insert(0, os.path.dirname("../" + __file__))
sys.path.append('/home/john/proxyproject')
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

django.setup()
from django.conf import settings

logging.basicConfig(
    #filename='client.log',
    format="%(levelname)-10s %(lineno)d %(asctime)s %(message)s",
    level=logging.INFO
)





class ClientWorker:
    def __init__(self, server, data):
        self.server = server
        self.data = data

    def _botstrap(self):
            try:
                conn = Client((self.server, 55555), authkey=b'Jvtkmxtyrj')
            except AuthenticationError as exc:
                logging.info("ошибка авторизации")
                return
            except socket.error as exc:
                logging.info(exc)
                return
            except IOError as exc:
                logging.info(exc)
                return
            
            conn.send((self.data))
            #res = conn.recv()
            #logging.info(res)
            conn.close()
        
    def run(self):
        self._botstrap()

def main(ip, obj):
    cli = ClientWorker(ip, obj)
    cli.run()


def ol():
    @asyncio.coroutine
    def read():
        dsn = settings.DSN
        pool = yield from aiopg.create_pool(dsn)
        with (yield from pool.cursor()) as cur:
            yield from cur.execute('SELECT ipreal FROM proxy_proxy WHERE checkers = True')
            res = yield from cur.fetchall()
            return res

    
    loop = asyncio.get_event_loop()
    res = loop.run_until_complete(read())
    return res

def chunks(lst, count):
    """Группировка элементов последовательности по count элементов"""
    start = 0
    for i in range(count):
        stop = start + len(lst[i::count])
        yield lst[start:stop]
        start = stop

if __name__ == '__main__':
    #daemonize(stdout='/tmp/clien.log',stderr='/tmp/clienterror.log')
    ip = settings.WORKERS_BLACK
    res = ol()
    blacklist = [x[0] for x in res]
    results = chunks(blacklist, len(ip))
    for i, data in zip(ip, results):
        logging.info(i)
        main(i, data)


