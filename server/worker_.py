#-*-coding:utf-8-*-
import asyncio
import os
import sys
import time
import logging
import concurrent.futures
import multiprocessing
from multiprocessing.connection import Listener, AuthenticationError

import aiodns
import aiopg

import django

from daemonize import daemonize

sys.path.insert(0, os.path.dirname("../" + __file__))
sys.path.append('/home/john/proxyproject')
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

django.setup()
from django.conf import settings


logging.basicConfig(
        filename = "black-list-scan.log",
        format = '%(asctime)s - %(name)s - %(levelname)s -%(lineno)s - %(message)s',
        level = logging.INFO
        )


BASE_PROVIDERS = [
    'sbl.spamhaus.org',
    'xbl.spamhaus.org',
    'css.spamhaus.org',
    'pbl.spamhaus.org',
    'cbl.abuseat.org',
    'bl.spamcop.net',
    'b.barracudacentral.org',
    ]



#dsn = 'dbname=db_new user=django password=rikitiki1984 host=78.46.93.209'
dsn = settings.DSN


class AsyncBlack:
    def __init__(self, data, loop=None):
        self.data = data
        self.resolver = aiodns.DNSResolver(['195.63.103.144', '101.0.88.20', '208.67.220.220', '212.28.34.65', ])
        self.dsn = dsn
        self.log = logging.getLogger("workers")
        self.provider = BASE_PROVIDERS
        self.sem = asyncio.Semaphore(300)
        self.q = asyncio.Queue()
        #self.res = self.emerge([self.build_query(ip) for ip in data])
        self.loop = loop if loop else asyncio.get_event_loop()
        self.black = set()


    def emerge(self, lst):
        res = []
        for l in lst:
            res.extend(l)
        return res

    
    def build_query(self, ip):
        resurces = []
        for i in self.provider:
            reverse = '.'.join(reversed(ip.split('.')))
            resurces.append('{reverse}.{provider}'.format(reverse=reverse, provider=i))
        return resurces


    @asyncio.coroutine
    def write(self, conn, ip, blk):
        cur = yield from conn.cursor()
        try:
            yield from cur.execute('UPDATE proxy_proxy SET blacklist=%s WHERE ipreal=%s',(blk, ip))
        except concurrent.futures.TimeoutError:
            logging.info(exc)
        except Exception as exc:
            logging.info(exc)
        finally:
            cur.close()
            return

    @asyncio.coroutine
    def checkers(self, ip, loop):
        values = []
        fut = loop.run_in_executor(None, self.build_query, ip)
        val = yield from fut
        for i in val:
            with (yield from self.sem):
                try:
                    ips = yield from asyncio.wait_for(self.resolver.query(i, 'A'), timeout=10)
                    values.append('.'.join(i.split('.')[4:]))
                except aiodns.error.DNSError as exc:
                    pass
                except asyncio.TimeoutError:
                    pass

        return (ip, values)

    @asyncio.coroutine
    def _bootstrap(self, loop):
        logging.info(" > старт <")
        logging.info(len(self.data))
        st = loop.time()
        coro = [self.checkers(ip, loop) for ip in self.data]
        pool = yield from aiopg.create_pool(dsn)
        with (yield from pool) as conn:
            for task in asyncio.as_completed(coro):
                ip, blk = yield from task
                if blk:
                    blk = " ".join(x for x in blk)
                    self.black.add((ip, blk))
                    yield from self.write(conn, ip, blk)
                #else:
                #    blk = None
                #    yield from self.write(conn, ip, blk)

            conn.close()
        
        now = loop.time()
        logging.info('********** FINISH **********')
        logging.info(now - st)



    def run(self):
        logging.info('run')
        loop = asyncio.get_event_loop()
        try:
            loop.run_until_complete(
                asyncio.wait_for(self._bootstrap(loop), timeout=60*50))
        except asyncio.TimeoutError:
            logging.info('время вышло')
        finally:
            loop.close()

class Server:
    def __init__(self):
        self.address = ('', 55555)
        self.serv = Listener(self.address, authkey=b'Jvtkmxtyrj1984')

    def _bootstrap(self):
        while True:
            try:
                logging.info("ждем коннекта")
                conn = self.serv.accept()
                try:
                    res = conn.recv()
                    work = AsyncBlack(res)
                    proc = multiprocessing.Process(target=work.run)
                    proc.start()
                    logging.info("ждем")
                    proc.join()
                    
                except EOFError as exc:
                    logging.exception(exc)
                conn.close()
            except OSError:
                continue
            except AuthenticationError:
                continue
            except IOError:
                continue

    def run(self):
        self._bootstrap()

if __name__ == '__main__':
    daemonize(stdout='/tmp/server.log',stderr='/tmp/servererror.log')
    serv = Server()
    serv.run()