#-*-coding:utf-8 -*-
from django.apps import AppConfig


class RegistrationConfig(AppConfig):
    name = 'registration'
    verbose_name = "Регистрация"
