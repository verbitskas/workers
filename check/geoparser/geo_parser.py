import asyncio

import pygeoip
from geolib._rev import IP2Location

from time_zone import gtm



obj = IP2Location.IP2Location()
obj.open('geolib/geo.bin')

gi = pygeoip.GeoIP('geolib/GeoLiteCity.dat',flags=pygeoip.const.GEOIP_MEMORY_CACHE)


def parser(ip):
	rec = obj.get_all(ip)
	if hasattr(rec, 'city'):
		city = rec.city.decode("utf-8")
	else:
		city = None
	if hasattr(rec, 'country_long'):
		country_name = rec.country_long.decode("utf-8")
	else:
		country_name = None
	if hasattr(rec, 'country_short'):
		country_code = rec.country_short.decode("utf-8")
	else:
		country_code = None
	if hasattr(rec, 'latitude') and hasattr(rec, 'longitude'):
		latitude, longitude = rec.latitude, rec.longitude
	else:
		latitude, longitude = None, None
	if hasattr(rec, 'zipcode'):
		postal_code = rec.zipcode.decode("utf-8")
	else:
		postal_code = None
	if hasattr(rec, 'region'):
		region = rec.region.decode("utf-8")
	else:
		region = None
	if hasattr(rec, 'domain'):
		domain = rec.domain.decode("utf-8")
	else:
		domain = None

	continent = gi.record_by_addr(ip).get('continent', None)
	region_code = gi.record_by_addr(ip).get('region_code', None)
	time_zone = gi.record_by_addr(ip).get('time_zone', None) 
	gmt,time_region = gtm(time_zone)

	return (
		country_name, city, continent, country_code,
		latitude, longitude, postal_code,region_code,
		time_zone, gmt, time_region,domain, region,
		)






'''
gevent.spawn(
	self.pool.execute, """
			INSERT INTO proxy_proxy (
				worker_id, vendor_id, ip, port, ipreal,
				country, city, continent, country_code, lat,
				lon, postal_code, region_code, time_zone, tp,
				anonymity, checkers, timeout, gmt, time_region,
				created, update, typeproxy, auth, usr, pswd,
				domain, region) 
			VALUES (
				%s,%s,%s,%s,%s,
				%s,%s,%s,%s,%s,
				%s,%s,%s,%s,%s,
				%s,%s,%s,%s,%s,
				now(), now(), %s,%s,%s,%s,
				%s,%s)
			ON CONFLICT (ipreal, vendor_id) DO UPDATE
				SET port = (%s), update = now()""" % proxy)
'''