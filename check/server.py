#coding:utf-8
import sys
import time
import os
import multiprocessing
from multiprocessing.connection import Listener, AuthenticationError
import logging
import resource

logging.basicConfig(
    #filename='server.log',
    format="%(name)s %(pathname)s %(filename)s строка:%(lineno)d %(asctime)s %(message)s",
    level=logging.INFO
)

log = logging.getLogger('server')

# тестинг
from scaner import func

# импорт демонизации сервера


# импорт чекеров
from scaner import AsyncSock5Geo
#from scaner import AsyncSock5Online
#from scaner import AsyncSock5GeoBp
#from scaner import AsyncSock5OnlineBp

#from scaner import AsyncSock4Geo
#from scaner import AsyncSock4Online
#from scaner import AsyncSock4GeoBp
#from scaner import AsyncSock4OnlineBp

#from scaner import AsyncHttpGeo
#from scaner import AsyncHttpOnline


# импорт настроек
password = b"password"

class BaseServer:
    def __init__(self):
        self.log = log


    def _sock4(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_sock4"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()
        


    def _sock4_bc(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_sock4_bc"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()


    def _sock5(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_sock5"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()
        #while proc.is_alive():
        #    print("тут зависли")
        #    time.sleep(6)
        #print("проснулись")


    def _sock5_bc(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_sock5_bc"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()


    def _http(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_http"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()


    def _http_auth(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_http_auth"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()


    def _sock5_auth(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_sock5_auth"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()


    def _sock4_no(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_sock4_no"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()


    def _sock5_no(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_sock5_no"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()


    def _http_no(self, data=None):
        #if not isinstance(data, (tuple, list)):
        #    return
        msg = "_http_no"
        proc = multiprocessing.Process(target=func, args=(msg,))
        proc.start()





class Server(BaseServer):
    def __init__(self):
        super().__init__()
        self.address = ('0.0.0.0', 65100)
        self.server = Listener(self.address, authkey=password)

    def _bootstrap(self):
        self.log.info('запущен')
        while True:
            try:
                conn = self.server.accept()
                while True:
                    try:
                        res = conn.recv()
                        self.log.info("получили данные")
                        if res[0] == 'sock4':
                            #self.log.info('sock4')
                            self._sock4()
                        
                        elif res[0] == 'sock4_bc':
                            #self.log.info('sock4_bc')
                            self._sock4_bc()

                        elif res[0] == 'sock5':
                            #self.log.info('сокс5')
                            print(type(res[0]))
                            self._sock5()

                        elif res[0] == 'socks5_bc':
                            #self.log.info('сокс5_bc')
                            self._sock5_bc()

                        elif res[0] == 'http':
                            #self.log.info('http/https')
                            self._http()

                        elif res[0] == 'http_auth':
                            #self.log.info('http_auth')
                            self._http_auth()

                        elif res[0] == 'sock5_auth':
                            #self.log.info('sock5_auth')
                            self._sock5_auth()

                        elif res[0] == 'sock4_no':
                            #self.log.info('sock4_no')
                            self._sock4_no()

                        elif res[0] == 'sock5_no':
                            #self.log.info('sock5_no')
                            self._sock5_no()

                        elif res[0] == 'http_no':
                            #self.log.info('http_no')
                            self._http_no()

                        else:
                            pass
                    except EOFError as exc:
                        self.log.exception(exc)
                        break
                    #log.info(res[0])
                    result = res
                    conn.send(result)
                conn.close()
                self.log.inf("закрыли")
            except OSError:
                self.log.info("OSError")
                conn.close()
                continue
            except AuthenticationError:
                pass
            except IOError:
                continue

    def run(self):
        self._bootstrap()

if __name__ == '__main__':
    #daemonize(stdout='/tmp/server.log',stderr='/tmp/servererror.log')
    #try:
    #    resource.setrlimit(resource.RLIMIT_NOFILE, (100000, 100000))
    #except:
    #    pass

    try:
        serv = Server()
        serv.run()
    except KeyboardInterrupt:
        pass