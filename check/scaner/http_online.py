import asyncio
import aiohttp
import aiopg
import asyncpg
from asyncpg.exceptions import TooManyConnectionsError, ConnectionDoesNotExistError
import aiosocks
from aiosocks.connector import ProxyConnector, ProxyClientRequest
from async_timeout import timeout
import logging
import socket
import sys
from struct import pack, unpack
import datetime

from fields import http_online_true as sql_true
from fields import http_online_false as sql_false

from settings import IP, DSN, SERVER_CHECK_HTTP, PORT_CHECK_HTTP

logging.basicConfig(
	filename='log/http.log',
	format="%(levelname)-10s строка %(lineno)d '|' %(asctime)s %(message)s",
	level=logging.INFO
)



	

class AsyncHttpOnline:
	def __init__(self, SQL, loop=None):
		self._sem  = asyncio.BoundedSemaphore(1000)
		self.server_check = SERVER_CHECK_HTTP
		self.port_check = PORT_CHECK_HTTP
		self._loop = loop or asyncio.get_event_loop()
		self.sql = SQL
		self.sql_true = sql_true
		self.sql_false = sql_false
		self.local_ip = IP
		self.dsn = DSN
		self._pool = self._loop.run_until_complete(
			asyncpg.create_pool(
				dsn=self.dsn,
				max_size=5,
				min_size=2,
				max_queries=1,
				loop=self._loop))
		self._queue_true = asyncio.Queue()
		self._queue_false = asyncio.Queue()


	async def _read_db(self):
		async with aiopg.create_pool(self.dsn) as pool:
			async with pool.acquire() as conn:
				async with conn.cursor() as cur:
					await cur.execute(self.sql)
					res = await cur.fetchall()
					return res


	async def _write_db_many(self, sql, objlist):
		async with self._pool.acquire() as con:
			try:
				await con.executemany(sql, objlist)
			except ConnectionDoesNotExistError:
				logging.info("ошибка в бд в execute")
			except TooManyConnectionsError as exc:
				logging.exception(exc)
			except Exception as exc:
				logging.exception(exc)


	async def _write_db(self, sql, checker, anonymity, ip, port, id):
		con = await self._pool.acquire()
		try:
			await con.execute(sql, checker, anonymity, ip, port, id)
		except TooManyConnectionsError as exc:
			logging.exception(exc)
		except ConnectionDoesNotExistError as exc:
			logging.exception(exc)
		finally:
			await self._pool.release(con)

			

	async def sock(self, obj):
		ip, port, id = obj
		anonymity = 'no'
		async with self._sem:
			try:
				connector = ProxyConnector(remote_resolve=False)
				async with timeout(15):
					async with aiohttp.ClientSession(connector=connector, request_class=ProxyClientRequest) as session:
						async with session.get('http://{}:{}'.format(self.server_check, self.port_check), \
								proxy='http://{ip}:{port}'.format(ip=ip, port=port), proxy_auth=None) as resp:
							if resp.status == 200:
								data = await resp.text()
								if data != self.local_ip:
									anonymity = 'yes'
								return (True, anonymity, ip, port, id)
							else:
								return (False, anonymity, ip, port, id)
			except Exception:
			    return (False, anonymity, ip, port, id)
		

	async def _resp_socks(self, url):
		async with aiohttp.ClientSession() as session:
			async with session.get(url) as resp:
				data = await resp.text()
				return data



	async def _bootstrap(self, loop):
		data = await self._read_db()
	
		if not data:
			logging.info("не чего сканировать")
			return

		logging.info("всего на скан %s", len(data))

		restore = []

		tasks = [self.sock(obj) for obj in data]
		for task in asyncio.as_completed(tasks):
			res = await task
			if res[0]:
				await self._write_db(self.sql_true, *res)
			else:
				restore.append((res[2], res[3], res[4]))
			
		logging.info("ПОВТОР")

		tasks = [loop.create_task(self.sock(obj)) for obj in restore]
		for task in asyncio.as_completed(tasks):
			res = await task
			if res[0]:
				await self._queue_true.put(res)
				#await self._write_db(self.sql_true, *res)
			else:
				await self._queue_false.put(res)
				#await self._write_db(self.sql_false, *res)

		logging.info("запись")
		writer_true = []
		writer_false = []
		for i in range(self._queue_true.qsize()):
			writer_true.append(self._queue_true.get_nowait())
		await self._write_db_many(self.sql_true, writer_true)

		for i in range(self._queue_false.qsize()):
			writer_false.append(self._queue_false.get_nowait())
		await self._write_db_many(self.sql_false, writer_false)


		#res = loop.run_until_complete(asyncio.gather(*tasks))
		
		#socks_true = []
		#socks_false = []
		#restor_socks_scan = []

		#for obj in res:
		#	if obj[0]:
		#		socks_true.append(self._write_db(self.sql_true, *obj))
		#	else:
		#		restor_socks_scan.append(obj[2:])
		#		#restor_socks_scan.append("{}:{}".format(obj[2],obj[3]))

		#loop.run_until_complete(asyncio.gather(*socks_true))

		#socks_true = []
		#socks_false = []

		#tasks = [loop.create_task(self.sock(obj)) for obj in restor_socks_scan]
		#res = loop.run_until_complete(asyncio.gather(*tasks))

		#for obj in res:
		#	if obj[0]:
		#		socks_true.append(self._write_db(self.sql_true, *obj))
		#	else:
		#		socks_false.append(self._write_db(self.sql_false, *obj))

		#loop.run_until_complete(asyncio.gather(*socks_true))
		#loop.run_until_complete(asyncio.gather(*socks_false))
		#loop.run_until_complete(self._pool.close())


	def go(self):
		loop = asyncio.get_event_loop()
		try:
			loop.run_until_complete(asyncio.wait_for(self._bootstrap(loop), timeout=60*15))
		except asyncio.TimeoutError:
			logging.info('время вышло')
		finally:
			loop.run_until_complete(self._pool.close())
			#loop.close()