import asyncio
import logging
import pygeoip
from geoparser.geolib._rev import IP2Location

from .time_zone import gtm


logging.basicConfig(
	filename='log/socks_geo.log',
	format="%(levelname)-10s %(lineno)d %(asctime)s %(message)s",
	level=logging.NOTSET
)

log = logging.getLogger('geo')


obj = IP2Location.IP2Location()
obj.open('geoparser/geolib/geo.bin')

gi = pygeoip.GeoIP('geoparser/geolib/GeoLiteCity.dat',flags=pygeoip.const.GEOIP_MEMORY_CACHE)


def parser_bp(ip, port, ipreal, worker_id, vendor_id, time_out, typeproxy, typesocks, anonymity, checkers, auth, scan):
	rec = obj.get_all(ipreal.strip())
	if hasattr(rec, 'city'):
		city = rec.city.decode("utf-8")
	else:
		city = None
	if hasattr(rec, 'country_long'):
		country = rec.country_long.decode("utf-8")
	else:
		country = None
	if hasattr(rec, 'country_short'):
		country_code = rec.country_short.decode("utf-8")
	else:
		country_code = None
	if hasattr(rec, 'latitude') and hasattr(rec, 'longitude'):
		lat, lon = rec.latitude, rec.longitude
	else:
		lat, lon = None, None
	if hasattr(rec, 'zipcode'):
		postal_code = rec.zipcode.decode("utf-8")
	else:
		postal_code = None
	if hasattr(rec, 'region'):
		region = rec.region.decode("utf-8")
	else:
		region = None
	if hasattr(rec, 'domain'):
		domain = rec.domain.decode("utf-8")
	else:
		domain = None
	try:
		continent = gi.record_by_addr(ip).get('continent', None)
	except AttributeError:
		continent = None
	try:
		region_code = gi.record_by_addr(ip).get('region_code', None)
	except AttributeError:
		region_code = None
	try:
		time_zone = gi.record_by_addr(ip).get('time_zone', None) 
	except AttributeError:
		time_zone = None

	gmt,time_region = gtm(time_zone)

	return (
		worker_id, vendor_id, ip, port, ipreal,time_out,
		country, city, continent, country_code,
		lat, lon, postal_code,region_code,
		time_zone, gmt, time_region,domain, region,
		typeproxy, typesocks, anonymity, checkers,
		auth, scan, port, ipreal, ip
		)