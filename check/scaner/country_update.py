import asyncio
import uvloop
import aiohttp
import aiopg
import asyncpg
from asyncpg.exceptions import TooManyConnectionsError, ConnectionDoesNotExistError
from async_timeout import timeout
import logging
import socket
import sys
import re
from struct import pack, unpack

#from geo_parser import parser
from geo_country import func
from fields import update_geo as sql


logging.getLogger('geo.socks5')
logging.DEBUG



query = '''
	SELECT v.typeproxy, v.tp, v.link, v.user_id, v.worker_id
	FROM vendor_vendors as v, proxy_worker as w 
	WHERE v.worker_id=w.id 
	AND w.ip='185.235.245.14' 
	AND v.tp='socks5' AND v.scan=False;
	'''

class AsyncSock5:
	def __init__(self):

		self._loop = asyncio.get_event_loop()
		self._sem = asyncio.BoundedSemaphore(1000)
		self.dsn = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'

		self._pool = self._loop.run_until_complete(
			asyncpg.create_pool(
				dsn=self.dsn,
				#command_timeout=60*10,
				max_size=5,
				min_size=2,
				max_queries=1,
				loop=self._loop))



	async def _read_db(self):
		async with aiopg.create_pool(self.dsn) as pool:
			async with pool.acquire() as conn:
				async with conn.cursor() as cur:
					await cur.execute("SELECT ip, ipreal FROM proxy_proxy")
					res = await cur.fetchall()
					return res



	async def _write_db(self, content):
		logging.info("пишем")
		async with self._pool.acquire() as con:
			try:
				await con.execute(sql, *content)
			except asyncio.CancelledError:
				logging.info("ошибка")



	async def _geo(self, ip, ipreal):
		async with self._sem:
			try:
				async with timeout(15):
					fut = self._loop.run_in_executor(None, func, ip, ipreal)
					content = await fut
					await self._write_db(content)
			except Exception as exc:
				logging.exception(exc)



	async def _bootstrap(self, loop):
		data_db = await self._read_db()
		logging.info("создания tasks")
		tasks = [loop.create_task(self._geo(*obj)) for obj in data_db]

		for task in asyncio.as_completed(tasks):
			res = await task

		logging.info("всего %s", len(data_db))

	def run(self):
		loop = asyncio.get_event_loop()
		try:
			loop.run_until_complete(asyncio.wait_for(self._bootstrap(loop), timeout=600*15))
		except asyncio.TimeoutError:
			logging.info('время вышло')
		finally:
			loop.run_until_complete(self._pool.close())
			#loop.close()




def main():
	import time
	start = time.time()
	checker = AsyncSock5()
	checker.run()
	end = time.time() - start
	logging.info("время затраченое на ГЕО скан %s мин", int(end)/60)

if __name__ == '__main__':
	main()