import asyncio
import aiohttp
import aiopg
import asyncpg
from asyncpg.exceptions import TooManyConnectionsError, ConnectionDoesNotExistError
import aiosocks
from async_timeout import timeout
import logging
import socket
import re
import sys
from struct import pack, unpack
import datetime

import maxminddb

from fields import sql4_online_bp_true as sql_true
from fields import sql4_online_bp_false as sql_false

#from geo_parser_bp import parser
from geo_parser_bp_mixmand import parser_online
from fields import sql4_write_bp_isp_new as sql_online_geo

from settings import IP, DSN, SERVER_CHECK, PORT_CHECK


logging.basicConfig(
	filename='log/sock5bc.log',
	format="%(levelname)-10s строка %(lineno)d '|' %(asctime)s %(message)s",
	level=logging.INFO
)




class AsyncSockBCOnline:
	def __init__(self, SQL, loop=None):
		self._sem  = asyncio.BoundedSemaphore(1000)
		self.dst = (SERVER_CHECK, PORT_CHECK)
		self._loop = loop or asyncio.get_event_loop()
		self.sql = SQL
		self.sql_true = sql_true
		self.sql_false = sql_false
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
		self.local_ip = IP
		self.dsn = DSN

		self._pool = self._loop.run_until_complete(
			asyncpg.create_pool(
				dsn=self.dsn,
				max_size=5,
				min_size=2,
				max_queries=1,
				loop=self._loop))
		self._queue_true = asyncio.Queue()
		self._queue_false = asyncio.Queue()



	async def _read_db(self):
		async with aiopg.create_pool(self.dsn) as pool:
			async with pool.acquire() as conn:
				async with conn.cursor() as cur:
					await cur.execute(self.sql)
					res = await cur.fetchall()
					return res


	#async def _write_db_many(self, sql, objlist):
	#	async with self._pool.acquire() as con:
	#		try:
	#			await con.executemany(sql, objlist)
	#		except ConnectionDoesNotExistError:
	#			logging.info("ошибка в бд в execute")
	#		except TooManyConnectionsError as exc:
	#			logging.exception(exc)
	#		except Exception as exc:
	#			logging.exception(exc)


	async def _write_db(self, content):
		async with self._pool.acquire() as con:
			try:
				await con.execute(sql_online_geo, *content)
			except asyncio.CancelledError as exc:
				logging.exception(exc)
			except asyncpg.exceptions.UniqueViolationError:
				await con.execute("DELETE FROM proxy_proxy WHERE id=$1", content[-1])
				await con.execute(sql_online_geo, *content)
			except Exception as exc:
				logging.exception(exc)


	async def _write_db_false(self, sql, checker, anonymity, ipreal, ip, port, sid):
		async with self._pool.acquire() as con:
			try:
				await con.execute(sql, checker, anonymity, ipreal, ip, port, sid)
			except TooManyConnectionsError as exc:
				logging.exception(exc)
			except ConnectionDoesNotExistError as exc:
				logging.exception(exc)
			except Exception as exc:
				logging.exception(exc)
			#	try:
			#		await con.execute("UPDATE proxy_proxy SET ipreal=$1 WHERE ipreal=$2", None, ipreal)
			#	except Exception as exc:
			#		logging.exception(exc)
			#		try:
			#			await con.execute(sql, checker, anonymity, ipreal, ip, port, sid)
			#		except Exception as exc:
			#			logging.exception(exc)

			

	async def sock(self, obj):
		ip, port, sid = obj
		anonymity = 'yes'
		checker = False
		ipreal = ip
		async with self._sem:
			try:
				socks5_addr = aiosocks.Socks5Addr(ip, int(port))
				async with timeout(10):
					reader, writer = await aiosocks.open_connection(
						proxy=socks5_addr, proxy_auth=None, dst=self.dst)
					data = await reader.read(1024)
					if data:
						checker = True
						try:
							ipreal = self.pattern.findall(data.decode('latin1'))[0]
						except UnicodeDecodeError as exc:
							writer.close()
							return (False, anonymity, None, ip, port, id)
					else:
						checker = False
					writer.close()
					fut = self._loop.run_in_executor(
						None, parser_online, ip, port, ipreal,
						anonymity, checker, sid)

					content = await fut
					await self._write_db(content)

			except Exception:
			    await self._write_db_false(self.sql_false, False, anonymity, None, ip, port, sid)
		

	async def _resp_socks(self, url):
		async with aiohttp.ClientSession() as session:
			async with session.get(url) as resp:
				data = await resp.text()
				return data



	async def _bootstrap(self, loop):
		data = await self._read_db()
		logging.info("всего на скан bp соксов %s", len(data))
	
		if not data:
			return

		#restore = []

		logging.info("запуск")
		tasks = [self.sock(obj) for obj in data]
		#loop.run_until_complete(asyncio.gather(*tasks))

		for task in asyncio.as_completed(tasks):
			try:
				res = await task
			except Exception as exc:
				logging.exception(exc)
		#	if res[0]:
		#		await self._write_db(self.sql_true, *res)
		#	else:
		#		restore.append((res[3], res[4], res[5]))
			
		#logging.info("ПОВТОР")

		#tasks = [loop.create_task(self.sock(obj)) for obj in restore]
		#for task in asyncio.as_completed(tasks):
		#	res = await task
		#	if res[0]:
		#		await self._write_db(self.sql_true, *res)
		#	else:
		#		await self._write_db(self.sql_false, *res)

		#res = loop.run_until_complete(asyncio.gather(*tasks))

		#socks_true = []
		#restor_socks_scan = []

		#for obj in res:
		#	if obj[0]:
		#		socks_true.append(self._write_db(self.sql_true, *obj))
		#	else:
		#		restor_socks_scan.append(obj[3:])

		#logging.info("запись")		
		#loop.run_until_complete(asyncio.gather(*socks_true))

		#socks_true = []
		#socks_false = []

		#logging.info("повторный скан")

		#tasks = [self.sock(obj) for obj in restor_socks_scan]
		#res = loop.run_until_complete(asyncio.gather(*tasks))

		#for obj in res:
		#	if obj[0]:
		#		socks_true.append(self._write_db(self.sql_true, *obj))
		#	else:
		#		socks_false.append(self._write_db(self.sql_false, *obj))

		#logging.info("запись")
		#loop.run_until_complete(asyncio.gather(*socks_true))
		#loop.run_until_complete(asyncio.gather(*socks_false))

		#loop.run_until_complete(self._pool.close())
		#loop.close()


	def go(self):
		loop = asyncio.get_event_loop()
		try:
			loop.run_until_complete(asyncio.wait_for(self._bootstrap(loop), timeout=60*15))
		except asyncio.TimeoutError:
			logging.info('время вышло')
		finally:
			loop.run_until_complete(self._pool.close())
			#loop.close()