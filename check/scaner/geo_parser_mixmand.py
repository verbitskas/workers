import asyncio
import logging
import re

import maxminddb

from time_zone import gtm



_city    = maxminddb.open_database('../maxmind/city.mmdb', mode=maxminddb.MODE_MEMORY)
_isp     = maxminddb.open_database('../maxmind/isp.mmdb', mode=maxminddb.MODE_MEMORY)
_domain  = maxminddb.open_database('../maxmind/domain.mmdb', mode=maxminddb.MODE_MEMORY)
_country = maxminddb.open_database('../maxmind/country.mmdb', mode=maxminddb.MODE_MEMORY)

pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")

def parser_maxminddb(ip, port, ipreal, worker_id, vendor_id, time_out, typeproxy, typesocks, anonymity, checkers, auth, scan):
	try:
		try:
			ipreal_ = pattern.findall(ipreal)[0]
		except IndexError:
			ipreal_ = ip.strip()
		rec_city    = _city.get(ipreal_)
		rec_isp     = _isp.get(ipreal_)
		rec_domain  = _domain.get(ipreal_)
	except Exception:
		return
	# maxmanddb
	try:
		city = rec_city.get('city').get('names').get('en')
	except Exception:
		city = None
	try:
		country = rec_city.get('country').get('names').get('en')
	except Exception:
		country = None
	try:
		country_code = rec_city.get('registered_country').get('iso_code')
	except Exception:
		country_code = None
	try:
		lat, lon = rec_city.get('location').get('latitude'), rec_city.get('location').get('longitude')
	except Exception:
		lat, lon = None, None
	try:
		postal_code = rec_city.get('postal').get('code')
	except Exception:
		postal_code = None
	try:
		region = rec_city.get('subdivisions')[0].get('names').get('en')
	except Exception:
		region = None
	try:
		domain = rec_domain.get('domain')
	except Exception:
		domain = None
	try:
		continent = rec_city.get('continent').get('names').get('en')
	except Exception:
		continent = None
	region_code = None 
	try:
		time_zone = rec_city.get('location').get('time_zone')
	except Exception:
		time_zone = None
	try:
		isp = rec_isp.get('isp')
	except Exception:
		isp = None
	try:
		gmt,time_region = gtm(time_zone)
	except Exception:
		gmt, time_region = None, None
	return (
		worker_id, vendor_id, ip, port, ipreal, time_out,
		country, city, continent, country_code,
		lat, lon, postal_code,region_code,
		time_zone, gmt, time_region,domain, region,
		typeproxy, typesocks, anonymity, checkers,
		auth, scan, isp, port
		)