import asyncio
import uvloop
import aiohttp
import aiopg
import asyncpg
from asyncpg.exceptions import TooManyConnectionsError, ConnectionDoesNotExistError
from async_timeout import timeout
import logging
import socket
import sys
import re
from struct import pack, unpack

#from geo_parser import parser
from .geo_parser_mixmand import parser_maxminddb as parser
from .fields import sql5_write_isp as sql


logging.getLogger('geo.socks5')


#asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

query = '''
	SELECT v.typeproxy, v.tp, v.link, v.user_id, v.worker_id, v.auth, v.scan
	FROM vendor_vendors as v, proxy_worker as w 
	WHERE v.worker_id=w.id 
	AND w.ip='185.235.245.14' 
	AND v.tp='socks5' AND v.scan=True;
	'''

class AsyncNoSock5:
	def __init__(self, typeproxy, typesocks, url, vendor_id, worker_id, auth=False, scan=False):
		
		self.typeproxy = typeproxy
		self.typesocks = typesocks
		self.url = url
		self.vendor_id = vendor_id
		self.worker_id = worker_id
		self.auth = auth
		self.scan = scan

		self._loop = asyncio.get_event_loop()
		self._sem = asyncio.BoundedSemaphore(1000)
		self.dsn = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'
		self.local = '185.235.245.17'
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}")
		self._pool = self._loop.run_until_complete(
			asyncpg.create_pool(
				dsn=self.dsn,
				#command_timeout=60*10,
				max_size=5,
				min_size=2,
				max_queries=1,
				loop=self._loop))



	async def _read_db(self):
		async with aiopg.create_pool(self.dsn) as pool:
			async with pool.acquire() as conn:
				async with conn.cursor() as cur:
					await cur.execute("SELECT ip, port FROM proxy_proxy WHERE tp='socks5'")
					res = await cur.fetchall()
					return res




	async def _write_db(self, content):
		async with self._pool.acquire() as con:
			try:
				await con.execute(sql, *content)
			except asyncio.CancelledError:
				pass



	async def sock(self, obj):
		ip , port = obj.split(":")
		async with self._sem:
			try:
				async with timeout(15):
					time_out, anonymity, checkers, ipreal = None, 'yes', True, ip
					fut = self._loop.run_in_executor(
						None, parser, ip, port, ipreal,
						self.worker_id, self.vendor_id,
						time_out, self.typeproxy, self.typesocks,
						anonymity, checkers, self.auth, self.scan)

					content = await fut
					await self._write_db(content)
			except asyncio.TimeoutError:
				#anonymity, checkers, ipreal = 'no', False, ip
				#fut = self._loop.run_in_executor(
				#	None, parser, ip, port, ipreal,
				#	self.worker_id, self.vendor_id,
				#	None, self.typeproxy, self.typesocks,
				#	anonymity, False, self.auth, self.scan)

				#content = await fut
				#await self._write_db(content)
				logging.info("timeout error")



	async def _resp_socks(self, url):
		async with aiohttp.ClientSession() as session:
			async with session.get(url) as resp:
				data = await resp.text()
				return data




	async def _bootstrap(self, loop):
		data_db = await self._read_db()
		data_db = ['{}:{}'.format(x[0], x[1]) for x in data_db]

		data = await self._resp_socks(self.url)
		data = data.split()
		
		data = [obj for obj in data if obj not in data_db]

		response = [url for url in data if self.pattern.findall(url)]

		if not response:
			return
		
		logging.info("%s", len(response))

		tasks = [self.sock(obj) for obj in response]
		for task in asyncio.as_completed(tasks):
			res = await task

		#loop.run_until_complete(asyncio.gather(*tasks))
		#loop.run_until_complete(self._pool.close())
		#loop.close()

	def run(self):
		loop = asyncio.get_event_loop()
		try:
			loop.run_until_complete(asyncio.wait_for(self._bootstrap(loop), timeout=60*15))
		except asyncio.TimeoutError:
			logging.info('время вышло')
		finally:
			loop.run_until_complete(self._pool.close())
			loop.close()


def do_main():
	dsn = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'
	async def _read_db():
		async with aiopg.create_pool(dsn) as pool:
			async with pool.acquire() as conn:
				async with conn.cursor() as cur:
					await cur.execute(query)
					res = await cur.fetchall()
					return res
	loop = asyncio.get_event_loop()
	res = loop.run_until_complete(_read_db())
	return res


def main(obj):
	logging.info("скан только ГЕО")
	import time
	start = time.time()
	checker = AsyncSock5(*obj)
	checker.run()
	end = time.time() - start
	logging.info("время затраченое на ГЕО скан %s мин", int(end)/60)
	logging.info("END ГЕО")

if __name__ == '__main__':
	res = do_main()
	for obj in res:
		main(obj)