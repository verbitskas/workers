import asyncio
import logging

import maxminddb

from time_zone import gtm



# import pygeoip
# from geoparser.geolib._rev import IP2Location
# obj = IP2Location.IP2Location()
# obj.open('geoparser/geolib/geo.bin')
# gi = pygeoip.GeoIP('geoparser/geolib/GeoLiteCity.dat',flags=pygeoip.const.GEOIP_MEMORY_CACHE)

_city    = maxminddb.open_database('../maxmind/city.mmdb', mode=maxminddb.MODE_MEMORY)
_isp     = maxminddb.open_database('../maxmind/isp.mmdb', mode=maxminddb.MODE_MEMORY)
_domain  = maxminddb.open_database('../maxmind/domain.mmdb', mode=maxminddb.MODE_MEMORY)
_country = maxminddb.open_database('../maxmind/country.mmdb', mode=maxminddb.MODE_MEMORY)

def parser_maxminddb_bp(ip, port, ipreal, worker_id, vendor_id, time_out, typeproxy, typesocks, anonymity, checkers, auth, scan):
	# rec = obj.get_all(ipreal.strip())
	# city = rec.city.decode("utf-8")
	# country = rec.country_long.decode("utf-8")
	# country_code = rec.country_short.decode("utf-8")
	# lat, lon = rec.latitude, rec.longitude
	# postal_code = rec.zipcode.decode("utf-8")
	# region = rec.region.decode("utf-8")
	# domain = rec.domain.decode("utf-8")
	# continent = gi.record_by_addr(ip).get('continent', None)
	# region_code = gi.record_by_addr(ip).get('region_code', None)
	# time_zone = gi.record_by_addr(ip).get('time_zone', None) 
	# gmt,time_region = gtm(time_zone)
	rec_city    = _city.get(ipreal)
	rec_isp     = _isp.get(ipreal)
	rec_domain  = _domain.get(ipreal)

	# maxminddb
	try:
		city = rec_city.get('city').get('names').get('en')
	except Exception:
		city = None
	try:
		country = rec_city.get('country').get('names').get('en')
	except Exception:
		country = None
	try:
		country_code = rec_city.get('registered_country').get('iso_code')
	except Exception:
		country_code = None
	try:
		lat, lon = rec_city.get('location').get('latitude'), rec_city.get('location').get('longitude')
	except Exception:
		lat, lon = None, None
	try:
		postal_code = rec_city.get('postal').get('code')
	except Exception:
		postal_code = None
	try:
		region = rec_city.get('subdivisions')[0].get('names').get('en')
	except Exception:
		region = None
	try:
		domain = rec_domain.get('domain')
	except Exception:
		domain = None
	try:
		continent = rec_city.get('continent').get('names').get('en')
	except Exception:
		continent = None
	region_code = None 
	try:
		time_zone = rec_city.get('location').get('time_zone')
	except Exception:
		time_zone = None
	try:
		isp = rec_isp.get('isp')
	except Exception:
		isp = None
	try:
		gmt,time_region = gtm(time_zone)
	except Exception:
		gmt, time_region = None, None

	return (
		worker_id, vendor_id, ip, port, ipreal,time_out,
		country, city, continent, country_code,
		lat, lon, postal_code,region_code,
		time_zone, gmt, time_region,domain, region,
		typeproxy, typesocks, anonymity, checkers,
		auth, scan, isp, port, ipreal, ip
		)


def parser_online(ip, port, ipreal, anonymity, checkers, sid):
	rec_city    = _city.get(ipreal)
	rec_isp     = _isp.get(ipreal)
	rec_domain  = _domain.get(ipreal)

	# maxminddb
	try:
		city = rec_city.get('city').get('names').get('en')
	except Exception:
		city = None
	try:
		country = rec_city.get('country').get('names').get('en')
	except Exception:
		country = None
	try:
		country_code = rec_city.get('registered_country').get('iso_code')
	except Exception:
		country_code = None
	try:
		lat, lon = rec_city.get('location').get('latitude'), rec_city.get('location').get('longitude')
	except Exception:
		lat, lon = "", ""
	try:
		postal_code = rec_city.get('postal').get('code')
	except Exception:
		postal_code = None
	try:
		region = rec_city.get('subdivisions')[0].get('names').get('en')
	except Exception:
		region = None
	try:
		domain = rec_domain.get('domain')
	except Exception:
		domain = None
	try:
		continent = rec_city.get('continent').get('names').get('en')
	except Exception:
		continent = None
	try:
		time_zone = rec_city.get('location').get('time_zone')
	except Exception:
		time_zone = None
	try:
		isp = rec_isp.get('isp')
	except Exception:
		isp = None
	try:
		gmt,time_region = gtm(time_zone)
	except Exception:
		gmt, time_region = None, None

	return (
		ipreal, isp, country, city, 
		continent, country_code, str(lat), str(lon), 
		postal_code, time_zone, 
		str(gmt), time_region, domain, region,
		anonymity, checkers, ip, port, int(sid)
		)