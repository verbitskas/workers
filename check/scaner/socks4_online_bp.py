import asyncio
import aiohttp
import aiopg
import asyncpg
from asyncpg.exceptions import TooManyConnectionsError, ConnectionDoesNotExistError
import aiosocks
from async_timeout import timeout
import logging
import socket
import sys
from struct import pack, unpack
import datetime

from .fields import sql4_online_bp_true as sql_true
from .fields import sql4_online_bp_false as sql_false


logging.basicConfig(
	filename='log/socks5bc.log',
	format="%(levelname)-10s строка %(lineno)d '|' %(asctime)s %(message)s",
	level=logging.INFO
)

now = datetime.datetime.now()
delta =  now - datetime.timedelta(hours=24)


SQL_READ = """
	SELECT p.ip, p.port, p.id FROM proxy_proxy as p, proxy_worker as w 
	WHERE p.worker_id=w.id AND w.ip='185.235.245.14'
	AND p.worker_id=w.id AND p.tp='socks4' AND p.scan=False
	AND p.typeproxy='bp' AND p.update > TIMESTAMP '%s';""" % delta



class AsyncSock4OnlineBp:
	def __init__(self, loop=None):
		self._sem  = asyncio.BoundedSemaphore(1000)
		self.dst = ('185.235.245.17', 5566)
		self._loop = loop or asyncio.get_event_loop()
		self.sql_read = SQL_READ
		self.sql_true = sql_true
		self.sql_false = sql_false
		self.local = '185.235.245.8'
		self.dsn = 'postgresql://djwoms:Djwoms18deuj_234567hfd@185.235.245.10:6432/djproxy'
		self._pool = self._loop.run_until_complete(
			asyncpg.create_pool(
				dsn=self.dsn,
				max_size=5,
				min_size=2,
				max_queries=1,
				loop=self._loop))


	async def _read_db(self):
		async with aiopg.create_pool(self.dsn) as pool:
			async with pool.acquire() as conn:
				async with conn.cursor() as cur:
					await cur.execute(self.sql_read)
					res = await cur.fetchall()
					return res


	async def _write_db(self, sql, checker, anonymity, ipreal, ip, port, id):
		#logging.info("пишем $%s", checker)
		con = await self._pool.acquire()
		try:
			await con.execute(sql, checker, anonymity, ipreal, ip, port, id)
		except TooManyConnectionsError as exc:
			logging.exception(exc)
		except ConnectionDoesNotExistError as exc:
			logging.exception(exc)
		except Exception:
			if checker:
				#logging.info("такая связка уже есть в БД %s", (ip, port, checker))
				await con.execute('''
					UPDATE proxy_proxy 
					SET update=now() WHERE id=$1::integer''', id)
			else:
				pass
				#logging.info("такая связка уже есть в БД %s", (ip, port, checker))
				#await con.execute('''
				#	UPDATE proxy_proxy 
				#	SET checkers=$1 
				#	WHERE ipreal=$2 AND id=$3::integer''', checker, ipreal, id)
		finally:
			await self._pool.release(con)

			

	async def sock(self, obj):
		ip, port, id = obj
		anonymity = 'yes'
		async with self._sem:
			try:
				socks4_addr = aiosocks.Socks4Addr(ip, int(port))
				async with timeout(20):
					reader, writer = await aiosocks.open_connection(
						proxy=socks4_addr, proxy_auth=None, dst=self.dst)
					data = await reader.read(1024)
					if data:
						ipreal = data.decode()
						await self._write_db(self.sql_true, True, anonymity, ipreal, ip, port, id)
					else:
						await self._write_db(self.sql_false, False, anonymity, None, ip, port, id)
					writer.close()

			except (ConnectionError, ConnectionResetError, ConnectionRefusedError, 
				asyncio.TimeoutError, OSError, ConnectionResetError, 
				aiosocks.errors.SocksError, AttributeError):
			    await self._write_db(self.sql_false, False, anonymity, None, ip, port, id)
		

	async def _resp_socks(self, url):
		async with aiohttp.ClientSession() as session:
			async with session.get(url) as resp:
				data = await resp.text()
				return data

	#async def _pool(self):
	#	self.pool = await asyncpg.create_pool(self.dsn)



	def bootstrap(self):
		#loop = asyncio.new_event_loop()
		#asyncio.set_event_loop(loop)

		loop = asyncio.get_event_loop()
		#loop.run_until_complete(self._pool())
		data = loop.run_until_complete(self._read_db())
		#data = ['{}:{}'.format(x[0], x[1]) for x in data]
	
		if not data:
			raise ValueError("страница не чего не вернула")

		tasks = [self.sock(obj) for obj in data]

		loop.run_until_complete(asyncio.gather(*tasks))
		loop.run_until_complete(self._pool.close())
		loop.close()


	def go(self):
		self.bootstrap()


def main():
	import time
	start = time.time()
	checker = AsyncSock4OnlineBp()
	checker.go()
	end = time.time() - start
	logging.info("время затраченое на online скан %s мин", int(end)/60)


if __name__ == '__main__':
	main()