from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.conf import settings
User = settings.AUTH_USER_MODEL

from pay.models import Purses


class HistoryMony(models.Model):
    """
    История движения стредст пользователя
    """
    PAY = (
        ('1',_('Пополнил')),
        ('2', _('Купил')),
        ('3', _('Продлил')),
        ('4', _('Вывел')),
        ('5', _('Партнерка')),
        ('6', _('Возврат'))
    )

    user = models.ForeignKey(User, verbose_name=_("Пользователь"))
    purse = models.ForeignKey(Purses, verbose_name=_("Кашелек"))
    usd = models.FloatField(_("Баланс в валюте"), default=0)
    bitcoin = models.FloatField(_("Баланс в bitcoin"), default=0)
    summ = models.FloatField(_("Сумма"), default=0)
    proc = models.FloatField(_("Процент"), default=0)
    popolnen = models.CharField("Операция", max_length=30, choices=PAY, default="1")
    date = models.DateTimeField(_("Дата"), auto_now_add=True)

    class Meta:
        verbose_name = _('История движения средств')
        verbose_name_plural = _('Истории движения средств')
        ordering = ['-date']

    def __str__(self):
        return "{}, {}".format(self.user, self.date)


class SocksHistory(models.Model):
    """
    История покупок сокса
    """
    ip = models.CharField(_("Сокс"), max_length=30)
    time_by = models.IntegerField(_("Срок"), default=0)
    user = models.ForeignKey(User, verbose_name=_("Покупатель"), null=True)
    created = models.DateField(_("Дата"), default=timezone.now(), null=True)

    class Meta:
        verbose_name = _("История покупки сокса")
        verbose_name_plural = _('История покупок соксов')
        #ordering = ['-date']

    def __str__(self):
        return "{}".format(self.ip)
