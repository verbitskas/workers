from django.contrib import admin
from folow_mony.models import HistoryMony, SocksHistory


class HistoryMonyAdmin(admin.ModelAdmin):
    """История операций с финансами
    """
    fields = ('user', 'purse', 'usd', 'bitcoin', 'summ', 'popolnen')
    list_display = [
        'user',
        'purse',
        'usd',
        'bitcoin',
        'summ',
        'proc',
        'popolnen',
        'date'
    ]
    list_filter = ('popolnen', 'date')
    list_display_links = ['user']
    date_hierarchy = 'date'
    search_fields = ['user', 'purse', 'usd', 'bitcoin', 'popolnen', 'date']

admin.site.register(HistoryMony, HistoryMonyAdmin)


class SocksHistoryAdmin(admin.ModelAdmin):
    """История покупки сокса
    """
    fields = ('ip', 'time_by', 'user')
    list_display = ['ip', 'time_by', "user", 'created']
    list_filter = ('ip', 'time_by', "user", 'created')
    list_display_links = ['ip']
    date_hierarchy = 'created'
    search_fields = ['ip', 'time_by', "user", 'created']

admin.site.register(SocksHistory,  SocksHistoryAdmin)
