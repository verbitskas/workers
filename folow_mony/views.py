from django.shortcuts import render
from django.http import HttpResponse

from folow_mony.models import HistoryMony
from pay.models import Purses

from django.contrib.auth import get_user_model
User = get_user_model()


def add_history_mony(user, balance, summ, proc, bitcoin, popoln):
    """История движения средств
    """
    history = HistoryMony()
    history.user = user
    history.purse = Purses.objects.get(client=user)
    history.usd = balance
    history.summ = summ
    history.proc = proc
    history.bitcoin = bitcoin
    history.popolnen = popoln
    history.save()


def add_history_refery(user, balance, summ, proc, bitcoin, popoln):
    """История движения средств для рефери
    """
    history = HistoryMony()
    history.user = user
    history.purse = Purses.objects.get(client=user)
    history.usd = balance
    history.summ = summ
    history.proc = proc
    history.bitcoin = bitcoin
    history.popolnen = popoln
    history.save()
