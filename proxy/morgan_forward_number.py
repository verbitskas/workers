#from gevent.monkey import patch_all
#patch_all (thread=False)

import os
import sys
import time
import logging
import random
from functools import partial
import struct
from struct import pack, unpack
from multiprocessing import Process
from multiprocessing import Queue
from multiprocessing.pool import Pool
from multiprocessing.context import TimeoutError
from multiprocessing.connection import AuthenticationError, Client

from threading import Thread, Event

import json

from datetime import datetime, timedelta

import gevent
from gevent.hub import get_hub
from gevent import signal
from gevent.server import StreamServer
from gevent.queue import Queue
from gevent.socket import create_connection
from gevent import socket
from gevent.pool import Pool
from gevent.threadpool import ThreadPool

from gevent.hub import BlockingSwitchOutError

import requests


sys.path.insert(0, os.path.dirname("../" + __file__))
sys.path.append('/home/john/proxyproject/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

from django.db.models import Q
import django
django.setup()

from client.models import IplistBack, IplistBackBack
from proxy.models import Proxy


DEBUG = False

logging.basicConfig(
	#filename="morgan.log",
	format="%(levelname)-10s строка %(lineno)d %(asctime)s %(message)s",
	level=logging.INFO
	)

# ****************************************************************
# ------------------------------------------------				 *
# | VER | REP | RSV | ATYP | BND.ADDR | BND.PORT |				 *
# ------------------------------------------------				 *
# |  1  |  1  | x00 |   1  |    var   |     2    |				 *
# ------------------------------------------------				 *
# VER    версия протокола: '\x05'								 *
# REP    код ответа:											 *
# 	'\x00' успешный												 *
#	'\x01' ошибка SOCKS-сервера									 *
#	'\x02' соединение запрещено набором правил					 *
#	'\x03' сеть недоступна										 *
#	'\x04' хост недоступен										 *
#	'\x05' отказ в соединении									 *
#	'\x06' истечение TTL										 *
#	'\x07' команда не поддерживается							 *
# 	'\x08' тип адреса не поддерживается							 *
#	'\x09' до X'FF' не определены								 *
# RSV    зарезервирован											 *
# ATYP   тип последующего адреса								 *
# 	IP v4 адрес: '\x01'											 *
#	имя домена:  '\x03'											 *
#	IP v6 адрес: '\x04'											 *
# BND.ADDR  выданный сервером адреса 							 *
# BND.PORT  выданный сервером порт (в сетевом порядке октетов)   *
# ****************************************************************


def forward(source, dest, server):
	"функция потока данных, запускаемая из класса ниже"
	try:
		while True:
			try:
				data = source.recv(8162)
				
				if not data:
					break

				dest.sendall(data)

			except socket.error:
				if DEBUG:
					logging.info("ERR %s", server.ip_source)
				break
			
			except ConnectionResetError:
				if DEBUG:
					logging.info("сокс сбросил")
				break
			
			except Exception as exc:
				logging.exception(exc)
	finally:
		return
    	#source.close()
    	#dest.close()
    	#server = None


def chunks(lst, count):
    """Группировка элементов последовательности по count элементов"""
    start = 0
    for i in range(count):
        stop = start + len(lst[i::count])
        yield lst[start:stop]
        start = stop


class SocksForward(StreamServer):
	""" сервер цепочка соксов """

	def __init__(self, local_port, src, dest, ipdos, portallow, **kwargs):
		listener = ('', int(local_port))
		StreamServer.__init__(self, listener, **kwargs)
		self.ip_local    = ''	  		# локальный ip адрес		
		self.local_port  = local_port   # локальный порт
		
		# исходяший сокс (значения меняем при проверки через сигналы)
		self.ip_source   = src[0]
		self.port_source = int(src[1])
		
		# сокс назначения (значения не изменяються (пока))
		self.ip_dest     = dest[0]
		self.port_dest   = int(dest[1])
		
		# доступ ip и разрешеные порты
		self.ipdos = ipdos
		self.portallow = portallow
		
		self.ERROR_CONNECT = None
		self.ERROR_TIMEOUT = 0
		self.ERROR_DEST = None
		self.ERROR_PORT = 0
		self.STATUS_OK = 0
		
		#self.BAD_SOCKS_SOURCE = None
		#self.BAD_SOCKS_DEST = None


	def handle(self, source, address):
		if self.ipdos:
			if address[0].split(':')[-1] not in self.ipdos:
				#if DEBUG:
				#	logging.info("closed from ip %s", address[0].split(':')[-1])
				return
		try:
			with gevent.Timeout(12):
				while True:
					try:
						dest = create_connection((self.ip_source, self.port_source), timeout=5)
						self.ERROR_CONNECT = None
						self.ERROR_TIMEOUT = 0
						self.ERROR_PORT = 0
						break
					
					except ConnectionRefusedError as exc:
						if DEBUG:
							logging.info("порт не работает %s:%s", self.ip_source, self.port_source)
						self.ERROR_PORT += 1
						self.ERROR_CONNECT = True
						gevent.sleep(0.5)
						continue

					except ConnectionResetError as exc:
						if DEBUG:
							logging.info("ConnectionResetError %s:%s", self.ip_source, self.port_source)
						self.ERROR_CONNECT = True
						gevent.sleep(0.5)
						continue

					except OSError as exc:
						# ошибка вызванная после установки соединения
						if DEBUG:
							logging.info(
								"OSError %s:%s <> %s:%s маршрут не найдее",
								self.ip_source, self.port_source,
								self.ip_dest, self.port_dest)
						self.ERROR_CONNECT = True
						gevent.sleep(0.5)
						continue

			dest.sendall(b'\x05\x01\x00')
			data = dest.recv(8162)
			
			if data.startswith(b'\x05\x00'):
				try:
					data = b"\x05\x01\x00\x01" + socket.inet_aton(self.ip_dest) + pack("!H", self.port_dest)
				except Exception:
					logging.exception(self.ip_dest)
					return
				dest.sendall(data)
				data = dest.recv(8162)
						
				if data.startswith(b'\x05\x00'):
					jobs = [
						gevent.spawn(forward, source, dest, self),
						gevent.spawn_later(2, forward, dest, source, self)
						]
					gevent.joinall(jobs)
					self.STATUS_OK += 1
					self.ERROR_DEST = None
					self.ERROR_TIMEOUT = 0
					self.ERROR_CONNECT = None
					dest.close()
					return

				elif data.startswith(b'\x05\x01'):
					# ответ при не рабочем соксе назначения
					self.ERROR_CONNECT = True
					self.ERROR_DEST = True
					if DEBUG:
						logging.error(
							"ошибка сервера %s:%s <> %s:%s",
							self.ip_source, self.port_source,
							self.ip_dest, self.port_dest)
					dest.close()
				elif data.startswith(b'\x05\x02'):
					self.ERROR_CONNECT = True
					self.ERROR_DEST = True
					if DEBUG:
						logging.error(
							"соединение запрещено набором правил")
					dest.close()
				elif data.startswith(b'\x05\x03'):
					if DEBUG:
						logging.error(
							"сеть недоступна: %s", self.ip_dest)
					self.ERROR_CONNECT = True
					self.ERROR_DEST = True
					dest.close()
				elif data.startswith(b'\x05\x04'):
					if DEBUG:
						logging.error(
							"хост недоступен: %s", self.ip_dest)
					dest.close()
					self.ERROR_CONNECT = True
					self.ERROR_DEST = True
				elif data.startswith(b'\x05\x05'):
					self.ERROR_CONNECT = True
					self.ERROR_DEST = True
					if DEBUG:
						logging.error(
							"отказ в соединении %s:%s",
							self.ip_dest, self.port_dest)
					dest.close()
				elif data.startswith(b'\x05\x06'):
					if DEBUG:
						logging.error("истечение TTL")
					dest.close()
				else:
					if DEBUG:
						logging.error(
							"%s:%s в блоке сокса %s:%s или доругая ошибка",
							self.ip_source, self.port_source,
							self.ip_dest, self.port_dest)
					self.ERROR_CONNECT = True
					self.ERROR_DEST = True
					dest.close()
			else:
				logging.info(data)

			return

		except ConnectionRefusedError as exc:
			if DEBUG:
				logging.info("порт не работает %s:%s", self.ip_source, self.port_source)
			self.ERROR_PORT += 1
			self.ERROR_CONNECT = True
			return

		except ConnectionResetError as exc:
			dest.close()
			if DEBUG:
				logging.info("ConnectionResetError %s:%s", self.ip_source, self.port_source)
			self.ERROR_CONNECT = True
			return

		except OSError as exc:
			# ошибка вызванная после установки соединения
			if DEBUG:
				logging.info(
					"OSError %s:%s <> %s:%s маршрут не найдее",
					self.ip_source, self.port_source,
					self.ip_dest, self.port_dest)
			self.ERROR_CONNECT = True
			return

		except gevent.Timeout:
			if DEBUG:
				logging.info("время вышло %s:%s", self.ip_source, self.port_source)
			self.ERROR_CONNECT = True
			self.ERROR_TIMEOUT += 1
			return


SERVERS = []
BAD_SOCKS = []
BAD_SOCKS_DEST = []
SERVER_RESULT = []
DICTS = {}

PORT_N = [str(x) for x in range(32000, 65535) if x != 5432 and x != 65100]
PORT_O = [str(x) for x in range(1024, 32000) if x != 5432 and x != 65100]


ev = Event()

'''
class Time:
	class TimeExc(Exception):
		pass

	def __init__(self, val):
		self.val = val

	def __enter__(self):
		signal.signal(signal.SIGALRM, self.error_time)
		signal.alarm(self.val)

	def __exit__(self, type, value, traceback):
		signal.alarm(0)

	def error_time(self, sig, fd):
		raise Time.TimeExc()
'''


class ClientMasscan:
	def __init__(self, data, ip):
		self.address = ip
		self.port = 65100
		self.password = b'rikitiki1984'

		self.data = data
		self._newsocks = list()


	def __call__(self):
		try:
			connection = Client((self.address, self.port), authkey=self.password)

		except (AuthenticationError, socket.error, IOError):
			logging.info("ошибка")
			return

		connection.send(self.data)
		data_recv = connection.recv()
		logging.info("*"*30)
		logging.info("получили %s", len(data_recv))
		connection.close()
		self._newsocks = data_recv


	def get_result(self):
		return self._newsocks


def remove_scaner():
	global SERVERS
	logging.info("стработал таймер")
	ev.set()

	data = [(x.ip_dest, x.port_dest) for x in SERVERS]
	
	ports_older = [x.port_dest for x in SERVERS]

	older_data = ["{}:{}".format(server.ip_dest, server.port_dest) for server in SERVERS]

	
	client_45 = ClientMasscan(data, '185.235.244.45')
	client_17 = ClientMasscan(data, '185.235.245.17')

	pool = Pool(2)

	res_45 = pool.apply_async(client_45)
	res_17 = pool.apply_async(client_17)

	try:
		res_45.get(60*20)
	except TimeoutError:
		pass

	try:
		res_17.get(60*20)
	except TimeoutError:
		pass

	data_recv_45 = client_45.get_result()
	data_recv_17 = client_17.get_result()

	if data_recv_45 and data_recv_17:
		logging.info("оба списка %s %s", len(data_recv_45), len(data_recv_17))
		data_recv = list(set(data_recv_45 + data_recv_17))

	elif data_recv_45:
		logging.info("список с сервера 185.235.244.45")
		data_recv = data_recv_45
	else:
		logging.info("список с сервера 185.235.245.17")
		data_recv = data_recv_17


	NEW_SERVER = []
	RESTORE = []
	
	if data_recv:
		for ip_ in data_recv:
			try:
				ip = ip_.split(":")[0]
				port = int(ip_.split(":")[1])
			except IndexError:
				logging.info("%s", ip_)
				continue
			
			if ip_ not in older_data:
				for server in SERVERS:
					if server.ip_dest == ip:
						server.ip_dest = ip
						server.port_dest = int(port)

						k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]

						NEW_SERVER.append(k)


	logging.info("новых портов %s", len(NEW_SERVER))
	
	i = IplistBackBack.objects.get(server__ip__in=['185.235.245.9'])

	server_dicts = {}
	server_dicts['185.235.245.9'] = json.dumps(NEW_SERVER)
	i.server_dicts = server_dicts
	i.save()
	
	logging.info("спим до 8:55")
	time.sleep(60*50)
	ev.clear()



class Schedule:
	def __init__(self, func, date):
		self._func = func
		self.hour = int(date.split(":")[0])
		self.minute = int(date.split(":")[1])
		self.running = False

	def _bootstrap(self):
		now = datetime.now()
		if now.hour == self.hour and now.minute == self.minute:
			if not self.running:
				res = self._func()
				self.running = True
				time.sleep(61)
				return res
		else:
			self.running = False

	def run(self):
		self._bootstrap()



def scheduler():
	sched = Schedule(remove_scaner, "08:55")

	while True:
		sched.run()
		try:
			time.sleep(10)
		except KeyboardInterrupt:
			return


def handler():
	time.sleep(60*15)

	while True:
		if ev.is_set():
			logging.info("спим 60с")
			time.sleep(60)
			continue

		global que_scan
		global que
		global BAD_SOCKS
		global BAD_SOCKS_DEST
		global SERVERS
		global PORT_N
		global PORT_O

		NEW_SERVER = []
		i = IplistBackBack.objects.get(server__ip__in=['185.235.245.9'])

		now = datetime.now()
		

		logging.info("BAD_SOCKS %s, %s", len(BAD_SOCKS), BAD_SOCKS[:3])

		
		dicts = i.dicts[0]
		vendor = i.link
		if i.filter_url:
			statis_pav = requests.get("http://hu-li.net/socks/logs/s1.txt").text.split()

		port_ipsrc_ipdest = eval(i.server_dicts['185.235.245.9'])
		local_port, src, dest = [], [], []
		[(local_port.append(x[0]), src.append(x[1]), dest.append(x[2])) for x in port_ipsrc_ipdest]
		logging.info("запустили проверку промежуточного листа")

		result = 0
		old_result = 0

		ip_sources = [x.ip_source for x in SERVERS]
		ip_destionals = [x.ip_dest for x in SERVERS]
		
		#local_ports = [str(x.server_port) for x in SERVERS]
		local_ports = list(DICTS.values())
		
		#new_ports = [port for port in range(1024, 65536) if port not in local_ports and port != 5432 and port != 65100]
		new_ports = [port for port in PORT_N if port not in local_ports]

		obj = Proxy.objects.filter(checkers=True, vendor=vendor).order_by('-created')
		sucess_proxy_src = [(x.ip, x.port) for x in obj if x.ip not in ip_sources and x.ip not in BAD_SOCKS]

		logging.info("----------- %s -----------", len(sucess_proxy_src))
		
		obj = Proxy.objects.filter(dicts).order_by('update')
		
		if i.filter_url:
			sucess_proxy_dest = [(x.ip, x.port) for x in obj if x.ip not in \
				ip_destionals and x.ip not in BAD_SOCKS_DEST and x.ip in statis_pav]
		else:
			sucess_proxy_dest = [(x.ip, x.port) for x in obj if x.ip not in \
				ip_destionals and x.ip not in BAD_SOCKS_DEST]

		NEW_SERVER = []
		RESTART = []
		TIME_OUT_PORT = False
		logging.info('go')


		for server in SERVERS:
			try:
				proxy_src = Proxy.objects.get(ip=server.ip_source, port=server.port_source, vendor=vendor, checkers=True)
				proxy_dest = Proxy.objects.get(dicts, ip=server.ip_dest, port=server.port_dest)

				k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]
				NEW_SERVER.append(k)

				continue
				'''

				if server.STATUS_OK > 0:
					old_result += 1
					k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]
					NEW_SERVER.append(k)

					continue

				if server.ERROR_CONNECT:
					BAD_SOCKS.append(server.ip_source)
					BAD_SOCKS_DEST.append(server.ip_dest)
					

					if server.ERROR_TIMEOUT > 3 or server.ERROR_PORT > 3:
						sucess_src = sucess_proxy_src.pop()
						server.ip_source = sucess_src[0]
						server.port_source = int(sucess_src[1])

						k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]
						NEW_SERVER.append(k)
						server.ERROR_CONNECT = None
						server.ERROR_DEST = None
						server.ERROR_TIMEOUT = 0
						old_result += 1
						if DEBUG:
							logging.info("[ пропуск есть соксы но нет портов]")
						continue

					else:
						sucess_src = sucess_proxy_src.pop()
						server.ip_source = sucess_src[0]
						server.port_source = int(sucess_src[1])
						sucess_dest = sucess_proxy_dest.pop()
						server.ip_dest = sucess_dest[0]
						server.port_dest = int(sucess_dest[1])

						port = new_ports.pop(0)
				
						logging.info("запуск новой связки на порт %s", port)

						try:
							server.stop()
							server.set_listener(port)
							server.local_port = port
							server.start()
						except OSError:
							RESTART.append(server)
							logging.info("порт %s занят", port)

						except BlockingSwitchOutError:
							logging.error("блокировка")

						k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]
						NEW_SERVER.append(k)
						result += 1
						continue


				else:
					old_result += 1
					k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]
					NEW_SERVER.append(k)
				'''
				
			except Proxy.DoesNotExist:
				try:
					proxy_src = Proxy.objects.get(ip=server.ip_source, vendor=vendor, checkers=True)
					proxy_dest = Proxy.objects.get(dicts, ip=server.ip_dest)
					
					server.ip_source = proxy_src.ip
					server.port_source = proxy_src.port

					server.ip_dest = proxy_dest.ip
					server.port_dest = int(proxy_dest.port)

					k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]
					NEW_SERVER.append(k)
					
					server.ERROR_CONNECT = None
					server.ERROR_DEST = None
					server.ERROR_TIMEOUT = 0
					old_result += 1
					
					if DEBUG:
						logging.info("[ пропуск есть соксы но нет портов]")
					continue

				except Proxy.MultipleObjectsReturned:
					proxy_src = Proxy.objects.filter(ip=server.ip_source, vendor=vendor, checkers=True)[0]
					proxy_dest = Proxy.objects.filter(dicts, ip=server.ip_dest)[0]
					
					server.ip_source = proxy_src.ip
						
					server.port_source = proxy_src.port
					server.ip_dest = proxy_dest.ip
					server.port_dest = int(proxy_dest.port)

					k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]
					NEW_SERVER.append(k)
					server.ERROR_CONNECT = None
					server.ERROR_DEST = None
					server.ERROR_TIMEOUT = 0
					old_result += 1
					if DEBUG:
						logging.info("[ пропуск есть несколько соксов но нет портов]")
					continue

				except Proxy.DoesNotExist:
					try:
						sucess_src = sucess_proxy_src.pop()
						server.ip_source = sucess_src[0]
						server.port_source = int(sucess_src[1])

						sucess_dest = sucess_proxy_dest.pop()
						server.ip_dest = sucess_dest[0]
						server.port_dest = int(sucess_dest[1])

					except IndexError:
						continue

					'''
					local_ports = [x.server_port for x in SERVERS]
					new_ports = [port for port in range(1024, 65536) if port not in local_ports and port != 5432 and port != 65100]
					'''

					if DICTS.get(server.ip_dest, None):
						logging.info("уже есть в таблице %s", DICTS.get(server.ip_dest))
						port = DICTS.get(server.ip_dest, None)
					else:
						port = new_ports.pop(0)
						DICTS[server.ip_dest] = port

					logging.info("запуск новой связки на порт %s", port)

					try:
						server.stop()
						server.set_listener(port)
						server.local_port = port
						server.start()

					except OSError:
						RESTART.append(server)
						logging.info("порт %s занят", port)

					except BlockingSwitchOutError:
						logging.error("блокировка")


					k = [server.local_port, [server.ip_source, server.port_source],[server.ip_dest, server.port_dest]]
					NEW_SERVER.append(k)
					server.ERROR_TIMEOUT = 0
					result += 1


		if RESTART:
			for server in RESTART:
				try:
					if server.closed:
						logging.info("уже закрыт")
						time.sleep(1)
						server.start()
					else:
						server.stop()
						server.start()
				except OSError:
					logging.info("не удалось повторно перезапустить %s", server.address)
					try:
						server.loop.reinit()
					except Exception as exc:
						logging.exception(exc)
			logging.info(dir(SERVERS[0].loop))

		logging.info("проверка закончена, %s новых портов", result)
		logging.info("проверка закончена, %s старых портов", old_result)
		server_dicts = {}
		server_dicts['185.235.245.9'] = json.dumps(NEW_SERVER)
		i.server_dicts = server_dicts
		i.save()

		logging.info("спим")
		try:
			time.sleep(60*20)
		except KeyboardInterrupt:
			return
	


def do_start_orm():
	global DICTS
	try:
		i = IplistBackBack.objects.get(server__ip__in=['185.235.245.9'])
	except IplistBackBack.DoesNotExist:
		logging.info("список не обнаружен.......")
		return None, None, None, None, None

	except IplistBack.MultipleObjectsReturned:
		logging.exception("модель имеет 2 записи")
		return None, None, None, None, None

	port_ipsrc_ipdest = eval(i.server_dicts['185.235.245.9'])

	ipdos = i.ip
	if i.portallow:
		portallow = i.portallow.split()
	else:
		portallow = None

	try:
		ipdos = ipdos.split(' ')
	except AttributeError as exc:
		ipdos = None

	local_port = [x[0] for x in port_ipsrc_ipdest]
	src = [x[1] for x in port_ipsrc_ipdest]
	dest = [x[2] for x in port_ipsrc_ipdest]

	#----------------------------------------------
	dest_ = [x[0] for x in dest]
	DICTS = {k:v for k,v in zip(dest_, local_port)}
	#-----------------------------------
	'''
	
	NEW_SERVER = []
	for x in zip(local_port, src, dest):
		k = [x[0], x[1], ["127.0.0.1", x[0]]]
		NEW_SERVER.append(k)

	server_dicts = {}
	server_dicts['185.235.245.9'] = json.dumps(NEW_SERVER)
	i.server_dicts = server_dicts
	i.save()
	'''
	#-----------------------------------
	try:
		i = IplistBackBack.objects.get(server__ip__in=['185.235.245.9'])
	except IplistBackBack.DoesNotExist:
		logging.info("список не обнаружен.......")
		return None, None, None, None, None

	except IplistBack.MultipleObjectsReturned:
		logging.exception("модель имеет 2 записи")
		return None, None, None, None, None

	port_ipsrc_ipdest = eval(i.server_dicts['185.235.245.9'])

	ipdos = i.ip
	if i.portallow:
		portallow = i.portallow.split()
	else:
		portallow = None

	try:
		ipdos = ipdos.split(' ')
	except AttributeError as exc:
		ipdos = None

	local_port = [x[0] for x in port_ipsrc_ipdest]
	src = [x[1] for x in port_ipsrc_ipdest]
	dest = [x[2] for x in port_ipsrc_ipdest]

	return local_port, src, dest, ipdos, portallow


def main():
	global SERVERS

	local_port, src, dest, ipdos, portallow = do_start_orm()

	for obj in zip(local_port, src, dest):
		server = SocksForward(*obj, ipdos, portallow)
		server.max_delay = 5
		server.max_accept = 100
		SERVERS.append(server)

	for server in SERVERS:
		try:
			server.start()
		except OSError:
			logging.info("порт %s занят", server.local_port)
	try:
		logging.info("ЗАПУСК")

		sh = Thread(target=scheduler)
		sh.start()

		th = Thread(target=handler)
		th.start()

		

		gevent.wait()

	except KeyboardInterrupt:
		logging.info('\nвыходим')


	finally:
		for server in SERVERS:
			server.stop()




if __name__ == '__main__':
	from daemonize import daemonize
	#daemonize(stdout='/tmp/portforward_https.log',stderr='/tmp/portforwarderror_https.log')
	try:
		resource.setrlimit(resource.RLIMIT_NOFILE, (90000, 100000))
	except Exception:
		pass
	
	main()

