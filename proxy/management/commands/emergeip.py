#-*-coding:utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from proxy.models import Proxy, CountryProxy, CityProxy
from client.models import Server

#link = list(set([x.country for x in Proxy.objects.all() if x.country]))
class Command(BaseCommand):
    help = 'build ip'

    def handle(self, *args, **options):
		local_port = list(range(1024,65001))
		local_port.remove(5432)
		local_port.remove(42408)
		o = ["socks5://188.68.240.2:{}".format(x) for x in local_port]

		for i in o:
			Server.objects.create(ip=i)

		self.stdout.write('write ip "%s"' % len(o))