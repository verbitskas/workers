#-*-coding:utf-8 -*-
from django.db import models
from django.core.management.base import BaseCommand, CommandError
from proxy.models import Proxy, CountryProxy

#link = list(set([x.country for x in Proxy.objects.all() if x.country]))
class Command(BaseCommand):
    help = 'build country'
    
    def handle(self, *args, **options):
        link = list(set([x.country for x in Proxy.objects.all() if x.country]))
        for x in link:
            try:
                CountryProxy.objects.get(country=x)
            except CountryProxy.DoesNotExist:
                CountryProxy.objects.create(country=x)
            except CountryProxy.MultipleObjectsReturned:
                pass
        self.stdout.write('write country "%s"' % len(link))