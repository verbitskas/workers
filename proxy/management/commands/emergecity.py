#-*-coding:utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from proxy.models import Proxy, CountryProxy, CityProxy

#link = list(set([x.country for x in Proxy.objects.all() if x.country]))
class Command(BaseCommand):
    help = 'build city'

    def handle(self, *args, **options):
    	link = list(set([x.city for x in Proxy.objects.all() if x.city]))
    	[CityProxy.objects.create(city=x) for x in link]
    	self.stdout.write('write city "%s"' % len(link))