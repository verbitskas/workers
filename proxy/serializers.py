from rest_framework import serializers

from proxy.models import Proxy, TimeSet, CountryProxy, ProxyAuth
from vendor.models import Vendors

from django.conf import settings

User = settings.AUTH_USER_MODEL

class CountryProxySerializer(serializers.ModelSerializer):
    '''
    Страны
    '''
    class Meta:
        model = CountryProxy
        fields = ('id', 'country')

class ProxyCountrySerializer(serializers.ModelSerializer):
    '''
    Прокси
    '''
    class Meta:
        model = Proxy
        fields = ('id', 'country')


class VendorSerializer(serializers.ModelSerializer):
    ''' Продавцы'''
    class Meta:
        model = Vendors
        fields = ('user', 'title')


class VSerializer(serializers.ModelSerializer):
    ''' Продавцы'''
    class Meta:
        model = Vendors
        fields = ('title', )


class BLSerializer(serializers.ModelSerializer):
    ''' Black list'''
    class Meta:
        model = Proxy
        fields = ('blacklist',)


# class UtcSerializer(serializers.ModelSerializer):
#     ''' Часовые пояса'''
#     class Meta:
#         model = Vendors
#         fields = ('id', 'title')


class ProxySerializer(serializers.ModelSerializer):
    '''
    Прокси
    '''
    #vendor = VSerializer()
    class Meta:
        model = Proxy
        fields = ('id', 'hidden_ip', 'country', 'region_code', 'city', \
            'gmt', 'postal_code', 'domain', 'vendor', 'vendor_socks', 'time_zone', 'blacklist', 'region')


class ProxyIpSerializer(serializers.ModelSerializer):
    '''
    Прокси ip
    '''
    class Meta:
        model = Proxy
        fields = ('id', 'ip', 'hidden_ip', 'country', 'region_code', 'city', \
            'gmt', 'postal_code', 'domain', 'speed', 'timeout', 'vendor', 'blacklist', 'lat', 'lon', 'update', 'created', 'port', 'region')


class ProxyRegionSerializer(serializers.ModelSerializer):
    '''
    Штаты
    '''
    class Meta:
        model = Proxy
        fields = ('id', 'country', 'region_code', 'city', 'region')


class ProxyPaySerializer(serializers.ModelSerializer):
    '''
    Прокси с реальным IP для купленых прокси
    '''
    class Meta:
        model = Proxy
        fields = ('id', 'ip', 'ipreal', 'port', 'country', 'region_code', 'city', \
            'gmt', 'postal_code', 'domain', 'vendor', 'time_zone', 'blacklist', 'region')
