import asyncio
import socket
import re
from struct import unpack, pack

import requests

def par(url):
	reg = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}"
	data = requests.get(url).text.split()
	datas = [x.strip() for x in data]
	response = [x for x in datas if re.findall(reg, x)]
	return response


async def check(obj):
	ip, port = obj.split(':')
	try:
		reader, writer = await asyncio.wait_for(asyncio.open_connection(ip, port), timeout=10.0)
	except (ConnectionResetError, ConnectionRefusedError, OSError, Exception):
		return

	writer.write(b'\x05\x01\x00')
	try:
		data = await reader.read(1024)
	except (ConnectionResetError, ConnectionRefusedError, OSError):
		return

	if data.startswith(b'\x05\x00'):
		try:
			data = b"\x05\x01\x00\x01" + socket.inet_aton('78.46.93.209') + pack("!H", 9999)
		except Exception as exc:
			print(exc)
			writer.close()
			return
		writer.write(data)
		data = await reader.read(1024)

		if data:
			query = b"GET / HTTP/1.1\r\n\r\n"
			writer.write(query)
			data = await reader.read(1024)
			print(data.split()[-1])
			await writer.drain()
			writer.close()
			try:
				data = data.split()[-1]
			except:
				pass
			return data


		else:
			writer.close()
			return



def main():
	res = par('http://proxybase.net/doubleBCA')
	loop = asyncio.get_event_loop()
	coro = [check(obj) for obj in res[:100]]
	result = loop.run_until_complete(asyncio.gather(*coro))
	loop.close()
	print(result)
	return



if __name__ == '__main__':
	main()