import asyncio
import socket
from struct import unpack, pack
import re

import aiohttp

def aiter(func):
	return func

class AsyncSock:
	def __init__(self, socks):
		self.socks = socks
		self.running = True
		self.result = None

	class Go:
		def __init__(self, ip, port):
			self.ip = ip
			self.port = port
			self.dest_ip = '78.46.93.209'
			self.dest_port = 9999


		async def connect(self):
			try:
				reader, writer = await asyncio.open_connection(self.ip, self.port)
			except (ConnectionRefusedError, ConnectionResetError, OSError):
				return
			writer.write(b'\x05\x01\x00')

			try:
				data = await reader.read(1024)
			except (ConnectionResetError, ConnectionRefusedError, OSError):
				return
	
			if data.startswith(b'\x05\x00'):
				try:
					data = b"\x05\x01\x00\x01" + socket.inet_aton(self.dest_ip) + pack("!H", self.dest_port)
				except Exception as exc:
					print(exc)
					writer.close()
					return
				writer.write(data)
				data = await reader.read(1024)

				if data:
					query = b"GET / HTTP/1.1\r\n\r\n"
					writer.write(query)
					data = await reader.read(1024)
					await writer.drain()
					writer.close()
					try:
						data = data.split()[-1]
					except:
						pass
					return data


			else:
				writer.close()
				return


	#@aiter
	def __aiter__(self):
		return self


	async def parser(self, socks):
		(ip, port) = socks.split(':')
		return (ip, port)


	async def __anext__(self):
		while self.running:
			print(len(self.socks))
			if not self.socks:
				print("конец")
				raise StopAsyncIteration

			ip, port = await self.parser(self.socks.pop(0))
			self.go = self.Go(ip, port)

			data = await self.go.connect()
			return data

			self.result = None



async def check(urls):
	async for line in AsyncSock(urls):
		print(line)



async def parser_http(url):
	reg = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}"
	read = await aiohttp.request('GET', url)
	data = await read.read()
	datas = [x.strip() for x in data.decode().split()]
	response = [x for x in datas if re.findall(reg, x)]
	return response

def main():
	loop = asyncio.get_event_loop()
	result = loop.run_until_complete(parser_http('http://proxybase.net/doubleBCA'))
	loop.run_until_complete(check(result[:300]))


if __name__ == '__main__':
	main()
