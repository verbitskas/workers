import datetime
import json
from datetime import datetime, timedelta

from django.utils import timezone
from django.http import HttpResponse, JsonResponse
from proxy.models import Proxy, TimeSet, CountryProxy, CityProxy

from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from rest_framework import viewsets

from .serializers import *

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.db.models import Q, Count, Avg
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.shortcuts import get_list_or_404
from django.contrib.auth.decorators import login_required

from vendor.models import Vendors
from client.models import Port
#def ajax_city(request):
#	ts = TimeSet.objects.first()
#	now = timezone.now()
#	try:
#		offset = now - datetime.timedelta(minutes=ts.offset)
#	except AttributeError:
#		offset = None
#	if request.method == 'GET':
#		query = request.GET.get('term', '')
#		if query:
#			if offset:
#				city = [x.city for x in Proxy.objects.filter(checkers=True, city__istartswith=query,update__gte=offset)]
#			else:
#				city = [x.city for x in Proxy.objects.filter(checkers=True, city__istartswith=query)]
#			city = list(set(city))
#		else:
#			city = ''
#		data = json.dumps(city)
#		mimetype = 'application/json'
#	else:
#		data = 'fail'
#		mimetype = 'application/json'
#	return HttpResponse(data, mimetype)

def ajax_continent(request):
    if request.method == 'GET':
        query = request.GET.get('term', '')
        if query:
            continent = [x.continent for x in Proxy.objects.filter(Q(checkers=True) & Q(continent__istartswith=query))]
            continent = list(set(continent))
        else:
            continent = ''
        data = json.dumps(continent)
        mimetype = 'application/json'
    else:
        data = 'fail'
        mimetype = 'application/json'
    return HttpResponse(data, mimetype)

def ajax_zone(request):
    if request.method == 'GET':
        query = request.GET.get('term', '')
        if query:
            time_zone = [x.time_zone for x in Proxy.objects.filter(Q(checkers=True) & Q(time_zone__icontains=query))]
            time_zone = list(set(time_zone))
        else:
            time_zone = ''
        data = json.dumps(time_zone)
        mimetype = 'application/json'
    else:
        data = 'fail'
        mimetype = 'application/json'
    return HttpResponse(data, mimetype)

def ajax_code_country(request):
    if request.method == 'GET':
        query = request.GET.get('term', '')
        if query:
            country_code = [x.country_code for x in Proxy.objects.filter(Q(checkers=True) & Q(country_code__istartswith=query))]
            country_code = list(set(country_code))
        else:
            country_code = ''
        data = json.dumps(country_code)
        mimetype = 'application/json'
    else:
        data = 'fail'
        mimetype = 'application/json'
    return HttpResponse(data, mimetype)

def ajax_code_region(request):
    if request.method == 'GET':
        query = request.GET.get('term', '')
        if query:
            region_code = [x.region_code for x in Proxy.objects.filter(Q(checkers=True) & Q(region_code__istartswith=query))]
            region_code = list(set(region_code))
        else:
            region_code = ''
        data = json.dumps(region_code)
        mimetype = 'application/json'
    else:
        data = 'fail'
        mimetype = 'application/json'
    return HttpResponse(data, mimetype)

def ajax_code_postal(request):
    if request.method == 'GET':
        query = request.GET.get('term', '')
        if query:
            postal_code = [x.postal_code for x in Proxy.objects.filter(Q(checkers=True) & Q(postal_code__istartswith=query))]
            postal_code = list(set(postal_code))
        else:
            postal_code = ''
        data = json.dumps(postal_code)
        mimetype = 'application/json'
    else:
        data = 'fail'
        mimetype = 'application/json'
    return HttpResponse(data, mimetype)


@login_required
def ajax_all_country(request):
    """
    Ajax запрос на список всех стран
    """
    if request.method == 'GET':
        #snippets = CountryProxy.objects.all().exclude(country='-')
        #serializer = CountryProxySerializer(snippets, many=True)

        proxy = Proxy.objects.filter(checkers=True, update__isnull=False).values('country')\
                                                    .exclude(country='-') \
                                                    .order_by('country') \
                                                    .distinct('country')
        snippets = CountryProxy.objects.filter(country__in=proxy) \
                                        .exclude(country='-') \
                                        .values('country', 'id') #\
                                        #.order_by('country')
        serializer = CountryProxySerializer(snippets, many=True)

        p_c = Proxy.objects.filter(checkers=True, update__isnull=False).count()
        return JsonResponse({'country':serializer.data, 'prox_coun':p_c}, safe=False)


@login_required
def ajax_all_vendors(request):
    """
    Ajax запрос на список всех продавцов
    """
    if request.method == 'GET':
        #snippets = Vendors.objects.all()
        proxy = Proxy.objects.filter(checkers=True, update__isnull=False).values('vendor')\
                                                    .order_by('vendor') \
                                                    .distinct('vendor')
        snippets = Vendors.objects.filter(user=proxy).order_by('user')
        serializer = VendorSerializer(snippets, many=True)
        return JsonResponse(serializer.data, safe=False)


# @login_required
# def ajax_all_vendor(request):
#     """
#     Ajax запрос на список всех продавцов
#     """
#     if request.method == 'GET':
#         snippets = CountryProxy.objects.all()
#         serializer = CountryProxySerializer(snippets, many=True)
#         return JsonResponse(serializer.data, safe=False)


@login_required
def ajax_dj_region(request):
    '''Ajax запрос на список штатов'''
    if request.is_ajax():
        country = request.GET.get('country', None)
        if country:
            country = CountryProxy.objects.get(id=int(country))
            snippets = Proxy.objects.filter(country__istartswith=country.country, \
                                    checkers=True, update__isnull=False) \
                                    .order_by('region') \
                                    .distinct('region')
            serializer = ProxyRegionSerializer(snippets, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            return HttpResponse(json.dumps({"error":"error"}))
    else:
        return HttpResponse(json.dumps({"error":"error"}))


@login_required
def ajax_post_region(request):
    '''Ajax запрос на список прокси конкретного штата'''
    if request.is_ajax():
        country = request.GET.get('country', None)
        region = request.GET.get('reg', None)
        bl = request.GET.getlist('bl[]', None)
        # Если есть блек
        if bl:
            country = CountryProxy.objects.get(id=int(country))
            query = Q()
            for el in bl:
                query |= Q(blacklist__icontains=el)
            snippets = Proxy.objects.filter(country__istartswith=country.country, \
                                          checkers=True, update__isnull=False,\
                                          region_code=region) \
                                          .order_by('city') \
                                          .distinct('city') \
                                          .exclude(query)[:300]
            serializer = ProxySerializer(snippets, many=True)
            return JsonResponse(serializer.data, safe=False)
        if region:
            country = CountryProxy.objects.get(id=int(country))
            snippets = Proxy.objects \
                                .filter(country__istartswith=country.country, \
                                        checkers=True, update__isnull=False, \
                                        region_code=region) \
                                        .order_by('city') \
                                        .distinct('city')
            serializer = ProxySerializer(snippets, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            return HttpResponse(json.dumps({"error":"error"}))


@login_required
def ajax_dj_city(request):
    '''Ajax запрос на список городов'''
    if request.is_ajax():
        country = request.GET.get('country', None)
        if country:
            country = CountryProxy.objects.get(id=int(country))
            snippets = Proxy.objects.filter(country__istartswith=country.country, \
                                    checkers=True, update__isnull=False) \
                                    .order_by('city') \
                                    .distinct('city')
            serializer = ProxyRegionSerializer(snippets, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            return HttpResponse(json.dumps({"error":"error"}))


@login_required
def ajax_search_form(request):
    '''Ajax запрос из поля поиска'''
    if request.is_ajax():
        search = request.GET.get('search', None)
        if search:
            try:
                ser = Proxy.objects \
                                    .filter(Q(vendor__exact=search) | \
                                            Q(ip__istartswith=search) |
                                            Q(postal_code__istartswith=search)) \
                                            [:300]
                s_c = Proxy.objects \
                                    .filter(Q(vendor__exact=search) | \
                                            Q(ip__istartswith=search) |
                                            Q(postal_code__istartswith=search))\
                                            .count()
            except:
                ser = Proxy.objects \
                                    .filter(Q(country__istartswith=search) |
                                            Q(region__istartswith=search) |
                                            Q(city__iexact=search) |
                                            Q(domain__icontains=search) |
                                            Q(postal_code__istartswith=search))\
                                            [:300]
                s_c = Proxy.objects \
                                    .filter(Q(country__istartswith=search) |
                                            Q(region__istartswith=search) |
                                            Q(city__iexact=search) |
                                            Q(domain__icontains=search) |
                                            Q(postal_code__istartswith=search))\
                                            .count()

            serializer = ProxySerializer(ser, many=True)
            return JsonResponse({"search":serializer.data, "search_count": s_c}, safe=False)
        else:
            return HttpResponse(json.dumps({"error":"error"}))


@login_required
def ajax_new_search(request):
    """
    Ajax запрос поиска по полям
    """
    if request.is_ajax():
        country = request.GET.get('country', None)
        reg =  request.GET.get('reg', None)
        city = request.GET.get('city', None)
        vend = request.GET.get('vendor', None)
        bl = request.GET.getlist('bl[]', None)
        ips = request.GET.get('ip', None)
        dns = request.GET.get('dns', None)
        zips = request.GET.get('zip', None)
        gmt = request.GET.get('gmt', None)
        pub = request.GET.get('pub', None)
        ipsd = request.GET.get('ipsd', None)
        blfilter = request.GET.get('blfilter', None)
        page = request.GET.get('page', None)

        query = Q()
        filt = []
        if country:
            country = CountryProxy.objects.get(id=int(country))
            c = Q()
            c &= Q(country__istartswith=country.country)
            filt.append(c)
        if reg:
            r = Q()
            r &= Q(region_code=reg)
            filt.append(r)
        if city:
            ci = Q()
            ci &= Q(city=city)
            filt.append(ci)
        if vend:
            vend = int(vend)
            v = Q()
            v &= Q(vendor=vend)
            filt.append(v)
        if bl:
            query = Q()
            for el in bl:
                query |= Q(blacklist__icontains=el)
        if ips:
            ip = Q()
            ip &= Q(ip__istartswith=ips)
            filt.append(ip)
        if dns:
            d = Q()
            d &= Q(domain__icontains=dns)
            filt.append(d)
        if zips:
            z = Q()
            z &= Q(postal_code__istartswith=int(zips))
            filt.append(z)
        if gmt:
            g = Q()
            g &= Q(gmt__istartswith=gmt)
            filt.append(g)
        if pub:
            pub = int(pub)
            last = datetime.now() - timedelta(pub)
            p = Q()
            p &= Q(created__gt=last)
            filt.append(p)
        if ipsd:
            ipsd = int(ipsd)
            sd = Q()
            if ipsd == 2:
                last = datetime.now() - timedelta(3)
                sd &= Q(created__gt=last)
                filt.append(sd)
            if ipsd == 1:
                last = datetime.now() - timedelta(3)
                sd &= Q(created__lt=last)
                filt.append(sd)

        order = Port.objects.filter(use=True, socks__isnull=False) \
                    .values('socks')

        if page and page != "1":
            p = int(page) * 300
            p2 = p + 300
        else:
            p = 0
            p2 = 300

        if blfilter:
            blfilter = int(blfilter)
            if blfilter != 0:
                prox = Proxy.objects.filter(*filt) \
                                        .filter(checkers=True, update__isnull=False) \
                                        .exclude(query) \
                                        .exclude(country='-') \
                                        .exclude(id__in=order) \
                                        .extra(where=["array_length(regexp_split_to_array(blacklist, E'\\s+'), 1) <= %s"], params=[blfilter])
                bl_none = Proxy.objects.filter(*filt) \
                                        .filter(blacklist=None, checkers=True, update__isnull=False) \
                                        .exclude(country='-') \
                                        .exclude(id__in=order) \
                                        .exclude(query)
                s = bl_none | prox
            if blfilter == 0:
                s = Proxy.objects.filter(*filt) \
                                        .filter(blacklist=None, checkers=True, update__isnull=False) \
                                        .exclude(country='-') \
                                        .exclude(id__in=order)
        else:
            s = Proxy.objects.filter(*filt).filter(checkers=True, update__isnull=False) \
                                                .exclude(query) \
                                                .exclude(country='-') \
                                                .exclude(id__in=order)

        snippets = s[p:p2]
        s_c = s.count()
        serializer = ProxySerializer(snippets, many=True)
        return JsonResponse({"search":serializer.data, "search_count": s_c}, safe=False)


def ajax_ip_detail(request):
    """
    Подробнее о ip
    """
    if request.is_ajax():
        id_ip = request.GET.get('id', None)
        if id_ip:
            obj = get_list_or_404(Proxy, pk=id_ip)
            serializer = ProxyIpSerializer(obj, many=True)
            return JsonResponse(serializer.data, safe=False)
        else:
            return HttpResponse(json.dumps({"error":"error"}))
