#-*-coding:utf-8 -*-
import datetime
import time
import os
import random

from django.core.mail import send_mail
from django.conf import settings

from celery.task.base import periodic_task, task
from celery.exceptions import TimeLimitExceeded, SoftTimeLimitExceeded


from proxy.utils import del_db, del_db_auth, del_dubli, del_db_orm

from bc.static_memory import trace

queues = ['185.235.245.4', '185.235.245.8', '185.235.245.14']


os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'


@task(name="отправка-почты", soft_time_limit=60*2)
@trace
def send_mail_error(msg):
	if not isinstance(msg, str):
		log.info("не строка")
		return
	send_mail(
		'ERROR-TASKS',
		msg,
		'johnmnemonik.jm@gmail.com',
		settings.LIST_OF_EMAIL_RECIPIENTS,
		fail_silently=False,
)


@periodic_task(
	name='ОЧИСТА-ОТ-ДУБЛЕЙ',
	run_every=datetime.timedelta(minutes=60*2),
	queue=random.choice(queues),soft_time_limit=600*18)
@trace
def starter_parser():
	try:
		msg = del_db()
	except (SoftTimeLimitExceeded, TimeLimitExceeded):
		msg = "время вышло"
		send_mail_error.apply_async(args=(msg,), queue=random.choice(queues))
	return msg