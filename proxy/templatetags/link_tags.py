from django import template
from proxy.models import Proxy
register = template.Library()
 
@register.inclusion_tag('proxy/link.html')
def display_link(object_id):
    link = Proxy.objects.filter(id=object_id)
    return { 'links': link }