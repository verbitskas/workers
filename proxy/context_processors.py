import datetime

from django.utils import timezone

from proxy.models import Proxy, TimeSet


def country(request):
    cou = list(set([x.country for x in Proxy.objects.all()]))

    try:
        cou.remove('')
    except Exception:
        pass

    return {'country':cou}


def count_socks(request):
    ts = TimeSet.objects.first()
    now = timezone.now()
    try:
        offset = now - datetime.timedelta(minutes=ts.offset)
    except AttributeError:
        offset = False

    if offset:
        cou = Proxy.objects.filter(update__gte=offset).count()

    else:
        cou = Proxy.objects.filter(checkers=True, update__isnull=False).count()

    return {'counts':cou}


from django.contrib.sites.models import Site

def site(request):
    return {'site': Site.objects.get_current()}
