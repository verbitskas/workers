#-*-coding:utf-8-*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.db.models import Avg, Q

from .models import (Proxy, ProxyAuth, ProxyList, SettingsProxyScan, \
   TimeSet, Worker, CityProxy, CountryProxy)
from django.http import HttpResponseRedirect


class BlackListFilterDetail(admin.SimpleListFilter):
   title = _('Фильтр по колличеству/блеклистов')
   parameter_name = "black_count"

   def lookups(self, request, model_admin):
      return (
         ('1', _('один')),
         ('2', _('два')),
         ('3', _('три')),
         ('4', _('четыри')),
         ('5', _('пять')),
         ('6', _('шесть')),
         ('7', _('семь')),

      )

   def queryset(self, request, queryset):
      if self.value() == '1':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if len(x.blacklist.split()) == 1]
         return queryset.filter(id__in=ids)

      if self.value() == '2':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if len(x.blacklist.split()) == 2]
         return queryset.filter(id__in=ids)

      if self.value() == '3':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if len(x.blacklist.split()) == 3]
         return queryset.filter(id__in=ids)

      if self.value() == '4':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if len(x.blacklist.split()) == 4]
         return queryset.filter(id__in=ids)

      if self.value() == '5':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if len(x.blacklist.split()) == 5]
         return queryset.filter(id__in=ids)

      if self.value() == '6':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if len(x.blacklist.split()) == 6]
         return queryset.filter(id__in=ids)

      if self.value() == '7':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if len(x.blacklist.split()) == 7]
         return queryset.filter(id__in=ids)


class BlackListFilter(admin.SimpleListFilter):
   title = _('Соксы по блеклистам')
   parameter_name = "black"

   def lookups(self, request, model_admin):
      return (
         ('sbl.spamhaus.org', _('sbl.spamhaus.org')),
         ('xbl.spamhaus.org', _('xbl.spamhaus.org')),
         ('css.spamhaus.org', _('css.spamhaus.org')),
         ('pbl.spamhaus.org', _('pbl.spamhaus.org')),
         ('cbl.abuseat.org', _('cbl.abuseat.org')),
         ('bl.spamcop.net', _('bl.spamcop.net')),
         ('b.barracudacentral.org', _('b.barracudacentral.org')),
         ('False', _('чистый socks')),
      )

   def queryset(self, request, queryset):
      if self.value() == 'sbl.spamhaus.org':
         return queryset.filter(blacklist='sbl.spamhaus.org')

      if self.value() == 'xbl.spamhaus.org':
         return queryset.filter(blacklist='xbl.spamhaus.org')

      if self.value() == 'css.spamhaus.org':
         return queryset.filter(blacklist='css.spamhaus.org')

      if self.value() == 'pbl.spamhaus.org':
         return queryset.filter(blacklist='pbl.spamhaus.org')

      if self.value() == 'cbl.abuseat.org':
         return queryset.filter(blacklist='cbl.abuseat.org')

      if self.value() == 'bl.spamcop.net':
         return queryset.filter(blacklist='bl.spamcop.net')

      if self.value() == 'b.barracudacentral.org':
         return queryset.filter(blacklist='b.barracudacentral.org')

      if self.value() == 'False':
         return queryset.filter(blacklist__isnull=True)


class BlackListFilterIn(admin.SimpleListFilter):
   title = _('Соксы в блеклистах')
   parameter_name = "black_in"

   def lookups(self, request, model_admin):
      return (
         ('sbl.spamhaus.org', _('sbl.spamhaus.org')),
         ('xbl.spamhaus.org', _('xbl.spamhaus.org')),
         ('css.spamhaus.org', _('css.spamhaus.org')),
         ('pbl.spamhaus.org', _('pbl.spamhaus.org')),
         ('cbl.abuseat.org', _('cbl.abuseat.org')),
         ('bl.spamcop.net', _('bl.spamcop.net')),
         ('b.barracudacentral.org', _('b.barracudacentral.org')),
         ('False', _('чистый socks')),
      )

   def queryset(self, request, queryset):
      if self.value() == 'sbl.spamhaus.org':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if 'sbl.spamhaus.org' in x.blacklist.split()]
         return queryset.filter(id__in=ids)

      if self.value() == 'xbl.spamhaus.org':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if 'xbl.spamhaus.org' in x.blacklist.split()]
         return queryset.filter(id__in=ids)

      if self.value() == 'css.spamhaus.org':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if 'css.spamhaus.org' in x.blacklist.split()]
         return queryset.filter(id__in=ids)

      if self.value() == 'pbl.spamhaus.org':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if 'pbl.spamhaus.org' in x.blacklist.split()]
         return queryset.filter(id__in=ids)

      if self.value() == 'cbl.abuseat.org':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if 'cbl.abuseat.org' in x.blacklist.split()]
         return queryset.filter(id__in=ids)

      if self.value() == 'bl.spamcop.net':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if 'bl.spamcop.net' in x.blacklist.split()]
         return queryset.filter(id__in=ids)

      if self.value() == 'b.barracudacentral.org':
         ids = [x.id for x in queryset.filter(blacklist__isnull=False) if 'b.barracudacentral.org' in x.blacklist.split()]
         return queryset.filter(id__in=ids)

      if self.value() == 'False':
         ids = [x.id for x in queryset.filter(blacklist__isnull=True)]
         ids2 = [x.id for x in queryset.filter(blacklist='')]
         #ids = Proxy.objects.values_list('id', flat=True).filter(blacklist__isnull=True)
         #ids2 = Proxy.objects.values_list('id', flat=True).filter(blacklist='')
         #val = ids+ids2
         return queryset.filter(id__in=ids+ids2)



from proxy.form import ProxyModelForm, BackForm, AdminProxyModelForm

class ProxyAdmin(admin.ModelAdmin):
   #form = AdminProxyModelForm
   fields = (('ip','port','ipreal','dns','vendor'),
      ('country','region','city',),
      ('continent','postal_code','worker'),
      ('region_code','time_zone'),
      ('lon','lat','timeout'),
      ('tp','checkers','domain','auth', 'scan'),
      ('usr','pswd', 'blacklist'),
      ('speed','typeproxy','anonymity'),
      ('gmt','time_region','update'),)
   
   empty_value_display = 'unknown'
   list_display = ('ip', 'port', 'ipreal', 'region','country','tp','checkers',
      'anonymity','update','created', 'blacklist')
   list_filter = (BlackListFilterDetail, BlackListFilterIn ,
      "tp", "created", "worker", "speed","vendor","anonymity","checkers","country")
   list_display_links = ('ip', 'ipreal')
   list_per_page = 100
   search_fields = ("country","city","ip","ipreal","port")

   def birth_date_view(self, obj):
      return obj.ip

   birth_date_view.empty_value_display = 'unknown'

from django.utils.html import format_html
from django.core.urlresolvers import reverse
from django.conf.urls import url
from django.template.response import TemplateResponse

class ProxyAdminAuth(admin.ModelAdmin):
   form = AdminProxyModelForm
   fields = (('ip','port','ipreal','dns'),
      ('country','city','timeout'),
      ('continent','postal_code','usr'),
      ('region_code','time_zone','pswd','iploc'),
      ('lon','lat'),
      ('tp','checkers','blacklist','vendor'),
      ('speed','typeproxy','anonymity',),
      ('gmt','time_region','update'),)
   
   empty_value_display = 'unknown'
   list_display = ('id','ip','port', 'ipreal','usr', 'pswd','country','tp','checkers',
      'anonymity','update','created', 'blacklist')

   list_filter = ("blacklist","speed","vendor","anonymity","checkers","country")
   list_per_page = 100
   search_fields = ("country","city","ip","ipreal","usr")
   actions = ('birth_date_view')

   def birth_date_view(self, obj):
      return obj.ip

   birth_date_view.empty_value_display = 'unknown'
   
   birth_date_view.short_description = u'описание'

class CityAdmin(admin.ModelAdmin):
   fields = ('city',)

class CoutryAdmin(admin.ModelAdmin):
   fields = ('coutry',)

class SettingsAdmin(admin.ModelAdmin):
   fields = (('checktime','cle',),)

class TimeAdminSet(admin.ModelAdmin):
   fields = ('offset',)

@admin.register(Worker)
class WorkerAdmin(admin.ModelAdmin):
   list_display = ('ip',)
   fields = ('ip',)

admin.site.register(CountryProxy, CoutryAdmin)
admin.site.register(CityProxy, CityAdmin)
admin.site.register(Proxy, ProxyAdmin)
admin.site.register(ProxyAuth, ProxyAdminAuth)
admin.site.register(SettingsProxyScan, SettingsAdmin)
admin.site.register(TimeSet, TimeAdminSet)
