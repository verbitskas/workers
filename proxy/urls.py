#-*-coding:utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.decorators import login_required, permission_required

from proxy.views import index, detail, ipdetail, change_view, req_form, \
    service, Ip, other, ajax_form, socks_online #City

from prof.views import profile



urlpatterns = [
    url(r"^search/(?P<count>[\w+|\w+\s+\w+|\W+\w+|\w+\S+\w+\S+\w+]+)$", detail, name='search'),
   # url(r"^city/(?P<city>[\w+|\w+\s+\w+|\W+\w+|\w+\S+\w+\S+\w+]+)$", City.as_view(), name='city'),
    url(r"^ip/(?P<id>[\w+]+)$", ipdetail, name='detail'),
    url(r'^service/', service, name='service'),
    url(r'^filter/', ajax_form, name='proxy-list'),
    url(r"^ip/(?P<host>[\w+])$", detail, name='search'),
    url(r'^$', index, name='index'),

    url(r'^change_view/$', change_view, name='change_view'),
    url(r'^socks_online/$', socks_online, name='socks_online'),
]


from . import ajax

urlajax = [
    url(r'ajax_all_country/', ajax.ajax_all_country, name='ajax_all_country'),
    url(r'ajax_dj_region/', ajax.ajax_dj_region, name='ajax_dj_region'),
    url(r'ajax_dj_city/', ajax.ajax_dj_city, name='ajax_dj_city'),
    url(r'ajax_post_region/', ajax.ajax_post_region, name='ajax_region_post'),
    url(r'ajax_search_form/', ajax.ajax_search_form, name='ajax_search'),
    url(r'ajax_all_vendors/', ajax.ajax_all_vendors, name='ajax_all_vendors'),
    url(r'ajax_search/', ajax.ajax_new_search, name='ajax_new_search'),
    url(r'ajax_ip_detail/', ajax.ajax_ip_detail, name='ajax_ip_detail'),

    url(r'ajax_continent/', ajax.ajax_continent, name='ajax-continent'),
    url(r'ajax_zone/', ajax.ajax_zone, name='ajax-zone'),
    url(r'ajax_code_country/', ajax.ajax_code_country, name='ajax-code-coutry'),
    url(r'ajax_code_region/', ajax.ajax_code_region, name='ajax-code-region'),
    url(r'ajax_code_postal/', ajax.ajax_code_postal, name='ajax-code-postal'),

]

urlpatterns += urlajax
