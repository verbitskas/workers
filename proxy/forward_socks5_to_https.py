import asyncio
import logging
import concurrent.futures
import ssl
import socket
import sys
import os
import functools
import errno
import signal
import time
import resource

from struct import pack, unpack
from multiprocessing import Process
from traceback import print_exc
from socket import TCP_NODELAY
from functools import wraps

from daemonize import daemonize

sys.path.insert(0, os.path.dirname("../" + __file__))
sys.path.append('/home/john/proxyproject/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

import django
django.setup()

from client.models import IplistBack
from proxy.models import Proxy

logging.basicConfig(
    #filename="socks5_to_https.log",
    format="%(levelname)-10s строка %(lineno)d %(asctime)s %(message)s",
    level=logging.INFO
    )

logger = logging.getLogger('socks')



def chunks(lst, count):
    """Группировка элементов последовательности по count элементов"""
    start = 0
    for i in range(count):
        stop = start + len(lst[i::count])
        yield lst[start:stop]
        start = stop


class TimeoutError(Exception):
	pass


def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
	""" Декоратор перезапуска """
	def decorator(func):
		def _handle_timeout(signum, frame):
			raise TimeoutError(error_message)

		def wrapper(*args, **kwargs):
			signal.signal(signal.SIGALRM, _handle_timeout)
			signal.alarm(seconds)
			try:
				result = func(*args, **kwargs)
			finally:
				signal.alarm(0)
			return result

		return wraps(func)(wrapper)

	return decorator



@asyncio.coroutine
def forwarder(reader, writer):
	""" Две задачи пишут друг в друга client/server и server/client """
	try:
		while True:
			try:
				data = yield from reader.read(1024)
				if not data:
					break
			except ConnectionResetError:
				break
			writer.write(data)
			yield from writer.drain()
	except:
		print_exc()
	writer.close()


@asyncio.coroutine
def server_socks(result, ipdos, portallow, loc,reader, writer):
	""" сервер прокладка"""
	destip, destport = result
	logger.info(result)
	logger.info(ipdos)
	logger.info(portallow)

	peername = writer.get_extra_info('peername')[0]
	logger.info(peername)
	if ipdos and peername not in ipdos:
		writer.close()
		return
	while True:
		try:
			data = yield from reader.read(1024)
			if data == b'\x05\x01\x00' or data == b'\x05\x02\x00\x01':
				writer.write(b'\x05\x00') # 2
				yield from writer.drain()


			elif data.startswith(b"\x05\x01\x00\x01"):
				# ip
				domain = ".".join([str(x) for x in unpack('>BBBB', data[4:-2])])
				port = str(unpack('>H', data[-2:])[0])

				header = [
					b"CONNECT %b:%b HTTP/1.1\r\n" % (bytes(domain, 'utf-8'), bytes(port, 'utf-8')),
					b"Host: %b:%b\r\n" % (bytes(domain, 'utf-8'), bytes(port, 'utf-8')),
					b"User-Agent: User Agent\r\n",
					b"Accept: */*\r\n",
					b"Proxy-Connection: keep-alive\r\n"
					]

				reader_http, writer_http = yield from asyncio.open_connection(destip, destport, flags=TCP_NODELAY) # 1
				[writer_http.write(head) for head in header] # 2
				writer_http.write(b"\r\n")

				data = yield from reader_http.readline()

				if data.startswith(b'HTTP/1.1 200'):
					data = b"\x05\x00\x00\x01" + socket.inet_aton('217.23.13.144') + pack("!H", loc)
					writer.write(data)
					yield from writer.drain()
					
					tasks = [
						asyncio.ensure_future(forwarder(reader, writer_http)),
						asyncio.ensure_future(forwarder(reader_http, writer)),
						]

					yield from asyncio.wait(tasks)


			elif data.startswith(b"\x05\x01\x00\x03"):
				# host
				domain = data[5:-2].decode()
				port = str(unpack('>H', data[-2:])[0])

				header = [
					b"CONNECT %b:%b HTTP/1.1\r\n" % (bytes(domain, 'utf-8'), bytes(port, 'utf-8')),
					b"Host: %b:%b\r\n" % (bytes(domain, 'utf-8'), bytes(port, 'utf-8')),
					b"User-Agent: User Agent\r\n",
					b"Accept: */*\r\n",
					b"Proxy-Connection: keep-alive\r\n"
					]

				reader_http, writer_http = yield from asyncio.open_connection(destip, destport, flags=TCP_NODELAY) # 1
				[writer_http.write(head) for head in header] # 2
				writer_http.write(b"\r\n")

				data = yield from reader_http.read(1024)

				if data.startswith(b'HTTP/1.1 200'):
					data = b"\x05\x00\x00\x01" + socket.inet_aton('217.23.13.144') + pack("!H", loc)
					writer.write(data)
					yield from writer.drain()

					tasks = [
						asyncio.ensure_future(forwarder(reader, writer_http)),
						asyncio.ensure_future(forwarder(reader_http, writer)),
						]

					yield from asyncio.wait(tasks)
					writer_http.close()
			else:
				break
		except concurrent.futures.TimeoutError:
			break
		except ConnectionResetError as exc:
			break
		except Exception:
			break

	writer.close()


@timeout(60*15)
def start(loc_ports, results, ipdos, portallow):
	#sc = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
	#sc.load_cert_chain('ca.crt', 'ca.key')

	loop = asyncio.get_event_loop()
	func = [functools.partial(server_socks, res, ipdos, portallow, loc) for res,loc in zip(results, loc_ports)]
	coro = [asyncio.start_server(f, "0.0.0.0", port=port) for f, port in zip(func, loc_ports)]

	asyncio.gather(*coro)
	logger.info(len(coro))
	logger.info(len(func))
	try:
		logger.info("сервер старт")
		loop.run_forever()
	except KeyboardInterrupt:
		sys.stderr.flush()
		loop.stop()
		raise SystemExit("exit")  
	except concurrent.futures.TimeoutError:
		logger.info("restart 2")
	except Exception as exc:
		logger.info("перезапуск")
	finally:
		loop.stop()


def do_start_orm():
    try:
        i = IplistBack.objects.get(server__ip__in=['217.23.13.144'])
    except IplistBack.DoesNotExist:
        logger.info("список не обнаружен.......")
        return None, None, None, None
    
    except IplistBack.MultipleObjectsReturned:
        logger.exception("модель имеет 2 записи")
        return (None, None, None, None)
    
    port = eval(i.server_dicts['217.23.13.144'])
    ipdos = i.ip
    if i.portallow:
        portallow = i.portallow.split()
    
    else:
        portallow = None
    try:
        ipdos = ipdos.split(' ')
    except AttributeError as exc:
        ipdos = None

    s1 = i.dicts[-1]
    s = len(port)
    results = Proxy.objects.filter(i.dicts[0])[:s]
    count = results.count()
    
    ip_port = [(x.ip,int(x.port)) for x in results]
    port = port[:len(ip_port)]
    logger.info(len(ip_port))
    logger.info(len(port))
    return port, ip_port, ipdos, portallow

def main():
	while True:
		locals_port, results, ipdos, portallow = do_start_orm()
		logger.info(len(locals_port))
		logger.info(len(results))
		try:
			if results:
				loc = chunks(locals_port, 4)
				rem = chunks(results, 4)
				try:
					logger.info(" --- start ---")
					target = [Process(target=start, args=(k,v, ipdos, portallow)) for k,v in zip(loc,rem)]
					for proc in target:
						proc.start()
					for proc in target:
						proc.join()
				except Exception as e:
					pass
			else:
				logger.info("пусто")
				time.sleep(60*5)
		except concurrent.futures.TimeoutError:
			logger.info("restart 3")
		except KeyboardInterrupt:
			raise SystemExit("exit")
		except Exception as exc:
			logger.info(exc)

if __name__ == '__main__':
	daemonize(stdout='/tmp/portforward.log',stderr='/tmp/portforwarderror.log')
	try:
		resource.setrlimit(resource.RLIMIT_NOFILE, (100000, 100000))
	except Exception:
		pass

	main()