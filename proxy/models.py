#-*-coding:utf-8 -*-
from django.db import models, connection
from django.utils.translation import ugettext_lazy as _

from django.contrib.postgres.fields import ArrayField
from django.utils.encoding import python_2_unicode_compatible
from django.conf import settings

from vendor.models import Vendors

User = settings.AUTH_USER_MODEL

TYPE_PROXY = (
    ('dp',_('Direct Proxy')),
    ('bp',_('Backconnect Proxy')),
)

ANONYM = (
    ('no',_(u'Нет Анонимности')),
    ('yes',_(u'Полная Анонимность')),
    ('medium',_(u'Средняя'))
)

TP = (
    ('http',_('HTTP')),
    ('https',_('HTTPS')),
    ('https_auth',_('HTTPS с авторизацией')),
    ('socks4',_('SOCKS4')),
    ('socks5',_('SOCKS5'))
)

class UpdateManager(models.Manager):
    def black(self, dic):
        cur = connection.cursor()
        val = [(v,k) for k,v in dic.items()]
        sql = "UPDATE proxy_proxy SET blacklist=%s WHERE ip = %s"
        cur.executemany(sql,val)


    def parser(self, dic):
        cur = connection.cursor()
        cur.execute("SELECT ip,port FROM proxy_proxy")
        fetch = cur.fetchall()
        host = ["{}".format(x[0]) for x in fetch]

        if host:
            s = {}
            for x in host:
                try:
                    s[x] = dic.pop(x)

                except Exception:
                    pass

        if s:
            sql = "UPDATE proxy_proxy SET port=%s WHERE ip = %s"
            d = [(v[2], k) for k,v in s.items()]

            cur.executemany(sql,d)

        dicts = [x for x in dic.values()]
        sql = """
            INSERT INTO proxy_proxy (
            vendor_id,ip,port,country,city,continent,
            country_code,lat,lon,postal_code,region_code,time_zone,tp, anonymity,checkers,timeout,gmt,time_region)
            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""

        cur.executemany(sql,dicts)


    def par(self, dic):
        cur = connection.cursor()
        up = timezone.now()

        val = [(v[0],up,k.split(':')[0],k.split(':')[1]) for k,v in dic.items() if v[0] == True]
        sql = "UPDATE proxy_proxy SET checkers=%s, update=%s WHERE ip = %s AND port = %s"

        cur.executemany(sql,val)

        val = [(v[0],k.split(':')[0],k.split(':')[1]) for k,v in dic.items() if v[0] == False]
        sql = "UPDATE proxy_proxy SET checkers=%s WHERE ip = %s AND port = %s"

        cur.executemany(sql,val)




@python_2_unicode_compatible
class CityProxy(models.Model):
    city = models.CharField(_(u'Город'), max_length=300, blank=True, null=True,unique=True)

    def __str__(self):
        return self.city

    class Meta:
        verbose_name = _(u'Город')
        verbose_name_plural = _(u'Города')


@python_2_unicode_compatible
class CountryProxy(models.Model):
    country = models.CharField(_(u'Страна'), max_length=300, blank=True, null=True, unique=True)

    def __str__(self):
        return self.country

    class Meta:
        verbose_name = _(u'Страна')
        verbose_name_plural = _(u'Страны')
        ordering = ('country',)


@python_2_unicode_compatible
class Worker(models.Model):
    ip = models.CharField(_(u"ip"),max_length=100, blank=True, null=True)
    jobs = models.IntegerField(_(u"колличество использования"),blank=True,default=0)
    flag = models.BooleanField(_(u"занят?"),default=False)

    def __str__(self):
        return self.ip

    class Meta:
        verbose_name = _(u'Воркер')
        verbose_name_plural = _(u'Воркеры')


@python_2_unicode_compatible
class Proxy(models.Model):
    worker = models.ForeignKey(Worker,verbose_name=_(u"воркеры"))
    vendor = models.ForeignKey(User,verbose_name=_('Продавец'),blank=True, null=True)
    vendor_socks = models.ForeignKey(Vendors, verbose_name=_("Продавец сокса"), null=True, blank=True)

    ip = models.CharField(_(u'ip адрес'),max_length=1000,blank=True, null=True,db_index=True)
    port = models.CharField(_(u'порт'),max_length=1000,blank=True, null=True,db_index=True)

    ipreal = models.CharField(_(u'ip адрес (реальный ip)'),max_length=1000,blank=True,
        null=True,db_index=True)

    dns = models.CharField(_(u'dns'),max_length=1000,blank=True, null=True,db_index=True)
    usr = models.CharField(_(u'логин'),max_length=1000,blank=True, null=True, db_index=True)
    pswd = models.CharField(_(u'пароль'),max_length=1000,blank=True, null=True)

    country = models.CharField(_(u"Страна"),max_length=1000,blank=True,null=True)
    city = models.CharField(_(u"Город"),max_length=1000,blank=True,null=True)

    continent = models.CharField(_(u"Континент"),max_length=1000,blank=True,null=True)
    country_code = models.CharField(_(u"Код страны"),max_length=1000,blank=True,null=True)

    lat = models.CharField(_(u'Широта'), blank=True, null=True, max_length=1000)
    lon = models.CharField(_(u'Долгота'), blank=True, null=True, max_length=1000)

    postal_code = models.CharField(_("Почтовый код"),max_length=1000,blank=True,null=True)
    region_code = models.CharField(_("Код региона"),max_length=1000,blank=True,null=True)

    time_zone = models.CharField(_(u'Часовой пояс'), blank=True, null=True, max_length=1000)

    gmt = models.CharField(_(u'Gmt'),max_length=1000,blank=True, null=True)
    time_region = models.DateTimeField(_(u'Врями + зона'),blank=True, null=True)

    tp = models.CharField(_(u'Тип Прокси'),choices=TP,max_length=1000,blank=True,null=True)

    checkers = models.BooleanField(_(u'Проверен'),default=True)

    speed = models.CharField(_(u'скорость'),max_length=1000,blank=True,null=True)
    typeproxy = models.CharField(_(u'Тип Прокси'),max_length=1000,choices=TYPE_PROXY, default='dp', blank=True,null=True)

    anonymity = models.CharField(_(u'Анонимность'),choices=ANONYM, max_length=1000, blank=True, null=True)

    update = models.DateTimeField(_(u'Последние обновлеие'),blank=True,null=True)
    created = models.DateTimeField(_(u'создан'),blank=True,null=True)
    online = models.DateTimeField(_(u'онлайн'),blank=True,null=True)

    timeout = models.CharField(_(u'Отклик'),max_length=2000,blank=True,null=True)

    region = models.CharField(_("Область/Штат"),max_length=1000,blank=True,null=True)
    domain = models.CharField(_("Домен"),max_length=1000,blank=True,null=True)

    isp = models.CharField(_("isp провайдер"),max_length=1000,blank=True,null=True)


    blacklist = models.CharField(_(u"Блек-Лист"), max_length=5000, blank=True, null=True)
    auth = models.BooleanField(_(u"Auth?"), default=False)
    scan = models.BooleanField(_("не сканируеться"), default=False)

    #socks_list = models.BooleanField(_("Есть сокс в листе"), default=True)

    objects = models.Manager()
    geo = UpdateManager()


    def __str__(self):
        return "{0}:{1}".format(self.ip,self.port)

    def blacklist_names(self):
        bl = ', '.join([a.title for a in self.blacklist.all()])
        if bl:
            return bl
        else:
            return u"Нет в Блек-Лист'е"

    @property
    def hidden_ip(self):
        return ".".join(self.ip.split('.')[:2] + ['***','***'])

    @property
    def hidden_ipreal(self):
        return ".".join(self.ipreal.split('.')[:2] + ['***','***'])

    @property
    def black_split(self):
        return self.blacklist.split()

    @property
    def black_count(self):
        count = self.blacklist.split()
        return len(count)

    blacklist_names.short_description = "Black-List"

    @property
    def black_int(self):
        pass


    class Meta:
        verbose_name = _(u'Прокси')
        verbose_name_plural = _(u'Прокси')
        ordering = ('-update',)
        #unique_together = ("ip", "ipreal",  "vendor",)
        unique_together = ("ipreal",  "vendor",)


@python_2_unicode_compatible
class ProxyAuth(models.Model):
    worker = models.ManyToManyField(Worker,verbose_name=_(u"воркеры"))
    vendor = models.ForeignKey(User,verbose_name=_(u'Продавец'))
    iploc = models.CharField(_(u'локальный ip адрес'),max_length=255,blank=True, null=True)

    ip = models.CharField(_(u'ip адрес'),max_length=255,blank=True, null=True)
    ipreal = models.CharField(_(u'ip адрес (реальный ip)'),max_length=255,blank=True, null=True,db_index=True)
    port = models.CharField(_(u'порт'),max_length=255,blank=True, null=True)
    usr = models.CharField(_(u'логин'),max_length=255,blank=True, null=True, db_index=True)
    pswd = models.CharField(_(u'пароль'),max_length=255,blank=True, null=True)

    country = models.CharField(_(u"Страна"),max_length=255,blank=True,null=True)
    city = models.CharField(_(u"Город"),max_length=255,blank=True,null=True)

    continent = models.CharField(_(u"Континент"),max_length=100,blank=True,null=True)
    country_code = models.CharField(_(u"Код страны"),max_length=100,blank=True,null=True)

    lat = models.CharField(_(u'Широта'), blank=True, null=True, max_length=100)
    lon = models.CharField(_(u'Долгота'), blank=True, null=True, max_length=100)

    postal_code = models.CharField(_(u"Почтовый код"),max_length=100,blank=True,null=True)
    region_code = models.CharField(_(u"Код региона"),max_length=100,blank=True,null=True)

    time_zone = models.CharField(_(u'Часовой пояс'), blank=True, null=True, max_length=100)

    gmt = models.CharField(_(u'Gmt'),max_length=255,blank=True, null=True)
    time_region = models.DateTimeField(_(u'Врями + зона'),blank=True, null=True)

    tp = models.CharField(_(u'Тип Прокси'),choices=TP,max_length=20,blank=True,null=True)

    checkers = models.BooleanField(_(u'Проверен'),default=True)

    speed = models.CharField(_(u'скорость'),max_length=255,blank=True,null=True)
    typeproxy = models.CharField(_(u'Тип Прокси'),max_length=2,choices=TYPE_PROXY,blank=True,null=True)

    anonymity = models.CharField(_(u'Анонимность'),choices=ANONYM,max_length=10, blank=True, null=True)

    update = models.DateTimeField(_(u'Последние обновлеие'),blank=True,null=True)
    created = models.DateTimeField(_(u'создан'),blank=True,null=True)
    online = models.DateTimeField(_(u'онлайн'),blank=True,null=True)

    timeout = models.CharField(_(u'Отклик'),max_length=255,blank=True,null=True)
    blacklist = models.CharField(_(u"Блек-Лист"), max_length=500, blank=True, null=True)

    isp = models.CharField(_("isp провайдер"),max_length=1000,blank=True,null=True)

    objects = models.Manager()
    geo = UpdateManager()


    def __str__(self):
        return "{0}:{1}".format(self.ip,self.port)

    def blacklist_names(self):
        bl = ', '.join([a.title for a in self.blacklist.all()])
        if bl:
            return bl
        else:
            return u"Нет в Блек-Лист'е"

    def run_connect(self):
        return "run connect"

    blacklist_names.short_description = "Black-List"
    run_connect.short_description = "Connect-Run"


    class Meta:
        verbose_name = _(u'Прокси с Auth')
        verbose_name_plural = _(u'Прокси с Auth')
        ordering = ('-update',)
        #unique_together = ("iploc","usr","pswd")
        #unique_together = ("usr")

@python_2_unicode_compatible
class ProxyList(models.Model):
    user = models.ForeignKey(User,verbose_name=_(u"Продавец"))
    listproxy = ArrayField(models.CharField(max_length=200),blank=True)

    def __str__(self):
        return _(u"Продавец %s" % self.user)

@python_2_unicode_compatible
class SettingsProxyScan(models.Model):
    checktime = models.IntegerField(_(u'Регулярность проверки в минутах.'),
        help_text=_(u'''
            Отнеситесь с осторожностью к этому памаметру,
            если указать маленький интервал то есть вероятность
            переполнения запусков чекера'''))
    cle = models.IntegerField(_(u'Время очистки'),
        help_text=_(u"Интервал времени через которое будет вызвана очистка БД от мертвых прокси."))

    def __str__(self):
        return u"Очистка БД. через {} часа\nЗапуска Чекера каждые {} мин.".format(self.cle,self.checktime)

    class Meta:
        verbose_name = _(u'Настройка')
        verbose_name_plural = _(u'Настройки')


@python_2_unicode_compatible
class TimeSet(models.Model):
    offset = models.IntegerField(_(u"Минуты"),help_text=_(u"укажите время вывода в минутах"))

    def __str__(self):
        return u"показывать прокси за последнии {} минут.".format(self.offset)

    class Meta:
        verbose_name = _(u"Настройка времени")
        verbose_name_plural = _(u"Настройка времени")



def return_rename(user):
    obj = Proxy.objects.filter(vendor=user)
    return obj


'''
class Geo(models.Model):
    name = models.CharField("название", max_length=255, blank=True, null=True)
    geo  = models.FileField("geo", upload_to='/home/john/proxyproject/geo/')
    servers = models.CharField("сервера", max_length=2000)

    def __str__(self):
        return self.name

    def geo_ssh():
        import paramiko
        ssh = paramiko.SSHClient()
        
        for server in self.servers:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(server, username="john",password="Jvtkmxtyrj")
            ftp = ssh.open_sftp()
            ftp.chdir('proxyproject')
            ftp.put("/home/john/proxyproject/geo/geo.bin", 'geo/geo.bin')
            ftp.close()
            ssh.close()



    def func_geo_upload(self):
        import os
        path = '/home/john/proxyproject/geo'
        os.chdir(path)
        os.rename(self.name, 'geo.bin')

    def save(self, *args, **kwargs):
        super(Clientlist, self).save(*args, **kwargs)
        self.func_geo_upload()


'''