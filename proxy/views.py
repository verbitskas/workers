#-*-coding:utf-8 -*-
import datetime
import json
import operator
from functools import reduce

from django.utils import timezone
from django.http import Http404
from django.contrib.auth import logout
from django.utils.html import format_html
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.views.generic import ListView, View, DetailView, TemplateView
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.cache import cache_page
from django.db.models import Q
from django.core.urlresolvers import reverse

from django.core.exceptions import PermissionDenied

from proxy.models import Proxy, TimeSet, CountryProxy, ProxyAuth
from vendor.models import Blacklist, Vendors
from client.models import Iplist

from pay.tasks import check_speed, _online
from proxy.utils import check_blacklist
from proxy.form import ProxyModelForm
from client.models import Do

from client.models import Iplist, Clientlist, IplistAuth, Port, IplistBack, ClientlistBack

from django.contrib.auth import get_user_model
User = get_user_model()

from django.db.models import Count

from news.models import News

from django.conf import settings
from django.utils.module_loading import import_string
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
REGISTRATION_FORM_PATH = getattr(settings, 'REGISTRATION_FORM',
                                 'registration.forms.RegistrationForm')
REGISTRATION_FORM = import_string(REGISTRATION_FORM_PATH)


@cache_page(60*1)
def index(request):
    """Вывод статистики по странам на главной
       Окна входа, регистрации, сброса пароля
    """
    # try:
    #   link = dict(CountryProxy.objects.all())
    # except:
    #   link = list(set([x.country for x in Proxy.objects.all()]))
    # try:
    #   link.remove(None)
    # except:
    #   pass
    # return render(request,'proxy/index.html', {'country':link})
    # ts = TimeSet.objects.first()
    # now = timezone.now()
    # try:
    #     offset = now - datetime.timedelta(minutes=ts.offset)
    # except:
    #     offset = None
    coun = CountryProxy.objects.values('country').order_by('country')# \
                                                #.exclude(country='Brazil')
    # Для диаграммы
    # if offset:
    #     link = Proxy.objects.filter(country__in=coun, update__gte=offset) \
    #                                     .order_by('country') \
    #                                     .exclude(country='Brazil') \
    #                                     .exclude(country='-') \
    #                                     .values_list('country') \
    #                                     .annotate(Count('country')) \
    #                                     .order_by('-country__count')[:9]
    # else:
    #     link = Proxy.objects.filter(country__in=coun, checkers=True) \
    #                                     .exclude(country='Brazil') \
    #                                     .exclude(country='-') \
    #                                     .order_by('country') \
    #                                     .values_list('country') \
    #                                     .annotate(Count('country')) \
    #                                     .order_by('-country__count')[:9]
    # d =  list({'c':[{'v': x[0]}, {'v': x[1]}]} for x in link)
    # Для карты
    maps = Proxy.objects.filter(country__in=coun, checkers=True, update__isnull=False) \
                                        .exclude(country='Brazil') \
                                        .exclude(country='-') \
                                        .order_by('country') \
                                        .values_list('country') \
                                        .annotate(Count('country')) \
                                        .order_by('-country__count')
    maps_d = Proxy.objects.filter(country='Brazil', checkers=True, update__isnull=False).count()
    dic =  {'c':[{'v': "Brazil"}, {'v': maps_d}]}
    dicts =  list({'c':[{'v': x[0]}, {'v': x[1]}]} for x in maps)
    dicts.append(dic)
    # Вывод стран которых нет в прокси и присвоение 0
    p = Proxy.objects.values('country').order_by('country').distinct('country')
    d = CountryProxy.objects.exclude(country__in=p).exclude(country='-') \
                                                .order_by('country') \
                                                .values_list('country') \
                                                .annotate(Count('country')) \
                                                .order_by('-country__count')
    dicts2 =  list({'c':[{'v': x[0]}, {'v': 0}]} for x in d)
    dicts += dicts2

    # Новости
    news = News.objects.all()[:3]
    #context = {'country':d, "map": dicts, 'maps_d': dic, 'news': news}
    context = {"map": dicts, 'maps_d': dic, 'news': news}
    # Всплывающее окно логина
    if 'modal' in request.COOKIES:
        context["modal"] = True
        context["form"] = AuthenticationForm()
    #if 'modal_reg' in request.COOKIES:
    context["modal_reg"] = True
    context["form_reg"] = REGISTRATION_FORM
    if 'modal_reset' in request.COOKIES:
        context["modal_reset"] = True
        context["form_reset"] = PasswordResetForm()
    return render(request,'proxy/index.html', context)


@login_required
def detail(request, count):
    ts = TimeSet.objects.first()
    now = timezone.now()
    try:
        offset = now - datetime.timedelta(minutes=ts.offset)
    except:
        offset = None
    if offset:
        proxy = Proxy.objects.filter(country=count,update__gte=offset)
    else:
        proxy = Proxy.objects.filter(country=count,checkers=True)
    total = proxy.count()
    paginator = Paginator(proxy, 25)
    page = request.GET.get('page')
    try:
        pr = paginator.page(page)
    except PageNotAnInteger:
        pr = paginator.page(1)
    except EmptyPage:
        pr = paginator.page(paginator.num_pages)
    return render(request,'proxy/country.html', {"proxy": proxy,"total":total})


@login_required
def proxylist(request):
    ts = TimeSet.objects.first()
    now = timezone.now()
    offset = now - datetime.timedelta(minutes=ts.offset)
    if offset:
        proxy = Proxy.objects.filter(country=count,update__gte=offset)
    else:
        proxy = Proxy.objects.filter(country=count,checkers=True)


@login_required
def ipdetail(request, id):
    obj = get_object_or_404(Proxy, id=id)
    return render(request, "proxy/detail.html", {'ip':obj})


@login_required
def socks_online(request):
    '''Проверка on-line'''
    if request.is_ajax():
        id = request.GET.get('id', None)
        try:
            obj = Proxy.objects.get(id=id)
        except Proxy.DoesNotExist:
            status = None
            return HttpResponse(status)

        res = _online.apply_async(args=(obj.ip, obj.port, obj.tp, obj.usr, obj.pswd),queue=obj.worker.ip)
        total = res.get()
        return HttpResponse(total)
    else:
        return HttpResponse(None)


#@login_required
def change_view(request):
    '''Проверка скорости'''
    if request.method == "GET":
        id = request.GET.get('id', None)
        try:
            obj = Proxy.objects.get(id=int(id))
        except Exception:
            return HttpResponse("error")
        types, ip, port, user, password = obj.tp, obj.ip, obj.port, obj.usr, obj.pswd
        worker = obj.worker.ip
        res = check_speed.apply_async(args=(types, ip, port, user, password),queue=worker)
        speed = res.get()
        if speed:
            obj = Proxy.objects.get(id=int(id))
            obj.speed = speed
            obj.save()
        else:
            return HttpResponse("check speed not", content_type='text/html')
        return HttpResponse(speed, content_type='text/html')
    else:
        return HttpResponse('no', content_type='text/html')


# class City(View):
#     ts = TimeSet.objects.first()
#     now = timezone.now()
#     try:
#         offset = now - datetime.timedelta(minutes=ts.offset)
#     except AttributeError:
#         offset = False
#
#     def get(self, request, *args, **kwargs):
#         if self.offset:
#             obj = Proxy.objects.filter(city=kwargs['city'],update__gte=self.offset)
#         else:
#             obj = Proxy.objects.filter(city=kwargs['city'],checkers=True)
#         return render(request, 'proxy/city.html',{'city':obj})

@login_required
def proxy_list(request):
    filter = ProxyFilter(request.GET, queryset=Proxy.objects.all())
    return render(request, 'proxy/template.html', {'filter': filter})


class ProxyList(ListView):
    model = Proxy
    #paginate_by = 12
    form_class = ProxyModelForm
    context_object_name = 'proxy'
    template_name = 'proxy/template.html'

@login_required
def product_list(request):
    f = ProductFilter(request.GET, queryset=Proxy.objects.filter(checkers=True))
    return render(request, 'proxy/template.html', {'filter': f})


@login_required
def ajax_form(request):
    #proxy = Proxy.objects.all()
    #country = CountryProxy.objects.all()
    return render(request, "proxy/search.html")#,{"proxys": proxy})


@login_required
def req_form(request):
    ts = TimeSet.objects.first()
    now = timezone.now()
    try:
        offset = now - datetime.timedelta(minutes=ts.offset)
    except AttributeError:
        offset = False

    if request.method == 'GET':
        form = ProxyModelForm(request.GET or None)
        if form.is_valid():
            bl = form.cleaned_data['blacklist']
            ip = form.cleaned_data['ip']
            country = form.cleaned_data['country']
            city = form.cleaned_data['city']
            continent = form.cleaned_data['continent']
            country_code = form.cleaned_data['country_code']
            postal_code = form.cleaned_data['postal_code']
            region_code = form.cleaned_data['region_code']
            time_zone = form.cleaned_data['time_zone']
            gmt = form.cleaned_data['gmt']

            qs = Q()
            if ip:
                ip = form.cleaned_data.pop('ip')
            #start country
            if country:
                country = [x.country for x in country]
                form.cleaned_data.pop('country')
            if bl:
                if country:
                    title = [x.title for x in bl]
                    form.cleaned_data.pop('blacklist')
                    [qs.add(Q(['%s' % k, v]),'AND') for k,v in form.cleaned_data.items() if v]
                    if offset:
                        coun = Proxy.objects.filter(qs, Q(blacklist__in=title) \
                            & Q(blacklist__icontains=title) & Q(update__gte=offset))
                    else:
                        if len(title) > 1:
                            q = Q()
                            [q.add(Q(['%s' % "blacklist__icontains", v]),'OR') for v in title]
                            coun = Proxy.objects.filter(qs,q,checkers=True,country__in=country)
                        else:
                            title = title[0]
                            coun = Proxy.objects.filter(
                                qs, checkers=True,country__in=country).filter(Q(
                                    blacklist__in=title) | Q(blacklist__icontains=title))
                else:
                    title = [x.title for x in bl]
                    form.cleaned_data.pop('blacklist')
                    [qs.add(Q(['%s' % k, v]),'AND') for k,v in form.cleaned_data.items() if v]
                    if offset:
                        coun = Proxy.objects.filter(qs, Q(blacklist__in=title) \
                            & Q(blacklist__icontains=title) & Q(update__gte=offset))
                    else:
                        if len(title) > 1:
                            q = Q()
                            [q.add(Q(['%s' % "blacklist__icontains", v]),'OR') for v in title]
                            coun = Proxy.objects.filter(qs,q,checkers=True)
                        else:
                            title = title[0]
                            coun = Proxy.objects.filter(
                                qs, checkers=True).filter(Q(
                                    blacklist__in=title) | Q(blacklist__icontains=title))
            else:
                if country:
                    [qs.add(Q(['%s' % k, v]),'AND') for k,v in form.cleaned_data.items() if v]
                    if offset:
                        if ip:
                            coun = Proxy.objects.filter(
                                qs,update__gte=offset,blacklist__isnull=True,ip__istartswith=ip)
                        else:
                            coun = Proxy.objects.filter(
                                qs,update__gte=offset,blacklist__isnull=True,country__in=country)
                    else:
                        if ip:
                            coun = Proxy.objects.filter(
                                qs,checkers=True,blacklist__isnull=True,ip__istartswith=ip)
                        else:
                            coun = Proxy.objects.filter(
                                qs,checkers=True,blacklist__isnull=True,country__in=country)
                else:
                    [qs.add(Q(['%s' % k, v]),'AND') for k,v in form.cleaned_data.items() if v]
                    if offset:
                        if ip:
                            coun = Proxy.objects.filter(
                                qs,update__gte=offset,blacklist__isnull=True,ip__istartswith=ip)
                        else:
                            coun = Proxy.objects.filter(
                                qs,update__gte=offset,blacklist__isnull=True)
                    else:
                        if ip:
                            coun = Proxy.objects.filter(
                                qs,checkers=True,blacklist__isnull=True,ip__istartswith=ip)
                        else:
                            coun = Proxy.objects.filter(
                                qs,checkers=True,blacklist__isnull=True)
            total = coun.count()
            paginator = Paginator(coun, 100)
            page = request.GET.get('page')
            GET_params = request.GET.copy()
            try:
                con = paginator.page(page)
            except PageNotAnInteger:
                con = paginator.page(1)
            except EmptyPage:
                con = paginator.page(paginator.num_pages)
            return render(request, "proxy/form.html",{
                "coun":con,"total":total,'GET_params':GET_params })
    else:
        form = ProxyModelForm()
    return render(request, "proxy/form.html",{"form":form})


def logout_view(request):
    logout(request)


@login_required
def service(request):
    users = User.objects.all()
    return render(request, 'proxy/service.html',{'users':users})


class Ip(DetailView):
    template_name = 'proxy/iplist.html'
    model = Iplist

    def get_context_data(self, **kwargs):
        context = super(Ip, self).get_context_data(**kwargs)
        ip = Iplist.objects.get(id=self.kwargs['pk'])
        count_max = ip.dicts[1]
        context['proxy'] = Proxy.objects.filter(ip.dicts[0])[:count_max]
        return context


def other(request):
    i = IplistAuth.objects.get(server='217.23.7.73')
    port = list(range(2025,3025))
    results = ProxyAuth.objects.filter(i.dicts[0])
    count = results.count()
    s = min(len(port),count)
    port = port[:count]
    return render(request, "proxy/other.html", {"proxy":port})
