#-*-coding:utf-8 -*-
from django import forms

from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string

from proxy.models import Proxy, CountryProxy, CityProxy
from vendor.models import Blacklist, Vendors


class ButtonWidget(forms.Widget):
	def __init__(self, *args, **kwargs):
		self.key_attrs = {}
		self.val_attrs = {}

		if "key_attrs" in kwargs:
			self.key_attrs = kwargs.pop("key_attrs")

		forms.Widget.__init__(self, *args, **kwargs)

	template_name = 'auth_button_widget.html'

	def render(self, name, value, attrs=None):
		context = {
			'url': '/',
			'ip': self.key_attrs['ip']
			}
		return mark_safe(render_to_string(self.template_name, context))



class BackForm(forms.Form):
	@property
	def foo(self):
		pass

	def backconnect_up(self, user_id, proxy_id):
		pass


class AdminProxyModelForm(forms.ModelForm):
	blacklist = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(),
		required=False,queryset=Blacklist.objects.all())

	class Meta:
		model = Proxy
		fields = (
			'vendor','blacklist','ip','country',
			'continent','country_code','postal_code',
			'region_code','time_zone','gmt')




class ProxyModelForm(forms.ModelForm):
	def as_div(self):
		return self._html_output(
			normal_row = '<div class="js-active forms__width %(html_class_attr)s" >\
				<div class="forms__i">%(label)s %(field)s\
				</div>%(help_text)s\
				<div class="forms__hint">%(errors)s</div></div>',
		error_row = '%s',
		row_ender = '',
		help_text_html = '<div class="forms__hint">%s</div>',
		errors_on_separate_row = False)

	forms.BaseForm.as_div = as_div

	def as_ul(self):
		"Returns this form rendered as HTML <li>s -- excluding the <ul></ul>."
		return self._html_output(
			normal_row='<li%(html_class_attr)s>%(errors)s%(label)s %(field)s%(help_text)s</li>',
			error_row='<li>%s</li>',
			row_ender='</li>',
			help_text_html=' <span class="helptext">%s</span>',
			errors_on_separate_row=False)

	black_check = forms.BooleanField(label="Игнорировать Блек",required=False)
	blacklist = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(),
		required=False,queryset=Blacklist.objects.all())

	###vendor = forms.ModelChoiceField(required=False, queryset=Vendors.objects.all())
	##vendor = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(),
	##	required=False,queryset=Vendors.objects.all())

	country = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(),
		required=False,queryset=CountryProxy.objects.all(),label="Страна")

	###city = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(),
	###	required=False,queryset=CityProxy.objects.all())

	class Meta:
		model = Proxy
		fields = (
			'vendor','blacklist','ip','country','city',
			'continent','country_code','postal_code',
			'region_code','time_zone','gmt')


class ProxySearchForm(forms.Form):
    country = forms.CharField(required=False)
    city = forms.CharField(required=False)

class FilterProxy(forms.Form):
	country = forms.CharField()#choise=country_list)
	city = forms.CharField()#choise=country_list)
	continent = forms.CharField()
