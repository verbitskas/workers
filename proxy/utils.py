#-*-coding:utf-8 -*-
import os
import time
import datetime
import logging

from django.utils import timezone
import dns.resolver
import requests
import psycopg2 as ps
from psycopg2.pool import ThreadedConnectionPool
from psycopg2.extensions import TransactionRollbackError

from vendor.models import Blacklist
from proxy.models import Proxy

logging.basicConfig(format = u'%(filename)s[STRING:%(lineno)d]# %(levelname)-8s \
        [%(asctime)s]  %(message)s',level = logging.DEBUG, filename = u'/home/john/proxy.log')

PASSDB = "rikitiki1984"
IPDATABASE = '185.235.245.10'
DATABASEPORT = 6432
DATABASEUSER = 'djwoms'
DATABASENAME = 'djproxy'
DATABASEPASSWORD = 'Djwoms18deuj_234567hfd'
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

def check_blacklist(ip, host):

    try:
        my_resolver = dns.resolver.Resolver()
        query = '.'.join(reversed(str(ip).split("."))) + "." + host
        answers = my_resolver.query(query, "A")
        answer_txt = my_resolver.query(query, "TXT")
        return host

    except dns.resolver.NXDOMAIN:
        return False


def check_speed(id):
    pr = Proxy.objects.get(id=int(id))
    proxies = {"http":"{}://{}:{}".format(pr.tp,pr.ip,pr.port)}
    try:
        start = time.time()
        #s = Рє СЃРµСЂРІРµСЂСѓ
        s = requests.get('http://46.4.65.22/media/gens-gs-2.16.7-6-x86_64.pkg.tar.xz',
            proxies=proxies,timeout=90)
        end = time.time()
        total = int(end - start)
        size = int(s.headers.get('content-length'))
        s1 = size//total
        kb = round(s1/1024)
        mb = round(s1/1024/1024)

        if mb:
            speed = "{} Mb/s".format(mb)
        else:
            speed = "{} Kb/s".format(kb)

    except Exception as exc:
        speed = None

    return speed


def black_save_old(dicts):
    conn = ps.connect(
        dbname=DATABASENAME,
        host=IPDATABASE,
        user=DATABASEUSER,
        password=DATABASEPASSWORD
        )

    with conn:
        with conn.cursor() as cur:
            val = [(v,k) for k,v in dicts.items()]
            sql = "UPDATE proxy_proxy SET blacklist=%s WHERE ip = %s"

            args_str = ','.join(cur.mogrify("(%s)", x) for x in val)
            cur.execute("INSERT INTO client_do (ip) VALUES " + args_str)

            cur.executemany(sql,val)


    conn.close()


def black_save(dicts):
    conn = ps.connect(
        dbname=DATABASENAME,
        host=IPDATABASE,
        user=DATABASEUSER,
        password=DATABASEPASSWORD
        )
    cur = conn.cursor()
    val = set([(v,k) for k,v in dicts.items()])
    #val = zip(dicts.keys(), dicts.values())
    #args_str = ','.join(cur.mogrify("(%s, %s)", x) for x in val)

    try:
        while True:
            try:
                t1 = time.time()
                cur.executemany("UPDATE proxy_proxy SET blacklist=(%s) WHERE ip = (%s)", val);
                print("{}".format(time.time() - t1))
                cur.close()
                break

            except TransactionRollbackError as exc:
                print("deadlock РїСЂРѕС€Р»Рѕ {} СЃРµРє, СЃ РјРѕРјРµРЅС‚Р° Р·Р°РїСѓСЃРєР°".format(time.time() - t1))
                conn.rollback()
                time.sleep(30)
                continue

    except KeyboardInterrupt:
        conn.close()

    finally:
        conn.commit()
        conn.close()

def del_db_orm():
    delta = timezone.now() - datetime.timedelta(minutes=60*24)
    objall = Proxy.objects.filter(
            checkers=False,created__lte=delta)
    while True:
        try:
            objall.delete()
            break
        except:
            time.sleep(60*2)
            continue
    objall = Proxy.objects.filter(checkers=False,created__isnull=True)

    while True:
        try:
            objall.delete()
            break
        except:
            time.sleep(60*2)
            continue



def del_db():
    conn = ps.connect(
        dbname=DATABASENAME,
        host=IPDATABASE,
        port=DATABASEPORT,
        user=DATABASEUSER,
        password=DATABASEPASSWORD
        )
    delta = timezone.now() - datetime.timedelta(minutes=60*24*3)
    s1 = 0
    s2 = 0

    with conn:
        with conn.cursor() as cur:

            msg = """
                    DELETE FROM proxy_proxy WHERE update <= '%s' AND id NOT IN
                    (SELECT socks_id FROM client_port)""" % delta
            while True:
                try:
                    cur.execute(msg)
                    s1 = cur.rowcount
                    break
                except TransactionRollbackError:
                    conn.rollback()
                    continue

            msg = "DELETE FROM proxy_proxy WHERE created <= '%s' AND update IS NULL;" % delta
            while True:
                try:
                    cur.execute(msg)
                    s2 = cur.rowcount
                    break

                except TransactionRollbackError:
                    conn.rollback()
                    continue

    conn.close()
    return (s1, s2)

def del_dubli():
    sql = """
        DELETE FROM proxy_proxy WHERE id NOT in (
        SELECT MIN(id) FROM proxy_proxy
        GROUP BY ipreal)
        """
    conn = ps.connect(
        dbname=DATABASENAME,
        host=IPDATABASE,
        user=DATABASEUSER,
        password=DATABASEPASSWORD
        )
    cur = conn.cursor()
    try:
        cur.execute(sql)
        conn.commit()
    except Exception as exc:
        pass
    finally:
        cur.close()
        conn.close()



def del_db_auth():
    conn = ps.connect(
        dbname=DATABASENAME,
        host=IPDATABASE,
        user=DATABASEUSER,
        password=DATABASEPASSWORD
        )

    with conn:
        with conn.cursor() as cur:

            while True:
                try:
                    cur.execute("DELETE FROM proxy_proxyauth WHERE checkers = %s" % False)
                    s1 = cur.rowcount
                    break

                except TransactionRollbackError:
                    conn.rollback()
                    continue

    conn.close()
    return s1

def country_list():
    count = list(set([x.country for x in Proxy.objects.all()]))
    try:
        count.remove('')
    except Exception:
        pass

    return count

def city_list():
    city = list(set([x.city for x in Proxy.objects.all()]))
    try:
        count.remove('')
    except Exception:
        pass

    return city
