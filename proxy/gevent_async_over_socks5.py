import logging
import os
import sys
import signal
import errno
import time
from functools import partial, wraps
from multiprocessing import Process
import resource

from struct import pack, unpack

import gevent
from gevent import socket
from gevent.server import StreamServer
from gevent.socket import create_connection, gethostbyname
from gevent.pool import Pool

import requests
import timeout_decorator

from daemonize import daemonize


sys.path.insert(0, os.path.dirname("../" + __file__))
sys.path.append('/home/john/proxyproject/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'

import django
django.setup()

from client.models import IplistBack
from proxy.models import Proxy


logging.basicConfig(
	filename="gevent-socks5.log",
	format="%(levelname)-10s строка %(lineno)d %(asctime)s %(message)s",
	level=logging.INFO
	)


pool = Pool(10000)


def chunks(lst, count):
    """Группировка элементов последовательности по count элементов"""
    start = 0
    for i in range(count):
        stop = start + len(lst[i::count])
        yield lst[start:stop]
        start = stop


def forward(read, write):
	try:
		while True:
			try:
				data = read.recv(8162)
				if not data:
					break
				write.sendall(data)
			except socket.error:
				break
	finally:
		pass


def handler(result, ipdos, portallow, loc, src, sock, address):
	destip, destport = result
	src_ip, src_port = src

	if ipdos:
		if address[0] not in ipdos:
			sock.close()
			return

	while True:
		while True:
			try:
				dest = create_connection((src_ip, int(src_port)), timeout=5)
				break
			except (socket.timeout, ConnectionRefusedError, OSError):
				gevent.sleep(0.1)
				continue

		while True:
			try:
				dest.sendall(b'\x05\x01\x00')
				break
			except (socket.timeout, ConnectionResetError, ConnectionRefusedError):
				gevent.sleep(0.1)
				continue

		while True:
			try:
				data = dest.recv(8162)
				break
			except (socket.timeout, ConnectionResetError, ConnectionRefusedError):
				gevent.sleep(0.1)
				continue

		if data.startswith(b'\x05\x00'):
			# тут конечный ip сокса
			data = b"\x05\x01\x00\x01" + socket.inet_aton(destip) + pack("!H", int(destport))
			while True:
				try:
					dest.sendall(data)
					break
				except (socket.timeout, ConnectionRefusedError, OSError):
					gevent.sleep(0.1)
					continue

			while True:
				try:
					data = dest.recv(8162)
					break
				except (socket.timeout, ConnectionRefusedError, OSError):
					gevent.sleep(0.1)
					continue

			################ проброс ###############
			if data.startswith(b'\x05\x00'):
				tasks = [
					gevent.spawn(forward, sock, dest),
					gevent.spawn(forward, dest, sock)
				]
				gevent.joinall(tasks)
				break

	sock.close()
	dest.close()


def start(loc_ports, results, ipdos, portallow, sock_over):
	servers = []
	handlers = [partial(
		handler, res, ipdos, portallow, loc, sock) for res,loc, sock in zip(results, loc_ports, sock_over)]

	for func, port in zip(handlers, loc_ports):
		try:
			server = StreamServer(("0.0.0.0", port), func, spawn=pool)
			server.start()
			servers.append(server)
		except OSError:
			logging.info("порт %s занят", port)
			continue

	logging.info("ждем")
	gevent.wait(timeout=60*30)
	logging.info("КОНЕЦ")

	for server in servers:
		server.stop()
	logging.info("STOP")
	


def req(s):
	#import requests
	#r = requests.get("http://46.4.65.22/priv8all.txt").text.split()
	#result = [(x.split(":")[0], x.split(":")[1]) for x in r]
	obj = Proxy.objects.filter(checkers=True, vendor_id=1)
	result = [(x.ip, x.port) for x in obj]
	logging.info(len(result))
	return result[:s]


def do_start_orm():
	try:
		i = IplistBack.objects.get(server__ip__in=['185.235.245.8'])
	except IplistBack.DoesNotExist:
		logging.info("список не обнаружен.......")
		return None, None, None, None, None

	except IplistBack.MultipleObjectsReturned:
		logging.exception("модель имеет 2 записи")
		return None, None, None, None, None

	port = eval(i.server_dicts['185.235.245.8'])

	ipdos = i.ip
	if i.portallow:
		portallow = i.portallow.split()

	else:
		portallow = None

	try:
		ipdos = ipdos.split(' ')
	except AttributeError as exc:
		ipdos = None

	s1 = i.dicts[-1]
	s = len(port)
	results = Proxy.objects.filter(i.dicts[0])[:s]
	count = results.count()

	ip_port = [(x.ip, int(x.port)) for x in results]
	port = port[:s]
	over_socks = req(s)
	return port, ip_port, ipdos, portallow, over_socks


def main():
	while True:
		locals_port, results, ipdos, portallow, over_socks = do_start_orm()
		try:
			if results:
				loc = chunks(locals_port, 4)
				rem = chunks(results, 4)
				sock_over = chunks(over_socks, 4)

				logging.info("[ --- start --- ]")
				
				target = [Process(target=start, args=(
					k,v, ipdos, portallow, sock)) for k,v, sock in zip(loc, rem, sock_over)]
				for proc in target:
					logging.info("proc start")
					proc.start()
				for proc in target:
					logging.info("proc join")
					proc.join()


				time.sleep(10)
			else:
				logging.info("спим 5")
				time.sleep(60*5)

		except Exception as exc:
			logging.info("ошибка")
			logging.exception(exc)
			continue


if __name__ == '__main__':
	daemonize(stdout='/tmp/portforward_https.log',stderr='/tmp/portforwarderror_https.log')
	try:
		resource.setrlimit(resource.RLIMIT_NOFILE, (70000, 100000))
	except Exception:
		pass
	main()