#-*-coding:utf-8 -*-
from django.apps import AppConfig


class ProxyConfig(AppConfig):
    name = 'proxy'
    verbose_name=u'Прокси'
