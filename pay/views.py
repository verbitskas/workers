#-*-coding:utf-8 -*-
import json
import random

from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.conf import settings

from celery.task.control import revoke

from client.models import Port

from pay.forms import PayForm
from pay.models import Order
from prof.models import Notes, User
from proxy.models import Proxy

from pay.tasks import backforward, _portopen

from redis import Redis
from pay._rev import IP2Location

obj = IP2Location.IP2Location()
obj.open("pay/geo/geo.bin");

#@login_required
def info(request):
    dicts = []
    if request.is_ajax():
        pk = request.GET.get('pk', None)
        if pk:
            try:
                d = get_object_or_404(Proxy, pk=pk)
            except Proxy.DoestNotExist:
                return HttpResponse(json.dumps({'error':'error'}))

            dicts = [
                d.hidden_ip,
                d.country,
                d.region_code,
                d.city,
                d.vendor.get_short_name,
                d.postal_code,
                d.gmt,
                d.blacklist,
                #d.black_split,
                ## dns
                ## open port
                d.created.day,
                d.created.month,
                d.created.hour,
                d.created.minute,
                d.created.second
                ]
            return HttpResponse(json.dumps(dicts))
        else:
            return HttpResponse("no ip", content_type="text/html")
    else:
        return HttpResponse("no ajax", content_type="text/html")


@login_required
def returns(request, id):
    obj = Port.objects.get(id=int(id))
    ip, port = obj.ipreal.split(":")
    sock = Proxy.objects.get(ipreal=ip, port=port)
    order = Order.objects.get(socks=sock,user=request.user)
    revoke(obj.task, terminate=True)
    order.delete()
    obj.delete()
    return HttpResponse(json.dumps({"status":ip}))

@login_required
def buy(request, id):
    form = PayForm()

    if request.POST:
        form = PayForm(request.POST or None)
        if form.is_valid():
            user = request.user
            term = form.cleaned_data.get('term')
            socks = Proxy.objects.get(id=id)
            order = Order(
                    amount=100,
                    term=term,
                    socks=socks,
                )
            order.user = user
            order.save()
            backforward(id, request)
            return HttpResponseRedirect(reverse('prof:profile'))

        else:
            return HttpResponse("err")

    return render(request, 'pay/balance.html', {'form': form})


@login_required
def dns(request, id):
    if request.is_ajax():
        try:
            pr = Proxy.objects.get(id=id)
        except Proxy.DoestNotExist:
            return HttpResponse(json.dumps("err"))
        ip = pr.ipreal
        rec = obj.get_all(ip)
        country = rec.country_long
        city = rec.city
        red = Redis(host=settings.RED_SERVER, password=settings.RED_PASS)
        iplist,citylist = eval(red.get(country))
        ip_con = [a for a,b in zip(iplist,citylist) if b == city]
        if ip_con:
            _dns = random.choice(ip_con)
        else:
            _dns = random.choice(iplist)
    else:
        _dns = None

    return HttpResponse(_dns)

@login_required
def ajax_dns(request):
    '''Ajax проверка DNS'''
    if request.method == 'GET':
        id = request.GET.get('id', None)
        try:
            pr = Proxy.objects.get(id=id)
        except Proxy.DoestNotExist:
            return HttpResponse(json.dumps("err"))
        ip = pr.ipreal
        rec = obj.get_all(ip)
        country = rec.country_long
        city = rec.city
        red = Redis(host=settings.RED_SERVER, password=settings.RED_PASS)
        iplist,citylist = eval(red.get(country))
        ip_con = [a for a,b in zip(iplist,citylist) if b == city]
        if ip_con:
            _dns = random.choice(ip_con)
        else:
            _dns = random.choice(iplist)
    else:
        _dns = None

    return HttpResponse(_dns)

@login_required
def open_port(request):
    '''Проверка открытых портов'''
    if request.method == 'GET':
        id = request.GET.get('id', None)
        try:
            sock = Proxy.objects.get(id=id)
        except Exception:
            return HttpResponse(json.dumps({0:"error"}))
        ip = sock.ip
        worker = sock.worker.ip
        res = _portopen.apply_async(args=(ip,),queue=worker)
        port = res.get()
        if port:
            return HttpResponse(json.dumps(port))

    return HttpResponse(json.dumps({0:"error"}))
