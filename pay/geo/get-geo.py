import os
import shutil
import math

import requests

from tqdm import tqdm


import mmap

def get_num_lines(file_path):
    fp = open(file_path, "r+")
    buf = mmap.mmap(fp.fileno(), 0)
    lines = 0
    while buf.readline():
        lines += 1
    return lines


def download(urls):
	for url in urls:
		while True:
			req = requests.get(url, stream=True)
			total_size = int(req.headers.get('content-length', 0))
			block_size = 1024
			write_len = 0
			print("размер файла: %s Mb" % math.ceil(total_size//block_size//block_size))

			name = os.path.basename(url)
			name = name + ".mmdb"

			with open(name, "wb") as f:
				for data in tqdm(req.iter_content(block_size), total=math.ceil(total_size//block_size) , unit='KB', unit_scale=True):
					write_len += len(data)
					f.write(data)

			if total_size != 0 and write_len != total_size:
				print("ОШИБКА, Фаил не полный:")
				print("Повторная загрузка ......")
				continue
			else:
				break
		#with open(name, "wb") as f:
		#	shutil.copyfileobj(req.raw, f)
		#del req


urls = [
	"https://ipcheck.pro/api/v1/getGeoDB/city",
	"https://ipcheck.pro/api/v1/getGeoDB/country",
	"https://ipcheck.pro/api/v1/getGeoDB/domain",
	"https://ipcheck.pro/api/v1/getGeoDB/isp"
	]


def main():
	path = 'maxminddb'
	if not os.path.exists(path):
		os.mkdir(path)

	os.chdir(path)
	download(urls)


if __name__ == '__main__':
	main()
