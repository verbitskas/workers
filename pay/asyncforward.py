import asyncio
import base64
import socket
import logging
import re
import os
import sys
from struct import unpack, pack, error as struct_err

import aiodns
import aioredis
import aioping
from async_timeout import timeout

import maxminddb

from django.conf import settings


_city    = maxminddb.open_database('geo/maxminddb/city.mmdb', mode=maxminddb.MODE_MEMORY)
_isp     = maxminddb.open_database('geo/maxminddb/isp.mmdb', mode=maxminddb.MODE_MEMORY)


sys.path.insert(0, os.path.dirname("." + __file__))
sys.path.append('/home/john/proxyproject')
os.environ['DJANGO_SETTINGS_MODULE'] = 'proxyproject.settings'


logging.basicConfig(
    filename='asyncforward.log',
    format="%(levelname)-10s %(lineno)d %(asctime)s %(lineno)s %(message)s",
    level=logging.INFO
)

logging.info(settings.RED_PASS)

def ping(hosts):
	result = []
	for host in hosts:
		res = os.system("ping -c 3 -q -W 1 {host}".format(host=host))
		if res == 0:
			result.append(host)
		if len(result) == 3:
			break
	return result



class AsyncSocks5:
	def __init__(self, source, dest, user, password, dns=None):
		self.loop = None
		self.server = None
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
		self.source_ip = source[0]
		self.source_port = source[1]
		self._dest_ip = dest[0]
		self._dest_port = dest[1]
		self.ulen = len(user)
		self.plen = len(password)
		self.user = user
		self.password = password
		self._dns = dns 
		self._BUFF = 65535
		self._OFF = b'\x05\xff'
		self._SUCESS = b'\x05\x00'
		self.resolver = None
		self._geo = _city
		self._isp = _isp
		self.country = self._geo.get(self._dest_ip).get('country').get('names').get('en')
		self.isp = self._isp.get(self._dest_ip).get('isp')
		
		logging.info(self.country)
		logging.info(self._dest_ip)

	

	async def _red_dns(self):
		conn = await aioredis.create_connection(
			'redis://:{}@{}:{}/0'.format(settings.RED_PASS, settings.RED_SERVER, settings.RED_PORT))
		val = await conn.execute('get', self.country)
		conn.close()
		await conn.wait_closed()
		try:
			try:
				iplist = eval(val)
			except TypeError:
				logging.info('TypeError', val)
				dns = ['8.8.8.8', '8.8.4.4']
				return
			dns_isp = [ip for ip in iplist if self._isp.get(ip).get('isp') == self.isp]
			
			if dns_isp:
				logging.info("ISP %s", self.isp)
				hosts = [x for x in dns_isp if x.split('.')[-1] != '0' and x.split('.')[-1] != '255']
				dns = hosts[:3]
				logging.info("DNS %s", dns)
			else:
				logging.info("у ip адреса %s нет ISP провайдера в базе", self._dest_ip)
				hosts = [x for x in iplist if x.split('.')[-1] != '0' and x.split('.')[-1] != '255']
				dns = hosts[:3]
		except Exception as ex:
			logging.exception(ex)
			logging.info("используется google DNS")
			logging.exception(ex)
			dns = ['8.8.8.8', '8.8.4.4']
		if not self._dns:
			logging.info("DNS из базы %s", dns)
			self.resolver = aiodns.DNSResolver()
			self.resolver.nameservers = dns
			self._dns = dns
		else:
			if isinstance(self._dns, (list, tuple)):
				logging.info("DNS смена")
				self.resolver = aiodns.DNSResolver(self._dns)
			else:
				logging.info("замена DNS")
				self.resolver = aiodns.DNSResolver([self._dns])
		logging.info(self.resolver.nameservers)



	async def forward(self, reader, writer):
		try:
			while True:
				try:
					data = await reader.read(65535)
					if not data:
						break
					writer.write(data)
				except (socket.error, ConnectionResetError):
					break
		finally:
			writer.close()


	async def _async_dns_(self, domain):
		try:
			_ = await self.resolver.query(domain, 'A')
			ip = _[0].host
			return ip
		except (aiodns.error.DNSError, IndexError):
			return


	async def _auth_check(self, data):
		try:
			user_len = data[1:2]
			user_len = unpack(">b", user_len)[0]
			user = data[2:2+user_len]
			password_len = data[2+user_len:3+user_len]
			password_len = unpack(">b", password_len)[0]
			password = data[3+user_len:3+user_len+password_len]
			if user.decode() == self.user and password.decode() == self.password:
				return True
			else:
				logging.info("ршибка %s", (user, password, self.user, self.password))
		except struct_err:
			logging.info("ошибка авторизации")


	async def go(self, reader, writer):
		data_src = await reader.read(self._BUFF)
		writer.write(b'\x05\x02')
		data_src = await reader.read(self._BUFF)
		auth = await self._auth_check(data_src)
		if auth:
			writer.write(self._SUCESS)
			# socks to
			try:
				dest_reader, dest_writer = await asyncio.open_connection(self._dest_ip, self._dest_port)
			except (ConnectionResetError, ConnectionRefusedError, OSError):
				writer.close()
				return
			dest_writer.write(b"\x05\x01\x00")
			try:
				data = await dest_reader.read(self._BUFF)
			except ConnectionResetError:
				dest_writer.close()
				return
			data = await reader.read(self._BUFF)
			if data.startswith(b"\x05\x01\x00\x01"):
				dest_writer.write(data)
				asyncio.ensure_future(self.forward(reader, dest_writer))
				asyncio.ensure_future(self.forward(dest_reader, writer))
			elif data.startswith(b"\x05\x01\x00\x03"):
				domain = data[5:5+data[4]].decode()
				try:
					ip = self.pattern.findall(domain)[0]
				except IndexError:
					ip = None
				if not ip:
					ip = await self._async_dns_(domain)
				if not ip:
					writer.close()
					return
				ip = socket.inet_aton(ip)
				data = data[:3] + b"\x01" + ip + data[-2:]
				dest_writer.write(data)
				data_dest = await dest_reader.read(self._BUFF)
				if data_dest.startswith(b'\x05\x00\x00\x01'):
					writer.write(data_dest)
					asyncio.ensure_future(self.forward(reader, dest_writer))
					asyncio.ensure_future(self.forward(dest_reader, writer))
		else:
			logging.info("GOOD By")
			writer.write(self._OFF)
			writer.close()


	def stop(self):
		logging.info("остановка сервера")
		self.server.close()
		self.loop.stop()


	def run(self):
		self.loop = asyncio.new_event_loop()
		asyncio.set_event_loop(self.loop)

		self.loop.run_until_complete(self._red_dns())
		task = asyncio.start_server(self.go, self.source_ip, self.source_port, loop=self.loop)
		self.server = self.loop.run_until_complete(task)
		try:
			self.loop.run_forever()
		except KeyboardInterrupt:
			pass
		finally:
			self.stop()




class AsyncSocks4:
	def __init__(self, source, dest, user, password, dns=None):
		logging.info(dest)
		self.loop = None
		self.server = None
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
		self.source_ip = source[0]
		self.source_port = source[1]
		self._dest_ip = dest[0]
		self._dest_port = dest[1]
		self.ulen = len(user)
		self.plen = len(password)
		self.user = user
		self.password = password
		self._BUFF = 65535
		self._SUCESS = b'\x05\x00'
		self._OFF = b'\x05\xff'
		self._dns = dns 
		self.resolver = None
		
		#self.geo = obj.get_all(dest[0])
		#self._isp = self.geo.isp
		#self.country = self.geo.country_long.decode("utf-8")
		#logging.info(self.country)
		#logging.info(self._dest_ip)

		#self.geo = obj.get_all(dest[0])
		#self._isp = self.geo.isp
		#self.country = self.geo.country_long.decode("utf-8")

		self._geo = _city
		self._isp = _isp

		self.country = self._geo.get(self._dest_ip).get('country').get('names').get('en')
		self.isp = self._isp.get(self._dest_ip).get('isp')
		
		logging.info(self.country)
		logging.info(self._dest_ip)



	async def _red_dns(self):
		conn = await aioredis.create_connection(
			'redis://:{}@{}:{}/0'.format(settings.RED_PASS, settings.RED_SERVER, settings.RED_PORT))
		val = await conn.execute('get', self.country)
		conn.close()
		await conn.wait_closed()
		try:
			try:
				iplist = eval(val)
			except TypeError as exc:
				logging.exception(exc)
				dns = ['8.8.8.8', '8.8.4.4']
				return
			dns_isp = [ip for ip in iplist if self._isp.get(ip).get('isp') == self.isp]
			
			if dns_isp:
				logging.info("ISP %s", self.isp)
				hosts = [x for x in dns_isp if x.split('.')[-1] != '0' and x.split('.')[-1] != '255']
				dns = hosts[:3]
				logging.info("DNS %s", dns)
			else:
				logging.info("у ip адреса %s нет ISP провайдера в базе", self._dest_ip)
				hosts = [x for x in iplist if x.split('.')[-1] != '0' and x.split('.')[-1] != '255']
				dns = hosts[:3]
		except Exception as ex:
			logging.exception(ex)
			logging.info("используется google DNS")
			logging.exception(ex)
			dns = ['8.8.8.8', '8.8.4.4']
		if not self._dns:
			logging.info("DNS из базы")
			self.resolver = aiodns.DNSResolver()
			self.resolver.nameservers = dns
			self._dns = dns
		else:
			if isinstance(self._dns, (list, tuple)):
				logging.info("DNS смена")
				self.resolver = aiodns.DNSResolver(self._dns)
			else:
				logging.info("замена DNS")
				self.resolver = aiodns.DNSResolver([self._dns])
		logging.info(self.resolver.nameservers)


	async def forward(self, reader, writer):
		try:
			while True:
				try:
					data = await reader.read(65535)
					if not data:
						break
					writer.write(data)
				except (socket.error, ConnectionResetError):
					break
		finally:
			writer.close()


	async def _async_dns_(self, domain):
		try:
			_ = await self.resolver.query(domain, 'A')
			ip = _[0].host
			return ip
		except (aiodns.error.DNSError, IndexError):
			return


	async def _auth_check(self, data):
		try:
			user_len = data[1:2]
			user_len = unpack(">b", user_len)[0]
			user = data[2:2+user_len]
			password_len = data[2+user_len:3+user_len]
			password_len = unpack(">b", password_len)[0]
			password = data[3+user_len:3+user_len+password_len]
			if user.decode() == self.user and password.decode() == self.password:
				return True
		except struct_err:
			logging.info("ошибка авторизации")


	async def _build_host_to_ip(self, data):
		domain = data[5:5+data[4]].decode()
		try:
			ip = self.pattern.findall(domain)[0]
		except IndexError:
			ip = None
		if not ip:
			ip = await self._async_dns_(domain)
		port = unpack("!H",data[-2:])[0]
		try:
			sock4 = b'\x04\x01' + pack("!H", port) + socket.inet_aton(ip) + b'' + b'\x00'
		except Exception as exc:
			return
		return sock4



	async def _build_socks4(self, data):
		ip = socket.inet_ntoa(data[4:-2])
		port = unpack("!H",data[-2:])[0]
		sock4 = b'\x04\x01' + pack("!H", port) + socket.inet_aton(ip) + b'' + b'\x00'
		return sock4


	async def _build_socks5(self):
		return b'\x05\x00\x00\x01' + socket.inet_aton(self.source_ip) + pack("!H", self.source_port)


	async def go(self, reader, writer):
		data_src = await reader.read(self._BUFF)
		writer.write(b'\x05\x02')
		data_src = await reader.read(self._BUFF)
		auth = await self._auth_check(data_src)
		if auth:
			writer.write(self._SUCESS)
			try:
				dest_reader, dest_writer = await asyncio.open_connection(self._dest_ip, self._dest_port)
			except (ConnectionResetError, ConnectionRefusedError, OSError):
				writer.close()
				return
			data_src = await reader.read(self._BUFF)
			if data_src.startswith(b"\x05\x01\x00\x01"):
				sk5 = await self._build_socks5()
				writer.write(sk5)
				sk4 = await self._build_socks4(data_src)
				dest_writer.write(sk4)
				data_dest = await dest_reader.read(self._BUFF)
				if data_dest.startswith(b'\x00'):
					asyncio.ensure_future(self.forward(reader, dest_writer))
					asyncio.ensure_future(self.forward(dest_reader, writer))
			elif data_src.startswith(b"\x05\x01\x00\x03"):
				# HOST
				sk5 = await self._build_socks5()
				writer.write(sk5)
				domain = data_src[5:5+data_src[4]].decode()
				try:
					ip = self.pattern.findall(domain)[0]
				except IndexError:
					ip = None
				if not ip:
					ip = await self._async_dns_(domain)
				if not ip:
					writer.close()
					return
				sk4 = await self._build_host_to_ip(data_src)
				if not sk4:
					dest_writer.close()
					writer.close()
					return
				dest_writer.write(sk4)
				data_dest = await dest_reader.read(self._BUFF)
				if data_dest.startswith(b'\x00'):
					asyncio.ensure_future(self.forward(reader, dest_writer))
					asyncio.ensure_future(self.forward(dest_reader, writer))
		else:
			logging.info("GOOD By")
			writer.write(self._OFF)
			await writer.drain()
			writer.close()


	def stop(self):
		logging.info("остановка сервера")
		self.server.close()
		self.loop.stop()


	def run(self):
		self.loop = asyncio.new_event_loop()
		asyncio.set_event_loop(self.loop)

		self.loop.run_until_complete(self._red_dns())
		task = asyncio.start_server(self.go, self.source_ip, self.source_port, loop=self.loop)
		self.server = self.loop.run_until_complete(task)
		try:
			self.loop.run_forever()
		except KeyboardInterrupt:
			pass
		finally:
			self.stop()



class AsyncHttpAuth:
	def __init__(self, source, dest, user, password, userhttp, passwordhttp, dns=None):
		self.loop = None
		self.server = None
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
		self.source_ip = source[0]
		self.source_port = source[1]
		self._dest_ip = dest[0]
		self._dest_port = dest[1]
		self.ulen = len(user)
		self.plen = len(password)
		self.user = user
		self.password = password
		self.userhttp = userhttp
		self.passhttp = passwordhttp
		user_pass = base64.encodestring(userhttp.encode() + b':' + passwordhttp.encode())
		self.proxy_authorization = b'Proxy-authorization: Basic ' + user_pass.strip()
		self._BUFF = 65535
		self._SUCESS = b'\x05\x00'
		self._OFF = b'\x05\xff'
		self._dns = dns
		self.CRLF = b'\r\n'
		self.resolver = None
		
		#self.geo = obj.get_all(dest[0])
		#self._isp = self.geo.isp
		#self.country = self.geo.country_long.decode("utf-8")
		#logging.info(self.country)
		#logging.info(self._dest_ip)

		#self.geo = obj.get_all(dest[0])
		#self._isp = self.geo.isp
		#self.country = self.geo.country_long.decode("utf-8")

		self._geo = _city
		self._isp = _isp
		self.country = self._geo.get(self._dest_ip).get('country').get('names').get('en')
		self.isp = self._isp.get(self._dest_ip).get('isp')
		
		logging.info(self.country)
		logging.info(self._dest_ip)


	async def _red_dns(self):
		conn = await aioredis.create_connection(
			'redis://:{}@{}:{}/0'.format(settings.RED_PASS, settings.RED_SERVER, settings.RED_PORT))
		val = await conn.execute('get', self.country)
		conn.close()
		await conn.wait_closed()
		try:
			try:
				iplist = eval(val)
			except TypeError:
				logging.info('TypeError', val)
				dns = ['8.8.8.8', '8.8.4.4']
				return
			dns_isp = [ip for ip in iplist if self._isp.get(ip).get('isp') == self.isp]
			
			if dns_isp:
				logging.info("ISP %s", self.isp)
				hosts = [x for x in dns_isp if x.split('.')[-1] != '0' and x.split('.')[-1] != '255']
				dns = hosts[:3]
				logging.info("DNS %s", dns)
			else:
				logging.info("у ip адреса %s нет ISP провайдера в базе", self._dest_ip)
				hosts = [x for x in iplist if x.split('.')[-1] != '0' and x.split('.')[-1] != '255']
				dns = hosts[:3]
		except Exception as ex:
			logging.exception(ex)
			logging.info("используется google DNS")
			logging.exception(ex)
			dns = ['8.8.8.8', '8.8.4.4']
		if not self._dns:
			logging.info("DNS из базы")
			self.resolver = aiodns.DNSResolver()
			self.resolver.nameservers = dns
			self._dns = dns
		else:
			if isinstance(self._dns, (list, tuple)):
				logging.info("DNS смена")
				self.resolver = aiodns.DNSResolver(self._dns)
			else:
				logging.info("замена DNS")
				self.resolver = aiodns.DNSResolver([self._dns])
		logging.info(self.resolver.nameservers)


	async def forward(self, reader, writer):
		try:
			while True:
				try:
					data = await reader.read(65535)
					if not data:
						break
					writer.write(data)
				except (socket.error, ConnectionResetError):
					break
		finally:
			writer.close()


	async def _async_dns_(self, domain):
		try:
			_ = await self.resolver.query(domain, 'A')
			ip = _[0].host
			return ip
		except (aiodns.error.DNSError, IndexError):
			logging.info("Ошибка DNS %s", domain)
			return


	async def _auth_check(self, data):
		try:
			user_len = data[1:2]
			user_len = unpack(">b", user_len)[0]
			user = data[2:2+user_len]
			password_len = data[2+user_len:3+user_len]
			password_len = unpack(">b", password_len)[0]
			password = data[3+user_len:3+user_len+password_len]
			if user.decode() == self.user and password.decode() == self.password:
				return True
		except struct_err:
			logging.info("ошибка авторизации")


	async def go(self, reader, writer):
		data_src = await reader.read(self._BUFF)
		writer.write(b'\x05\x02')
		data_src = await reader.read(self._BUFF)
		auth = await self._auth_check(data_src)
		if auth:
			writer.write(self._SUCESS)
			data_src = await reader.read(self._BUFF)
			if data_src.startswith(b"\x05\x01\x00\x01"):
				domain = ".".join([str(x) for x in unpack('>BBBB', data_src[4:-2])])
				port = str(unpack('>H', data_src[-2:])[0])
				header = "CONNECT {host}:{port} HTTP/1.1\r\n\
				          Host: {host}:{port}\r\n{auth}\r\n\
				          Proxy-Connection: Keep-Alive\r\n\r\n""".format(
				          	host=domain, port=port, auth=self.proxy_authorization)
				try:
					dest_reader, dest_writer = await asyncio.open_connection(self._dest_ip, self._dest_port)
				except (ConnectionResetError, ConnectionRefusedError, OSError):
					writer.close()
					return
				dest_writer.write(header.encode('latin1'))
				data_dest = await dest_reader.read(self._BUFF)
				if data_dest.startswith(b'HTTP/1.1 200') or data_dest.startswith(b'HTTP/1.0 200'):
					data_src = b"\x05\x00\x00\x01" + socket.inet_aton(self.source_ip) + pack("!H", self.source_port)
					writer.write(data_src)
					asyncio.ensure_future(self.forward(reader, dest_writer))
					asyncio.ensure_future(self.forward(dest_reader, writer))
			elif data_src.startswith(b"\x05\x01\x00\x03"):
				domain = data_src[5:5+data_src[4]].decode()
				try:
					ip = self.pattern.findall(domain)[0]
				except IndexError:
					ip = None
				if not ip:
					ip = await self._async_dns_(domain)
				if not ip:
					writer.close()
					return
				port = str(unpack('>H', data_src[-2:])[0])
				header = "CONNECT {host}:{port} HTTP/1.1\r\n\
				          Host: {host}:{port}\r\n{auth}\r\n\
				          Proxy-Connection: Keep-Alive\r\n\r\n""".format(
				          	host=domain, port=port, auth=self.proxy_authorization)
				try:
					dest_reader, dest_writer = await asyncio.open_connection(self._dest_ip, self._dest_port)
				except (ConnectionResetError, ConnectionRefusedError, OSError):
					writer.close()
					return
				dest_writer.write(header.encode('latin1'))
				try:
					data_dest = await dest_reader.read(self._BUFF)
				except (ConnectionResetError, ConnectionRefusedError, OSError):
					writer.close()
					return
				if data_dest.startswith(b'HTTP/1.1 200') or data_dest.startswith(b'HTTP/1.0 200'):
					data_src = b"\x05\x00\x00\x01" + socket.inet_aton(self.source_ip) + pack("!H", self.source_port)
					writer.write(data_src)
					asyncio.ensure_future(self.forward(reader, dest_writer))
					asyncio.ensure_future(self.forward(dest_reader, writer))
		else:
			logging.info("GOOD By")
			writer.write(self._OFF)
			await writer.drain()
			writer.close()


	def stop(self):
		logging.info("остановка сервера")
		self.server.close()
		self.loop.stop()


	def run(self):
		self.loop = asyncio.new_event_loop()
		asyncio.set_event_loop(self.loop)

		self.loop.run_until_complete(self._red_dns())
		task = asyncio.start_server(self.go, self.source_ip, self.source_port, loop=self.loop)
		self.server = self.loop.run_until_complete(task)
		try:
			self.loop.run_forever()
		except KeyboardInterrupt:
			pass
		finally:
			self.stop()



class AsyncHttp:
	def __init__(self, source, dest, user, password, dns=None):
		self.loop = None
		self.server = None
		self.pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
		self.source_ip = source[0]
		self.source_port = source[1]
		self._dest_ip = dest[0]
		self._dest_port = dest[1]
		self.ulen = len(user)
		self.plen = len(password)
		self.user = user
		self.password = password
		self._BUFF = 65535
		self._SUCESS = b'\x05\x00'
		self._OFF = b'\x05\xff'
		self._dns = dns
		self.CRLF = b'\r\n'
		self.resolver = None
		
		#self.geo = obj.get_all(dest[0])
		#self._isp = self.geo.isp
		#self.country = self.geo.country_long.decode("utf-8")
		#logging.info(self.country)
		#logging.info(self._dest_ip)

		#self.geo = obj.get_all(dest[0])
		#self._isp = self.geo.isp
		#self.country = self.geo.country_long.decode("utf-8")

		self._geo = _city
		self._isp = _isp
		self.country = self._geo.get(self._dest_ip).get('country').get('names').get('en')
		self.isp = self._isp.get(self._dest_ip).get('isp')
		
		logging.info(self.country)
		logging.info(self._dest_ip)


	async def _red_dns(self):
		conn = await aioredis.create_connection(
			'redis://:{}@{}:{}/0'.format(settings.RED_PASS, settings.RED_SERVER, settings.RED_PORT))
		val = await conn.execute('get', self.country)
		conn.close()
		await conn.wait_closed()
		try:
			try:
				iplist = eval(val)
			except TypeError:
				logging.info('TypeError', val)
				dns = ['8.8.8.8', '8.8.4.4']
				return
			dns_isp = [ip for ip in iplist if self._isp.get(ip).get('isp') == self.isp]
			
			if dns_isp:
				logging.info("ISP %s", self.isp)
				hosts = [x for x in dns_isp if x.split('.')[-1] != '0' and x.split('.')[-1] != '255']
				dns = hosts[:3]
				logging.info("DNS %s", dns)
			else:
				logging.info("у ip адреса %s нет ISP провайдера в базе", self._dest_ip)
				hosts = [x for x in iplist if x.split('.')[-1] != '0' and x.split('.')[-1] != '255']
				dns = hosts[:3]
		except Exception as ex:
			logging.exception(ex)
			logging.info("используется google DNS")
			logging.exception(ex)
			dns = ['8.8.8.8', '8.8.4.4']
		if not self._dns:
			logging.info("DNS из базы")
			self.resolver = aiodns.DNSResolver()
			self.resolver.nameservers = dns
			self._dns = dns
		else:
			if isinstance(self._dns, (list, tuple)):
				logging.info("DNS смена")
				self.resolver = aiodns.DNSResolver(self._dns)
			else:
				logging.info("замена DNS")
				self.resolver = aiodns.DNSResolver([self._dns])
		logging.info(self.resolver.nameservers)


	async def forward(self, reader, writer):
		try:
			while True:
				try:
					data = await reader.read(65535)
					if not data:
						break
					writer.write(data)
				except (socket.error, ConnectionResetError):
					break
		finally:
			writer.close()


	async def _async_dns_(self, domain):
		try:
			_ = await self.resolver.query(domain, 'A')
			ip = _[0].host
			return ip
		except (aiodns.error.DNSError, IndexError):
			logging.info("Ошибка DNS %s", domain)
			return


	async def _auth_check(self, data):
		try:
			user_len = data[1:2]
			user_len = unpack(">b", user_len)[0]
			user = data[2:2+user_len]
			password_len = data[2+user_len:3+user_len]
			password_len = unpack(">b", password_len)[0]
			password = data[3+user_len:3+user_len+password_len]
			if user.decode() == self.user and password.decode() == self.password:
				return True
		except struct_err:
			logging.info("ошибка авторизации")


	async def go(self, reader, writer):
		data_src = await reader.read(self._BUFF)
		writer.write(b'\x05\x02')
		data_src = await reader.read(self._BUFF)
		auth = await self._auth_check(data_src)
		if auth:
			writer.write(self._SUCESS)
			data_src = await reader.read(self._BUFF)
			if data_src.startswith(b"\x05\x01\x00\x01"):
				domain = ".".join([str(x) for x in unpack('>BBBB', data_src[4:-2])])
				port = str(unpack('>H', data_src[-2:])[0])
				
				header = "CONNECT {host}:{port} HTTP/1.1\r\n \
				          Host: {host}:{port}\r\n \
				          Proxy-Connection: Keep-Alive\r\n\r\n".format(host=domain, port=port)

				try:
					dest_reader, dest_writer = await asyncio.open_connection(self._dest_ip, self._dest_port)
				except (ConnectionResetError, ConnectionRefusedError, OSError):
					writer.close()
					return
				dest_writer.write(header.encode('latin1'))
				data_dest = await dest_reader.read(self._BUFF)
				if data_dest.startswith(b'HTTP/1.1 200') or data_dest.startswith(b'HTTP/1.0 200'):
					data_src = b"\x05\x00\x00\x01" + socket.inet_aton(self.source_ip) + pack("!H", self.source_port)
					writer.write(data_src)
					asyncio.ensure_future(self.forward(reader, dest_writer))
					asyncio.ensure_future(self.forward(dest_reader, writer))
			elif data_src.startswith(b"\x05\x01\x00\x03"):
				domain = data_src[5:5+data_src[4]].decode()
				try:
					ip = self.pattern.findall(domain)[0]
				except IndexError:
					ip = None
				if not ip:
					ip = await self._async_dns_(domain)
				if not ip:
					writer.close()
					return
				port = str(unpack('>H', data_src[-2:])[0])
				
				header = "CONNECT {host}:{port} HTTP/1.1\r\n \
				          Host: {host}:{port}\r\n \
				          Proxy-Connection: Keep-Alive\r\n\r\n".format(host=domain, port=port)
				
				try:
					dest_reader, dest_writer = await asyncio.open_connection(self._dest_ip, self._dest_port)
				except (ConnectionResetError, ConnectionRefusedError, OSError):
					writer.close()
					return
				dest_writer.write(header.encode('latin1'))
				try:
					data_dest = await dest_reader.read(self._BUFF)
				except (ConnectionResetError, ConnectionRefusedError, OSError):
					writer.close()
					return
				if data_dest.startswith(b'HTTP/1.1 200') or data_dest.startswith(b'HTTP/1.0 200'):
					data_src = b"\x05\x00\x00\x01" + socket.inet_aton(self.source_ip) + pack("!H", self.source_port)
					writer.write(data_src)
					asyncio.ensure_future(self.forward(reader, dest_writer))
					asyncio.ensure_future(self.forward(dest_reader, writer))
		else:
			logging.info("GOOD By")
			writer.write(self._OFF)
			await writer.drain()
			writer.close()


	def stop(self):
		logging.info("остановка сервера")
		self.server.close()
		self.loop.stop()


	def run(self):
		self.loop = asyncio.new_event_loop()
		asyncio.set_event_loop(self.loop)

		self.loop.run_until_complete(self._red_dns())
		task = asyncio.start_server(self.go, self.source_ip, self.source_port, loop=self.loop)
		self.server = self.loop.run_until_complete(task)
		try:
			self.loop.run_forever()
		except KeyboardInterrupt:
			pass
		finally:
			self.stop()


dicts = {}
server = []

def test_start_http(server, obj):
	source, dest, user, password, type_proxy, dns = obj
	proxy = AsyncHttp(source, dest, user, password)
	serv.append(proxy)
	proxy.run()


def start_http(obj, server=server):
	source, dest, user, password, type_proxy, dns = obj
	proxy = AsyncHttp(source, dest, user, password, dns)
	server.append(proxy)
	logging.info("DNS: %s", dns)
	proxy.run()



def start_http_auth(obj, server=server):
	source, dest, user, password, type_proxy, dns = obj
	proxy = AsyncHttpAuth(source, dest, user, password, dns)
	server.append(proxy)
	logging.info("DNS: %s", dns)
	proxy.run()



def start_socks4(obj, server=server):
	source, dest, user, password, type_proxy, dns = obj
	proxy = AsyncSocks4(source, dest, user, password, dns)
	server.append(proxy)
	logging.info("DNS: %s", dns)
	proxy.run()


def start_socks5(obj, server=server):
	source, dest, user, password, type_proxy, dns = obj
	proxy = AsyncSocks5(source, dest, user, password, dns)
	server.append(proxy)
	logging.info("DNS: %s", dns)
	proxy.run()


def main(*args):
	source, dest, user, password, dns = args
	start_socks5(source, dest, user, password, dns)


if __name__ == '__main__':
	import sys
	from threading import Thread
	sock_list = []
	with open("socks4.txt") as f:
		for ip in f:
			sock_list.append(ip.strip())

	_sock = [(('0.0.0.0', 1026+num), (ip.split(':')[0], int(ip.split(':')[1])), 'admin', 'admin', 'socks4', None) \
			for num, ip in enumerate(sock_list)]

	def test(dic):
		import time
		from threading import Thread

		res = []
		for i in _sock[:5000]:
			if i[4] == 'http':
				th = Thread(target=start_http, args=(i,))
			if i[4] == 'socks4':
				th = Thread(target=start_socks4, args=(i,))
			if i[4] == 'socks5':
				th = Thread(target=start_socks5, args=(i,))

			res.append(th)

		for th in res:
			th.start()
			time.sleep(0.005)
			logging.info("ident %s", th.ident)
			dic[str(th.ident)] = th

		logging.info("всего в словаре %s", len(dic))

		logging.info("спим 2 минуты")
		time.sleep(60*60)
		logging.info("проснулись")
		for i in dic.keys():
			th = dic[i]
			th._tstate_lock = None
			th._stop()
			logging.info("остановка и выход")

		for s in server:
			s.stop()

		logging.info("выход!")

	test(dicts)