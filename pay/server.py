import os
import sys
import time
import uuid
import multiprocessing
from multiprocessing.connection import Listener, AuthenticationError
from threading import Thread
import logging
import resource
import pickle
import signal


from asyncforward import AsyncSocks4, AsyncSocks5, AsyncHttp, AsyncHttpAuth


log = logging.getLogger('back')




class BaseServer:
    def __init__(self, log):
        self.log = log
        self._socks = dict()
        self._proxy_socks = dict()
        self._thread_dict = dict()
        self._type_proxy = dict()
        self._dns = dict()


    def _handle_pay(self):
        self.log.info("сереализация (покупка)")
        f = open("proxy_exit.bin", "wb")
        pickle.dump(self._socks, f)
        f.close()


    def _handle_del_socks(self):
        self.log.info("сереализация (удаление)")
        f = open("proxy_exit.bin", "wb")
        pickle.dump(self._socks, f)
        f.close()


    def handle_exit(self, sig, fb):
        self.log.info("сереализация")
        if self._socks:
            f = open("proxy_exit.bin", "wb")
            pickle.dump(self._socks, f)
            f.close()


    def socks5(self, obj, ident):
        source, dest, user, password, type_proxy, dns = obj
        forward = AsyncSocks5(source, dest, user, password, dns)
        self._proxy_socks[ident] = forward
        forward.run()


    def socks4(self, obj, ident):
        source, dest, user, password, type_proxy, dns = obj
        forward = AsyncSocks4(source, dest, user, password, dns)
        self._proxy_socks[ident] = forward
        forward.run()

    
    def http(self, obj, ident):
        source, dest, user, password, type_proxy, dns = obj
        forward = AsyncHttp(source, dest, user, password, dns)
        self._proxy_socks[ident] = forward
        forward.run()

    
    def http_auth(self, obj, ident):
        source, dest, user, password, userhttp, passwordhttp, type_proxy, dns = obj
        forward = AsyncHttpAuth(source, dest, user, password, userhttp, passwordhttp, dns)
        self._proxy_socks[ident] = forward
        forward.run()


    def _dns_change(self, ident, dns_list):
        forward = server._proxy_socks[ident]
        if isinstance(dns_list, (list, tuple)):
            forward.resolver.nameservers = dns_list
            self.log.info("сменили DNS на %s", forward.source_ip)
            self._dns[ident] = dns_list
        else:
            self.log.info("ошибка смены DNS на %s", dns_list)

    
    def _restart(self, ident):
        try:
            th = self._thread_dict[ident]
            th._tstate_lock = None
            th._stop()
            forward = self._proxy_socks[ident]
            forward.stop()
            obj = self._socks[ident]
            ident = self._start(obj, ident)
            return ident
        except Exception as exc:
            self.log.exception(exc)
            return "ошибка"

    
    def _start(self, obj, ident):
        try:
            source, dest, user, password, type_proxy, userhttp, passwordhttp, dns = obj
            if userhttp and passwordhttp:
                pass
            else:
                obj = source, dest, user, password, type_proxy, dns

        except ValueError:
            source, dest, user, password, type_proxy, dns = obj


        if type_proxy == 'socks5':
            th = Thread(target=self.socks5, args=(obj, ident))
        elif type_proxy == 'socks4':
            th = Thread(target=self.socks4, args=(obj, ident))
        elif type_proxy == 'http':
            th = Thread(target=self.http, args=(obj, ident))
        elif type_proxy == 'http_auth':
            th = Thread(target=self.http_auth, args=(obj, ident))

        th.start()
        time.sleep(5)

        #self._dns[ident] = self._proxy_socks[ident].resolver.nameservers
        #if not self._dns[ident]:
        #    self.log.info("Не успел")
        self._thread_dict[ident] = th
        self._socks[ident] = obj
        self._handle_pay()
        return ident


    def _report(self, ident, dest):
        forward = self._proxy_socks[ident]
        forward._dest_ip = dest[0]
        forward._dest_port = dest[1]
        self.log.info("смена портов")
        self.log.info(forward)
        

    def _kill(self, ident):
        th = self._thread_dict[ident]
        th._tstate_lock = None
        th._stop()
        forward = self._proxy_socks[ident]
        forward.stop()
        del self._proxy_socks[ident]
        del self._thread_dict[ident]
        del self._socks[ident]
        self._handle_del_socks()



class ServerCall(BaseServer):
    def __init__(self, log):
        BaseServer.__init__(self, log)
        #self.log = log
        self.ip = "localhost"
        self.address = (self.ip, 49151)
        self.password = b'password'
        self.server = Listener(self.address, authkey=self.password)
        


    def _bootstrap(self):
        while True:
            try:
                self.log.info("ждем соединения")
                sock = self.server.accept()
                self.log.info("приняли соединения")
                while True:
                    try:
                        obj = sock.recv()
                        self.log.info("%s", obj)
                        
                        if isinstance(obj, (tuple, list)):
                            # перезапуск
                            if obj[0] == "restart":
                                self.log.info("рестарт")
                                self.log.info("%s", obj)
                                ident = obj[1]
                                self.log.info("%s", ident)
                                ident = self._restart(ident)
                                sock.send(ident)
                            
                            # смена портов
                            elif obj[0] == "port":
                                self.log.info("смена портов")
                                ident = obj[1]
                                dest = obj[2]
                                self._report(ident, dest)
                                sock.send("смена портов")

                            # убить
                            elif obj[0] == "kill":
                                self.log.info("остановка")
                                ident = obj[1]
                                self._kill(ident)
                                sock.send(ident)


                            # смена DNS
                            elif obj[0] == "dns":
                                self.log.info("смена DNS")
                                ident = obj[1]
                                dns_list = obj[2]
                                self._dns_change(ident, dns_list)
                                self.log.info("смена DNS: %s", dns_list)
                                sock.send(ident)

                            else:
                                # запустить
                                self.log.info("первый запуск")
                                ident = str(uuid.uuid4())
                                ident = self._start(obj, ident)
                                self.log.info("ident %s", ident)
                                sock.send(ident)
                        sock.close()
                    except EOFError as exc:
                        self.log.exception(exc)
                        break
                    break
            except (OSError, IOError, AuthenticationError, ConnectionResetError) as exc:
                self.log.exception(exc)
                continue
    
    def run(self):
        self._bootstrap()


if __name__ == '__main__':
    if os.path.exists("proxy_exit.bin"):
        statinfo = os.stat('proxy_exit.bin')
        if os.path.exists("proxy_exit.bin") and statinfo.st_size != 0:
            log.info("восстановление и запуск")
            server = ServerCall(log)
            f = open("proxy_exit.bin", "rb")
            obj = pickle.load(f)
            [server._start(obj, ident) for ident, obj in obj.items()]

            signal.signal(signal.SIGTERM, server.handle_exit)
            server.run()

    else:
        log.info("запуск")
        server = ServerCall(log)
        signal.signal(signal.SIGTERM, server.handle_exit)
        server.run()