#-*-coding:utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from pay.models import Order, Refill


User = get_user_model()


class PayForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = ('type_pay','term',)

class RefillForm(forms.ModelForm):
    """Форма пополнения баланса
    """
    class Meta:
        model = Refill
        fields = ('usd',)
        widgets={
            'usd': forms.widgets.NumberInput(attrs={'id': 'name4', 'min': '1'})
        }
        labels = {
            'usd': ''
        }
