from django.contrib import admin
from pay.models import Order, Purses, Refill

class OrderAdmin(admin.ModelAdmin):
    fields = ('amount', 'type_pay', 'term', 'status', 'user')

    #get_client.short_description = 'Покупатель'

admin.site.register(Order, OrderAdmin)


class PursesAdmin(admin.ModelAdmin):
    """Кошельки
    """
    fields = ('purse', 'client', 'busy')
    list_display = ['purse', 'client', 'busy']
    list_display_links = ['purse']

admin.site.register(Purses, PursesAdmin)


class RefillAdmin(admin.ModelAdmin):
    """Заявки на оплату
    """
    fields = ('user', 'purse', 'usd', 'bit', 'truck', 'success')
    list_display = ['user', 'purse', 'usd', 'bit', 'truck', 'created', 'success']
    list_filter = ('user', 'purse', 'created', 'success')
    list_display_links = ['user']
    date_hierarchy = 'created'
    search_fields = ['user', 'purse', 'created']

admin.site.register(Refill, RefillAdmin)
