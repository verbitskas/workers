#-*-coding:utf-8 -*-
from django.conf.urls import url
from pay.views import buy, info, returns, dns, ajax_dns, open_port
from . import ajax

urlpatterns = [
    url(r"buy/(?P<id>\w+)/$", buy, name='buy'),
    url(r"buy/$", info, name="info"),
    url(r"dns/(?P<id>\w+)/$", dns, name="dns"),

    url(r"ajax_dns/$", ajax_dns, name="ajax_dns"),
    url(r"ajax_openport//$", open_port, name="open_port"),

    url(r"ajax_order/$", ajax.ajax_orders, name="ajax_order"),
    url(r"ajax_port/$", ajax.ajax_ports, name="ajax_port"),
    url(r"ajax_search_pay/$", ajax.ajax_search_pay, name="ajax_search_pay"),
    url(r"ajax_buy_proxy/$", ajax.ajax_buy, name="ajax_buy_proxy"),
    url(r"ajax_renewal/$", ajax.ajax_renewal, name="ajax_renewal"),
    url(r"returns/$", ajax.ajax_returns, name='ajax_returns')
]
