#-*-coding:utf-8 -*-
from django.apps import AppConfig


class PayConfig(AppConfig):
    name = 'pay'
    verbose_name = u'Финансы'