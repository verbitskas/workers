from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from django.core import serializers
from rest_framework import viewsets
from django.views.decorators.csrf import csrf_exempt
import json, decimal
import datetime
from django.utils import timezone

from proxy.models import Proxy
from client.models import Port
from pay.models import Order
from pay.models import NotesIpPort, Purses
from prof.forms import NotesIpForm
from .serializers import *
from pay.tasks import backforward, _portopen

from referral.views import partnerka

from django.contrib.auth.decorators import login_required
from celery.task.control import revoke

from django.contrib.auth import get_user_model
User = get_user_model()

from folow_mony.models import HistoryMony, SocksHistory
from folow_mony.views import add_history_mony


@login_required
def ajax_orders(request):
    """
    Ajax запрос список купленых прокси
    """
    if request.method == 'GET':
        snippets = Port.objects.filter(user=request.user, socks__isnull=False)
        serializer = PortSerializer(snippets, many=True)

        form = NotesIpPort.objects.filter(user=request.user, order=snippets)
        notes = NotesIpPortSerializer(form, many=True)
        return JsonResponse({'notes': serializer.data}, safe=False)


@login_required
def ajax_search_pay(request):
    """
    Ajax запрос купленого прокси в поиске
    """
    if request.is_ajax() and request.method == 'GET':
        ids = request.GET.get('id', None)
        if ids:
            ports = Port.objects.filter(user=request.user, id=ids)
            ports_ser = PortSearchPaySerializer(ports, many=True)
            context = {'ports': ports_ser.data}
            return JsonResponse(context, safe=False)


@login_required
@csrf_exempt
def ajax_ports(request):
    """
    Ajax запрос купленого прокси и обработка заметок
    """
    if request.is_ajax() and request.method == 'GET':
        ids = request.GET.get('id', None)
        if ids:
            ports = Port.objects.filter(user=request.user, id=ids)
            ports_ser = PortSerializer(ports, many=True)
            context = {'ports': ports_ser.data}
            return JsonResponse(context, safe=False)

    if request.method == 'POST':
        json_str = ((request.body).decode('utf-8'))
        data = json.loads(json_str)
        for n in data['data']:
            t = data['data']["text"]
            ids = data['data']["id"]
            try:
                note = NotesIpPort.objects.get(user=request.user, order=ids)
                note.text = t
                note.save(update_fields=['text'])
            except:
                note = NotesIpPort()
                note.user = request.user
                note.text = t
                note.order = Port.objects.get(user=request.user, id=ids)
                note.save()
        return JsonResponse({"data": "Заметка записана"})
    return JsonResponse({"data": "Error"})


@login_required
@csrf_exempt
def ajax_buy(request):
    """Покупка прокси"""
    if request.method == 'POST':
        json_str = ((request.body).decode('utf-8'))
        data = json.loads(json_str)
        pk = data['data']["pk"]
        #summ = data['data']["summ"]
        term = data['data']["term"]
        id = pk
        term = int(term)
        if term == 1:
            summ = 2
        elif term == 3:
            summ = 5
        elif term == 7:
            summ = 10
        #summ = int(summ)
        if pk and term and summ:
            client = User.objects.get(id=request.user.id)
            if client.balance >= summ and client.balance != 0:
                # Проверка портнерской программы и зачисления %
                ref = partnerka(request.user, summ)
                if ref:
                    summ = float(summ) - ref["summ"]
                    summ_str = str(summ)
                    dec = decimal.Decimal(summ_str)
                    client.balance -= dec
                    proc = ref["proc"]
                else:
                    client.balance -= summ
                    proc = 0
                client.save()

                user = request.user
                socks = Proxy.objects.get(id=id)
                port = Port(
                    amount=summ,
                    term=term,
                    socks=socks,
                )
                port.user = user
                port.save()
                portid = port.id
                backforward(portid, id, request)
                # История движения средств
                bitcoin = 0
                add_history_mony(request.user, client.balance, summ, proc, bitcoin, "2")
                # История покупок соксов
                ip = Port.objects.get(user=request.user, socks=socks)
                socks_history = SocksHistory()
                socks_history.ip = ip.ipreal
                socks_history.time_by = term
                socks_history.user = user
                socks_history.save()
            else:
                return HttpResponse("Мало денег")
            return JsonResponse({"data": port.id})
        else:
            return HttpResponse("err")


@login_required
@csrf_exempt
def ajax_renewal(request):
    """Продление прокси"""
    if request.method == 'POST':
        json_str = ((request.body).decode('utf-8'))
        data = json.loads(json_str)
        pk = data['data']["pk"]
        #summ = data['data']["summ"]
        term = data['data']["term"]
        id = pk
        term = int(term)
        #summ = int(summ)
        if term == 1:
            summ = 2
        elif term == 3:
            summ = 5
        elif term == 7:
            summ = 10
        if pk and term and summ:
            client = User.objects.get(id=request.user.id)
            if client.balance >= summ and client.balance != 0:
                # Проверка портнерской программы и зачисления %
                ref = partnerka(request.user, summ)
                if ref:
                    summ = float(summ) - ref["summ"]
                    summ_str = str(summ)
                    dec = decimal.Decimal(summ_str)
                    client.balance -= dec
                    proc = ref["proc"]
                else:
                    client.balance -= summ
                    proc = 0
                client.save()

                user = request.user
                socks = Proxy.objects.get(id=id)
                order = Port.objects.get(user=request.user, socks=socks)
                order.amount += dec
                order.term += term
                # order.user = user
                order.save()
                portid = order.id
                backforward(portid, id, request)
                # История движения средств
                bitcoin = 0
                add_history_mony(request.user, client.balance, summ, proc, bitcoin, "3")
                # История покупок соксов
                socks_history = SocksHistory()
                socks_history.ip = order.ipreal
                socks_history.time_by = order.term
                socks_history.user = user
                socks_history.save()
            else:
                return HttpResponse("Мало денег")
            return JsonResponse({"data": "Прокси куплен"})
        else:
            return HttpResponse("err")


@login_required
def ajax_openports(request, id):
    if request.is_ajax() and request.GET:
        try:
            sock = Proxy.objects.get(id=id)
        except Exception:
            return HttpResponse(json.dumps("err"))
        ip = sock.ip
        worker = sock.worker.ip
        res = _portopen.apply_async(args=(ip), queue=worker)
        port = res.get()
        if port:
            return HttpResponse(port)

    return HttpResponse("err")


@login_required
def ajax_returns(request):
    """Возврат сокса
    """
    if request.is_ajax() and request.GET:
        id = request.GET.get('id')
        obj = Port.objects.get(id=int(id))
        ip, port = obj.ipreal.split(":")
        sock = Proxy.objects.get(ipreal=ip, port=port)
        order = Port.objects.get(socks=sock, user=request.user)
        revoke(obj.task, terminate=True)
        # # Возврат денег
        # now =  timezone.now()
        # date = order.created
        # day = now - date
        # hours = day.seconds // 3600
        # summ = order.amount / (order.term * 24)
        # potrach = float(summ) * hours
        # amount = float(order.amount)
        # vozvrat = amount - float(potrach)
        # client = User.objects.get(id=request.user.id)
        # client.balance += decimal.Decimal(str(vozvrat))
        # client.save()
        # # Запись истории
        # add_history_mony(request.user, client.balance, vozvrat, 0, 0, "6")
        order.delete()
        obj.delete()
        return HttpResponse(json.dumps({"status":ip}))
    else:
        return HttpResponse("Error")
