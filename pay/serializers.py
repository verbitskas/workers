from rest_framework import serializers
from django.conf import settings

from pay.models import Order
from proxy.serializers import ProxyPaySerializer, VendorSerializer
from client.models import Port
from pay.models import NotesIpPort

class PortSerializer(serializers.ModelSerializer):
    '''
    Список купленых соксов
    '''
    socks = ProxyPaySerializer()
    orderip = serializers.StringRelatedField(many=True)
    class Meta:
        model = Port
        fields = (
            'id',
            'user',
            'password',
            'worker',
            'port',
            'ipreal',
            'socks',
            'term',
            'created',
            'orderip'
        )

class PortSearchPaySerializer(serializers.ModelSerializer):
    '''
    Купленый сокс, окно в поиске
    '''
    class Meta:
        model = Port
        fields = (
            'id',
            'user',
            'password',
            'worker',
            'port',
            'ipreal'
        )
# class OrderSerializer(serializers.ModelSerializer):
#     '''
#     Список купленых ип
#     '''
#     socks = ProxyPaySerializer()
#     orde = serializers.StringRelatedField(many=True)
#
#     class Meta:
#         model = Order
#         fields = ('id', 'user', 'socks', 'term', 'created', 'orde')


class NotesIpPortSerializer(serializers.ModelSerializer):
    '''
    Форма заметок
    '''

    #order = OrderSerializer()
    class Meta:
        model = NotesIpPort
        fields = ('id', 'user', 'order', 'text')


class SearchOrderSerializer(serializers.ModelSerializer):
    '''
    Список купленых ип
    '''
    socks = ProxyPaySerializer()

    class Meta:
        model = Order
        fields = ('socks',)
