# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import random
import string
from decimal import Decimal
from datetime import datetime, timedelta
from django.utils import timezone
from django.utils.timezone import get_default_timezone
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db import IntegrityError
from django.contrib.auth import get_user_model

from proxy.models import Proxy
#from client.models import Port

User = settings.AUTH_USER_MODEL

TERM = (
    (1, _('Один день')),
    (2, _('Два дня')),
    (3, _('Три дня')),
    (7, _('Неделю')),
    (30, _('Месяц')),
)

@python_2_unicode_compatible
class Order(models.Model):
    STATUS_CHOICES = (
        ('active', _('Активна')),
        ('complete', _('Выполнена')),
        ('cancel', _('Отменена')),
    )

    PAY_CHOICES = (
        ('bitcoin',_('BitCoin')),
        ('webmoney', _('WebMoney')),
        ('perfect', _('Perfect')),
    )
    user = models.ForeignKey(User, verbose_name=_("Покупатель"))
    socks = models.ForeignKey(Proxy, on_delete=models.SET_NULL,
        verbose_name=_("сокс"), related_name='sock', blank=True, null=True)
    amount = models.DecimalField(_('цена'), max_digits=5, decimal_places=2)
    type_pay = models.CharField('Тип валюты', max_length=30, choices=PAY_CHOICES, default='webmoney')
    term = models.IntegerField(_("Срок"),choices=TERM, default=1)
    status = models.CharField('status', max_length=70, choices=STATUS_CHOICES,default='active')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Покупка')
        verbose_name_plural = _('Покупки')
        ordering = ['-created']

    def __str__(self):
        return '%s %s %s usd' % (
            self.user, self.type_pay, self.amount)

    @property
    def term_result(self):
        end = self.created + timedelta(days=int(self.term))
        now = timezone.now()
        total = end - now
        return "Осталось {} дней".format(total.days)


    def get_created(self):
        return self.created.strftime("%d.%m.%y %H:%M")


    def save(self, *args, **kwargs):
        if self.status == 'complete':
            self.user.balance = self.amount
            self.user.save(update_fields=['balance'])

        super(Order, self).save(*args, **kwargs)

class NotesIpPort(models.Model):
    '''Заметки к купленому ИП'''
    user = models.ForeignKey(User, verbose_name=_("пользователь"))
    order = models.ForeignKey('client.Port', on_delete=models.SET_NULL,
        verbose_name=_("купленый ип"), related_name='orderip', null=True)
    text = models.TextField(_("Заметки"), blank=True)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = _("Заметка к IP")
        verbose_name_plural = _("Заметки к IP")


class RandomManager(models.Manager):
    """Менеджер выбора случайной записи
    """
    def get_query_set(self):
        return super(RandomManager, self).get_query_set().order_by('?')


class Purses(models.Model):
    """Кошелькт биткоин для пользователей
    """
    purse = models.CharField(_("Кошелек"), max_length=100)
    client = models.ForeignKey(User, verbose_name=_("Пользователь"), max_length=100, blank=True, null=True)
    busy = models.BooleanField(_("Занят"), default=False)

    objects = models.Manager()
    randoms = RandomManager()

    def __str__(self):
        return self.purse

    class Meta:
        verbose_name = _("Кошелек для пользователя")
        verbose_name_plural = _("Кошельки для пользователей")


class Refill(models.Model):
    """Промежуточная таблица пополнения счета
    """
    user = models.ForeignKey(User, verbose_name=_("пользователь"))
    purse = models.ForeignKey(Purses, verbose_name=_("кошелек"))
    usd = models.IntegerField(_("Сумма USD"), default=0)
    bit = models.FloatField(_("Сумма Bitcoin"), default=0)
    truck = models.FloatField(_("Курс"), default=0)
    balance = models.FloatField(_("Баланс"), default=0)
    created = models.DateTimeField(_("Дата запроса"), auto_now_add=True)
    success = models.BooleanField(_("Оплата подтверждена"), default=False)

    def __str__(self):
        return "Заявки на пополнение"

    class Meta:
        verbose_name = _("Запрос пополнения")
        verbose_name_plural = _("Запросы пополнения")
        ordering = ['-created']
