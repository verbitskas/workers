from django.conf.urls import url

from news.views import news

urlpatterns = [
	url(r'^news/', news, name="index"),
]