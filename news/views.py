from django.shortcuts import render

from news.models import News

def news(request):
	obj = News.objects.all()
	return render(request, "news/news.html", {"news": obj})