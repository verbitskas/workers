from django.db import models
from django.utils.translation import ugettext_lazy as _

class News(models.Model):
	title = models.CharField(_("Тема"), max_length=500)
	text = models.TextField(_("Новости"))

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = _("Новость")
		verbose_name_plural = _("Новости")
