#-*-coding:utf-8 -*-
from django.shortcuts import render
from django.views.generic import ListView, View, DetailView
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.views.decorators.cache import cache_page
from proxy.models import Proxy, ProxyAuth
from client.models import Iplist, Clientlist, IplistBack
from django.http import Http404

class Socks(View):
	template_name = 'proxy/socks.html'
	def get(self, request, *args, **kwargs):
		#context = super(Socks, self).get(request,**kwargs)
		ip = Iplist.objects.get(link=kwargs['name'])
		print(kwargs)
		context['socks'] = ip
		return context

def ipres(request, ip):
	try:
		res = Proxy.objects.get(ip=ip)
	except Proxy.DoesNotExist:
		raise Http404
	return HttpResponse("работает {}".format(res.ip))


#@cache_page(60*5)
def result(request, urladmin):
	try:
		res = Iplist.objects.get(namelist=urladmin)
	except Iplist.DoesNotExist:
		raise Http404
	count_max = res.dicts[1]
	out_real = res.out_real
	proxy = Proxy.objects.filter(res.dicts[0])[:count_max]
	count = proxy.count()
	count_max = min(count_max,count)
	proxy = proxy[:count_max]
	obj = ["{ip}:{port}\n".format(ip=ip.ip,port=ip.port) for ip in proxy]
	return render(
		request, "client/result_list.html",
		{"proxy":obj,"count_max":count_max,"out_real":out_real})

@cache_page(60*5)
def result_auth(request, urladmin):
	try:
		res = IplistAuth.objects.get(namelist=urladmin)
	except Iplist.DoesNotExist:
		raise Http404
	if res.port:
		count_max = len(eval(res.port))
		proxy = ProxyAuth.objects.filter(res.dicts[0])[:count_max]
		count_max = min(len(eval(res.port)),proxy.count())
		lst = ["{}:{} {}".format(res.server,x,p.ipreal) for x,p in \
			zip(eval(res.port),proxy)][:count_max]
		return render(request, "proxy/result_list_auth.html", \
			{"proxy":proxy,'res':res,'ip':lst,'count_max':count_max})
	count_max = res.dicts[1]
	proxy = ProxyAuth.objects.filter(res.dicts[0])[:count_max]
	count_max = min(len(res.total()),proxy.count())
	lst = ["{}:{}".format(res.server,x) for x in res.total()][:count_max]
	return render(request, "client/result_list_auth.html", {"proxy":proxy,'res':res,'ip':lst,'count_max':count_max})

#@cache_page(60*5)
def result_back(request, urladmin):
	try:
		res = IplistBack.objects.get(namelist=urladmin)
		ipreal = res.ipreal
	except IplistBack.DoesNotExist:
		raise Http404
	s = [x.ip for x in res.server.all()]
	if res.out_sniff:
		count_max = res.dicts[1]
		proxy = Proxy.objects.filter(res.dicts[0])[:count_max]
		count_max = min(len(eval(res.port)),proxy.count())
		lst = ["{}:{}".format(s[0],x) for x in res.total()][:count_max]
		out_sniff = res.out_sniff
		return render(request, "client/result_list_auth.html", {
		"proxy":proxy,'res':res,'ip':lst,'count_max':count_max,'sniff':out_sniff, 'ipreal': ipreal})
	if len(s) >= 2:
		count_max = len(eval(res.port))
		proxy = Proxy.objects.filter(res.dicts[0])[:res.dicts[-1]]
		count_max = min(len(eval(res.port)),proxy.count()/2)
		ip1,ip2 = res.server_dicts.keys()
		port1 = eval(res.server_dicts.values()[0])[:count_max]
		port2 = eval(res.server_dicts.values()[1])[:count_max]
		return render(
			request, "proxy/result_list_auth_new.html", 
			{"proxy":proxy,'res':res,'count_max':count_max,
			'ip1':ip1,'ip2':ip2,'val1':port1,'val2':port2, 'ipreal': ipreal})
	count_max = res.dicts[1]
	proxy = Proxy.objects.filter(res.dicts[0])[:count_max]

	obj = ["{ip}".format(ip=ip.ipreal) for ip in proxy]
	
	lst = ["{}:{}".format(s[0],x) for x in res.total()][:count_max]
	
	p = ["{}:{} {}\n".format(s[0],x,y.ipreal) for x,y in zip(res.total(), proxy)][:count_max]

	obj = ["{ip}:{port}\n".format(ip=ip.ip,port=ip.port) for ip in proxy]

	count_max = min(len(eval(res.port)),proxy.count())
	context = {}
	
	#context['pairs'] = zip(lst, obj)
	context['prst'] = p
	#context['proxy'] = proxy
	#context['res'] = res
	#context['ip'] = lst
	#context['count_max'] = count_max
	#context['ipreal'] = ipreal
	return render(request, "client/result_list_auth.html", context)