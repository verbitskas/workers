#-*-coding:utf-8 -*-
from django.conf.urls import url
from client.views import result, ipres, result_auth, result_back


urlpatterns = [
    url(r'(?P<urladmin>\w+\.txt)$', result, name='result'),
    url(r'(?P<urladmin>\w+\.auth)$', result_auth, name='result_auth'),
    url(r'(?P<urladmin>\w+)$', result_back, name='result_back'),
    url(r'(?P<ip>\d{0,255}\.\d{0,255}\.\d{0,255}\.\d{0,255})$', ipres, name='ipres'),
]
