# coding: utf-8
from django.contrib import admin
from django.contrib.auth.models import User

from client.models import Iplist, IplistAuth, IplistBack, ClientlistBack, \
	Clientlist, Server, ClientlistAuth, Do, Port, History
from client.form import ProxyForm, ProxyFormBl
from proxy.models import Proxy, CountryProxy, CityProxy


@admin.register(History)
class AdminHistory(admin.ModelAdmin):
	list_display = ('ipreal', 'add')

@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
	pass

@admin.register(Port)
class AdminPort(admin.ModelAdmin):
	list_display = (
		'ipreal', 'port', 'worker','created',
		'update', 'buyer', 'get_full',
		'term', 'socks_id', 'online',
		)
	list_display_links = ('ipreal',)
	exclude = ('socks',)

@admin.register(Do)
class ServerAdmin(admin.ModelAdmin):
	pass



@admin.register(Iplist)
class AdminIp(admin.ModelAdmin):
	list_display = ('namelist','link')
	fields = (
			('link','pay','order',),
		)

@admin.register(IplistBack)
class AdminIpBack(admin.ModelAdmin):
	list_display = ('namelist','link')
	fields = (
			('link','pay','order',),
		)

@admin.register(IplistAuth)
class AdminIplist(admin.ModelAdmin):
	fields = (
			('link','server','pay','order','portallow'),
		)


from django.core.exceptions import ValidationError

@admin.register(Clientlist)
class AdminIplist(admin.ModelAdmin):
	list_display = ('urladmin','url')
	form = ProxyForm
	fields = (
		('ven','client','paid','urladmin'),
		('country',),
		('timeout','port',),
		('count_max','timelife'),
		('gmt','blacklist','blacklist_check','blacklist_off',"out_real","out_sniff"),)

	class Media:
	#	js = ['admin/js/jquery-ui-1.10.2.custom.js', 'admin/js/mselect-to-mcheckbox.js']

		css = {
			'all': ('css/style.css',)
		} 

	#def get_form(self, request, obj=None, **kwargs):
	#	if obj is None:
	#		"""тут наша кастомная форма"""
	#		return ProxyForm
	#	else:
	#		return super(AdminIplist, self).get_form(request, obj, **kwargs)

@admin.register(ClientlistAuth)
class AdminIplistAuth(admin.ModelAdmin):
	empty_value_display = '-empty-'
	list_display = ('urladmin','ven','server')
	list_display_links = ('urladmin',)
	form = ProxyForm
	fields = (
		('ven','client','paid','urladmin'),
		('server','ip','port'),
		('country',),
		('timeout',),
		('count_max','timelife'),
		('gmt','blacklist','blacklist_check','blacklist_off',"out_sniff"),)

	empty_value_display = 'Весь лист'

	def birth_date_view(self, obj):
		return obj.ven

	birth_date_view.empty_value_display = 'unknown'
	birth_date_view.short_description = u'описание'

	class Media:
	###	js = ['admin/js/jquery-ui-1.10.2.custom.js', 'admin/js/mselect-to-mcheckbox.js']

		css = {
			'all': ('css/style.css',)
		} 

@admin.register(ClientlistBack)
class AdminIplistBack(admin.ModelAdmin):
	empty_value_display = '-empty-'
	list_display = ('urladmin','ven',)
	list_display_links = ('urladmin',)
	form = ProxyFormBl
	fields = (
		('ven','client','paid','urladmin'),
		('server','ip','port'),
		('country',),
		('timeout',),
		('count_max','timelife'),
		('gmt','blacklist','blacklist_check','blacklist_off','auth','out_sniff', 'ipreal'),)

	empty_value_display = 'Весь лист'

	def birth_date_view(self, obj):
		return obj.ven

	birth_date_view.empty_value_display = 'unknown'
	birth_date_view.short_description = u'описание'

	class Media:
	###	js = ['admin/js/jquery-ui-1.10.2.custom.js', 'admin/js/mselect-to-mcheckbox.js']

		css = {
			'all': ('css/style.css',)
		} 