#-*-coding:utf-8 -*-
import uuid
from datetime import datetime, timedelta
import json
import logging
import random
import string

from django.utils import timezone
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.postgres.fields import HStoreField
from django.contrib.postgres.fields import ArrayField
from django.core.urlresolvers import reverse

from picklefield.fields import PickledObjectField

from proxy.models import Proxy, CountryProxy, ProxyAuth, Worker
from vendor.models import Blacklist
from vendor.async_receiver import async_receiver as receiver
from pay.models import Order

from client.logginghandler import JabberHandler

User = settings.AUTH_USER_MODEL



class Do(models.Model):
    ip = models.CharField(_(u"ip"),max_length=100, blank=True, null=True, db_index=True)

    def __str__(self):
        return self.ip

@python_2_unicode_compatible
class Iplist(models.Model):
    namelist = models.CharField(_(u"Название списка"),max_length=100, blank=True, null=True, unique=True)
    link = models.ForeignKey(User,verbose_name=_(u"Список"),blank=True,null=True)
    dicts = PickledObjectField(blank=True, null=True)
    pay = models.BooleanField(_(u"оплачен"), default=False)
    order = models.OneToOneField(Order, verbose_name=_(u'заказ'), blank=True, null=True)
    out_real = models.BooleanField(_(u"вывод репльного ip"),default=False,blank=True)
    out_sniff = models.CharField(_(u"Снифер"),max_length=300, blank=True, null=True)

    black_count = PickledObjectField(blank=True, null=True)


    @property
    def status(self):
        if self.pay:
            return True
        return False

    def __str__(self):
        return "{0}".format(self.link)

    class Meta:
        verbose_name = _(u"Socks лист")
        verbose_name_plural = _(u"Socks листы")


@python_2_unicode_compatible
class IplistAuth(models.Model):
    portallow = models.CharField(_(u"Порт"), max_length=1000,blank=True, null=True)
    server = models.CharField(verbose_name=_(u"Сервер"),max_length=255)
    namelist = models.CharField(_(u"Название списка"),max_length=100, blank=True, null=True, unique=True)
    link = models.ForeignKey(User,verbose_name=_(u"Список"),blank=True,null=True)
    dicts = PickledObjectField(blank=True, null=True)
    pay = models.BooleanField(_(u"оплачен"), default=False)
    order = models.OneToOneField(Order, verbose_name=_(u'заказ'), blank=True, null=True)
    ip = models.CharField(_(u"ip"), blank=True, null=True,max_length=300)
    port = PickledObjectField(blank=True, null=True)
    server_dicts = PickledObjectField(blank=True, null=True)
    out_sniff = models.CharField(_(u"Снифер"),max_length=300, blank=True, null=True)

    @property
    def status(self):
        if self.pay:
            return True

        return False

    def setport(self, x):
        self.port = json.dumps(x)

    def getport(self):
        return json.loads(self.port)

    def return_port(self):
        return eval(self.port)

    def total(self):
        count = ProxyAuth.objects.filter(self.dicts[0]).count()
        num = min(self.dicts[-1],20000)
        num = min(num,count)
        local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
        return local_port

    def __str__(self):
        return "{0}".format(self.link)

    class Meta:
        verbose_name = _(u"Socks лист Auth")
        verbose_name_plural = _(u"Socks листы Auth")


@python_2_unicode_compatible
class IplistBack(models.Model):
    portallow = models.CharField(
        _(u"Порт"),
        max_length=1000,
        blank=True,
        null=True)
    server = models.ManyToManyField('proxy.Worker', verbose_name=_(u"Сервер"))
    namelist = models.CharField(
        _(u"Название списка"),
        max_length=100,
        blank=True,
        null=True,
        unique=True)
    link = models.ForeignKey(
        User,
        verbose_name=_(u"Список"),
        blank=True,
        null=True)
    dicts = PickledObjectField(blank=True, null=True)
    pay = models.BooleanField(_(u"оплачен"), default=False)
    order = models.OneToOneField(
        Order,
        verbose_name=_(u'заказ'),
        blank=True,
        null=True)
    ip = models.TextField(_(u"ip"), blank=True, null=True)
    port = PickledObjectField(blank=True, null=True)
    server_dicts = PickledObjectField(blank=True, null=True)
    auth = models.BooleanField(_(u"Auth?"), default=False)
    out_sniff = models.CharField(
        _(u"Снифер"),
        max_length=300,
        blank=True,
        null=True)
    ipreal = models.BooleanField(_("показывать реальный ip ?"), default=False)
    login_pass = models.BooleanField(default=False)

    @property
    def status(self):
        if self.pay:
            return True
        return False

    def setport(self, x):
        self.port = json.dumps(x)

    def getport(self):
        return json.loads(self.port)

    def return_port(self):
        return eval(self.port)

    def total(self):
        count = Proxy.objects.filter(self.dicts[0]).count()
        num = min(self.dicts[-1],20000)
        num = min(num,count)
        local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
        return local_port

    def __str__(self):
        return "{0}".format(self.link)

    class Meta:
        verbose_name = _(u"Socks лист прямой")
        verbose_name_plural = _(u"Socks листы прямые")



@python_2_unicode_compatible
class IplistBackBack(models.Model):
    portallow = models.CharField(_(u"Порт"), max_length=1000,blank=True, null=True)
    server = models.ManyToManyField('proxy.Worker',verbose_name=_(u"Сервер"))
    namelist = models.CharField(_(u"Название списка"),max_length=100, blank=True, null=True, unique=True)
    link = models.ForeignKey(User,verbose_name=_(u"Список"),blank=True,null=True)
    dicts = PickledObjectField(blank=True, null=True)
    pay = models.BooleanField(_(u"оплачен"), default=False)
    order = models.OneToOneField(Order, verbose_name=_(u'заказ'), blank=True, null=True)
    ip = models.TextField(_(u"ip"), blank=True, null=True)
    port = PickledObjectField(blank=True, null=True)
    server_dicts = PickledObjectField(blank=True, null=True)
    auth = models.BooleanField(_(u"Auth?"),default=False)
    out_sniff = models.CharField(_(u"Снифер"),max_length=300, blank=True, null=True)
    ipreal = models.BooleanField(_("показывать реальный ip ?"), default=False)
    login_pass = models.BooleanField(default=False)
    filter_url = models.CharField(_("Фильтр"), max_length=1000, blank=True, null=True)

    @property
    def status(self):
        if self.pay:
            return True
        return False

    def setport(self, x):
        self.port = json.dumps(x)

    def getport(self):
        return json.loads(self.port)

    def return_port(self):
        return eval(self.port)

    def total(self):
        count = Proxy.objects.filter(self.dicts[0]).count()
        num = min(self.dicts[-1],20000)
        num = min(num,count)
        local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
        return local_port


    def value_unpacked(self):
        return '{value}'.format(value=self.server_dicts.keys())


    value_unpacked.allow_tags = True


    def __str__(self):
        return "{0}".format(self.link)

    class Meta:
        verbose_name = _(u"Socks лист Бек-Бек")
        verbose_name_plural = _(u"Socks листы Бек-Бек")



@python_2_unicode_compatible
class IplistBackBackPassword(models.Model):
    portallow = models.CharField(_(u"Порт"), max_length=1000,blank=True, null=True)
    server = models.ManyToManyField('proxy.Worker',verbose_name=_(u"Сервер"))
    namelist = models.CharField(_(u"Название списка"),max_length=100, blank=True, null=True, unique=True)
    link = models.ForeignKey(User,verbose_name=_(u"Список"),blank=True,null=True)
    dicts = PickledObjectField(blank=True, null=True)
    pay = models.BooleanField(_(u"оплачен"), default=False)
    order = models.OneToOneField(Order, verbose_name=_(u'заказ'), blank=True, null=True)
    ip = models.TextField(_(u"ip"), blank=True, null=True)
    port = PickledObjectField(blank=True, null=True)
    server_dicts = PickledObjectField(blank=True, null=True)
    auth = models.BooleanField(_(u"Auth?"),default=False)
    out_sniff = models.CharField(_(u"Снифер"),max_length=300, blank=True, null=True)
    ipreal = models.BooleanField(_("показывать реальный ip ?"), default=False)
    login_pass = models.BooleanField(default=True)

    @property
    def status(self):
        if self.pay:
            return True
        return False

    def setport(self, x):
        self.port = json.dumps(x)

    def getport(self):
        return json.loads(self.port)

    def return_port(self):
        return eval(self.port)

    def total(self):
        count = Proxy.objects.filter(self.dicts[0]).count()
        num = min(self.dicts[-1],20000)
        num = min(num,count)
        local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
        return local_port

    def __str__(self):
        return "{0}".format(self.link)

    class Meta:
        verbose_name = _(u"Socks лист Бек-Бек с паролями")
        verbose_name_plural = _(u"Socks листы Бек-Бек с паролями")


TP = (
    ('http',_('HTTP')),
    ('https',_('HTTPS')),
    ('socks4',_('SOCKS4')),
    ('socks5',_('SOCKS5'))
)


@python_2_unicode_compatible
class Clientlist(models.Model):
    types = models.CharField(_("Тип саиска"), choices=TP, max_length=10)
    urladmin = models.CharField(_(u"Ссылка"),max_length=255,unique=True,
        help_text=_(u"Допустимые символы a-z, A-Z .txt"))
    client = models.ForeignKey(User, verbose_name=_(u'Покупатель'))
    ven = models.ForeignKey(User,verbose_name=_(u'Продовец'),
        related_name='ven',blank=True,null=True)
    country = models.ManyToManyField(
        CountryProxy,
        verbose_name=_(u"Страна"),
        blank=True,
        help_text=_(u'Удерживайте "Control" (или "Command" на Mac), чтобы выбрать несколько значений'))
    city = models.CharField(_(u"Город"), max_length=255, blank=True, null=True)
    timeout = models.CharField(
        _(u'тайм-аут'),
        max_length=10,
        help_text=_(u'укажите время отклика в секундах,пример 2-5'),
        blank=True,
        null=True)
    port = models.CharField(
        _(u"Порт"),
        help_text=_(u'укажите порт от 0-65535, запись должна иметь вид 45-1024'),
        blank=True,
        null=True,
        max_length=10)
    count_max = models.IntegerField(
        _(u"Колличество"),
        help_text=_(u"укажите колличество соксов"))
    timelife = models.IntegerField(
        _(u"Минуты"),
        help_text=_(u"укажите время жизни сокса в минутах, например 30"),
        blank=True,
        null=True)
    gmt = models.IntegerField(
        _(u'Gmt'),
        blank=True,
        null=True,
        help_text=_(u'Укажите временную зону, например 3 без'))
    blacklist = models.ManyToManyField(
        'vendor.Blacklist',
        verbose_name=_(u'Без Блека'),
        blank=True)

    check_one = models.BooleanField(_('Один блек'), default=False)
    check_two = models.BooleanField(_('Два блека'), default=False)
    check_three = models.BooleanField(_('Три блека'), default=False)
    check_four = models.BooleanField(_('Четыри блека'), default=False)
    check_five = models.BooleanField(_('Пять блеков'), default=False)
    check_six = models.BooleanField(_('Шесть блеков'), default=False)
    check_seven = models.BooleanField(_('Семь блеков'), default=False)

    blacklist_check = models.BooleanField(
        _(u"Точное совподение в блек"),
        default=False)
    blacklist_off = models.BooleanField(_(u"Игнорировать бэк"), default=False)
    out_real = models.BooleanField(
        _(u"вывод репльного ip"),
        default=False,
        blank=True)
    paid = models.BooleanField(_(u'Оплачен'), default=False)
    url = models.URLField(_(u"url листа"))
    out_sniff = models.CharField(
        _(u"Снифер"),
        max_length=300,
        blank=True,
        null=True)

    def __str__(self):
        return (self.urladmin if self.urladmin else self.url)

    def country_list(self):
        link = list(set([x.country for x in Proxy.objects.all()]))
        return link

    country_list.short_description = u'страны'


    def gen_url(self):
        self.url = uuid.uuid1().hex

    def save(self, *args, **kwargs):
        if self.paid or not self.urladmin:
            self.gen_url()
        super(Clientlist, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super(Clientlist, self).delete(*args, **kwargs)
        n = Iplist.objects.get(namelist=self.urladmin)
        n.delete()

    def get_absolute_url(self):
        return reverse('client:result', args=[self.urladmin])

    class Meta:
        verbose_name = _(u'Список')
        verbose_name_plural = _(u'Списки')


@python_2_unicode_compatible
class ClientlistAuth(models.Model):
    server = models.ForeignKey('proxy.Worker', verbose_name=_(u"Сервера"))
    urladmin = models.CharField(
        _(u"Ссылка"),
        max_length=255,
        unique=True,
        help_text=_(u"Допустимые символы a-z, A-Z и .auth в конце как расширение"))
    client = models.ForeignKey(User, verbose_name=_(u'Покупатель'))
    ven = models.ForeignKey(
        User,
        verbose_name=_(u'Продовец'),
        related_name='venauth',
        blank=True,
        null=True)
    country = models.ManyToManyField(
        CountryProxy,
        verbose_name=_(u"Страна"),
        blank=True,
        help_text=_(u'Удерживайте "Control" (или "Command" на Mac), чтобы выбрать несколько значений'))
    ip = models.CharField(
        _(u'IP клиента'),
        max_length=255,
        blank=True,
        null=True)
    port = models.CharField(_(u"Порты"), max_length=1000, blank=True, null=True)
    city = models.CharField(_(u"Город"), max_length=255, blank=True, null=True)
    timeout = models.CharField(
        _(u'тайм-аут'),
        max_length=10,
        help_text=_(u'укажите время отклика в секундах,пример 2-5'),
        blank=True,
        null=True)
    count_max = models.IntegerField(
        _(u"Колличество"),
        help_text=_(u"укажите колличество соксов"))
    timelife = models.IntegerField(
        _(u"Минуты"),
        help_text=_(u"укажите время жизни сокса в минутах, например 30"),
        blank=True,
        null=True)
    gmt = models.IntegerField(
        _(u'Gmt'),
        blank=True,
        null=True,
        help_text=_(u'Укажите временную зону, например 3 без'))
    blacklist = models.ManyToManyField(
        'vendor.Blacklist',
        verbose_name=_(u'Без Блека'),
        blank=True)
    blacklist_check = models.BooleanField(
        _(u"Точное совподение в блек"),
        default=False)
    blacklist_off = models.BooleanField(
        _(u"Игнорировать бэк"),
        default=False)
    paid = models.BooleanField(_(u'Оплачен'), default=False)
    url = models.URLField(_(u"url листа"))
    out_sniff = models.CharField(
        _(u"Снифер"),
        max_length=300,
        blank=True,
        null=True)

    def __str__(self):
        return (self.urladmin if self.urladmin else self.url)

    def country_list(self):
        link = list(set([x.country for x in Proxy.objects.all()]))
        return link

    country_list.short_description = u'страны'

    def gen_url(self):
        self.url = uuid.uuid1().hex

    def delete(self, *args, **kwargs):
        super(ClientlistAuth, self).delete(*args, **kwargs)
        n = IplistAuth.objects.get(namelist=self.urladmin)
        n.delete()

    def get_absolute_url(self):
        return reverse('client:result_auth', args=[self.urladmin])

    class Meta:
        verbose_name = _(u'Бек Auth')
        verbose_name_plural = _(u'Беки Auth')



@python_2_unicode_compatible
class ClientlistBack(models.Model):
    server = models.ForeignKey('proxy.Worker', verbose_name=_(u"Сервера"))
    #server = models.ManyToManyField('proxy.Worker', verbose_name=_(u"Сервера"))
    urladmin = models.CharField(_(u"Ссылка"),max_length=255,unique=True,
        help_text=_(u"Допустимые символы a-z, A-Z и в конце как расширение"))
    client = models.ForeignKey(User,verbose_name=_(u'Покупатель'))
    ven = models.ForeignKey(User,verbose_name=_(u'Продовец'), related_name='venback',blank=True,null=True)
    country = models.ManyToManyField(
        CountryProxy,verbose_name=_(u"Страна"),blank=True,
        help_text=_(u'Удерживайте "Control" (или "Command" на Mac), чтобы выбрать несколько значений'))
    ip = models.TextField(_(u'IP клиента'),blank=True, null=True)
    port = models.CharField(_(u"Порты"), max_length=1000, blank=True, null=True)
    city = models.CharField(_(u"Город"),max_length=255,blank=True,null=True)
    timeout = models.CharField(_(u'тайм-аут'),max_length=10,
        help_text=_(u'укажите время отклика в секундах,пример 2-5'),blank=True, null=True)
    count_max = models.IntegerField(_(u"Колличество"),
        help_text=_(u"укажите колличество соксов"))
    timelife = models.IntegerField(_(u"Минуты"),
        help_text=_(u"укажите время жизни сокса в минутах, например 30"),
        blank=True,null=True)
    gmt = models.IntegerField(_(u'Gmt'),blank=True, null=True,
        help_text=_(u'Укажите временную зону, например 3 без'))
    blacklist = models.ManyToManyField('vendor.Blacklist',verbose_name=_(u'Без Блека'), blank=True)
    blacklist_check = models.BooleanField(_(u"Точное совподение в блек"),default=False)
    blacklist_off = models.BooleanField(_(u"Игнорировать бэк"),default=False)
    paid = models.BooleanField(_(u'Оплачен'),default=False)
    url = models.URLField(_(u"url листа"))
    auth = models.BooleanField(_(u"Auth?"),default=False)
    out_sniff = models.CharField(_(u"Снифер"),max_length=300, blank=True, null=True)
    ipreal = models.BooleanField(_("показывать реальный ip ?"), default=False)

    def __str__(self):
        return (self.urladmin if self.urladmin else self.url)

    def country_list(self):
        link = list(set([x.country for x in Proxy.objects.all()]))
        return link

    country_list.short_description = u'страны'

    def gen_url(self):
        self.url = uuid.uuid1().hex

    def delete(self, *args, **kwargs):
        super(ClientlistBack, self).delete(*args, **kwargs)
        n = IplistBack.objects.get(namelist=self.urladmin)
        n.delete()

    def get_absolute_url(self):
        return reverse('client:result_back', args=[self.urladmin])

    class Meta:
        verbose_name = _(u'Бек прямой')
        verbose_name_plural = _(u'Беки Прямые')


@python_2_unicode_compatible
class ClientlistBackBack(models.Model):
    server = models.ForeignKey('proxy.Worker', verbose_name=_(u"Сервер"))
    urladmin = models.CharField(_(u"Ссылка"),max_length=255,unique=True,
        help_text=_(u"Допустимые символы a-z, A-Z и в конце как расширение"))
    filter_url = models.CharField(_("Фильтр"), max_length=1000, blank=True, null=True)
    client = models.ForeignKey(User,verbose_name=_(u'Покупатель'))
    ven = models.ForeignKey(User,verbose_name=_(u'Продовец'), related_name='venbackback',blank=True,null=True)
    forward = models.ForeignKey(User, verbose_name=_('через лист'), related_name='forward', blank=True, null=True)
    country = models.ManyToManyField(
        CountryProxy,verbose_name=_(u"Страна"),blank=True,
        help_text=_(u'Удерживайте "Control" (или "Command" на Mac), чтобы выбрать несколько значений'))
    ip = models.TextField(_(u'IP клиента'),blank=True, null=True)
    port = models.CharField(_(u"Порты"), max_length=1000, blank=True, null=True)
    city = models.CharField(_(u"Город"),max_length=255,blank=True,null=True)
    timeout = models.CharField(_(u'тайм-аут'),max_length=10,
        help_text=_(u'укажите время отклика в секундах,пример 2-5'),blank=True, null=True)
    count_max = models.IntegerField(_(u"Колличество"),
        help_text=_(u"укажите колличество соксов"))
    timelife = models.IntegerField(_(u"Минуты"),
        help_text=_(u"укажите время жизни сокса в минутах, например 30"),
        blank=True,null=True)
    gmt = models.IntegerField(_(u'Gmt'),blank=True, null=True,
        help_text=_(u'Укажите временную зону, например 3 без'))
    blacklist = models.ManyToManyField('vendor.Blacklist',verbose_name=_(u'Без Блека'), blank=True)
    blacklist_check = models.BooleanField(_(u"Точное совподение в блек"),default=False)
    blacklist_off = models.BooleanField(_(u"Игнорировать бэк"),default=False)
    paid = models.BooleanField(_(u'Оплачен'),default=False)
    url = models.URLField(_(u"url листа"))
    auth = models.BooleanField(_(u"Auth?"),default=False)
    out_sniff = models.CharField(_(u"Снифер"),max_length=300, blank=True, null=True)
    ipreal = models.BooleanField(_("показывать реальный ip ?"), default=False)
    sort = models.BooleanField(_("сортировка"), default=False)
    count_max_socks = models.IntegerField(_("Колличество соксов посредников"), blank=True, null=True)


    def __str__(self):
        return (self.urladmin if self.urladmin else self.url)

    def country_list(self):
        link = list(set([x.country for x in Proxy.objects.all()]))
        return link

    country_list.short_description = u'страны'

    def gen_url(self):
        self.url = uuid.uuid1().hex

    def delete(self, *args, **kwargs):
        super(ClientlistBackBack, self).delete(*args, **kwargs)
        n = IplistBackBack.objects.get(namelist=self.urladmin)
        n.delete()

    def get_absolute_url(self):
        return reverse('client:result_back', args=[self.urladmin])

    class Meta:
        verbose_name = _(u'Бек через Бек')
        verbose_name_plural = _(u'Беки через Беки')


CHOICES_PORTS = (
    ('1',  _('1 соксов на порт')),
    ('5',  _('5 соксов на порт')),
    ('10', _('10 соксов на порт')),
)


@python_2_unicode_compatible
class ClientlistBackBackPassword(models.Model):
    server = models.ForeignKey('proxy.Worker', verbose_name=_(u"Сервер"))
    port_count = models.CharField(_("Колличество соксов на порт"), max_length=2, choices=CHOICES_PORTS)
    urladmin = models.CharField(_(u"Ссылка"),max_length=255,unique=True,
        help_text=_(u"Допустимые символы a-z, A-Z и в конце как расширение"))
    client = models.ForeignKey(User,verbose_name=_(u'Покупатель'))
    ven = models.ForeignKey(User,verbose_name=_(u'Продовец'), related_name='venbackback_password',blank=True,null=True)
    forward = models.ForeignKey(User, verbose_name=_('через лист'), related_name='forward_password', blank=True, null=True)
    country = models.ManyToManyField(
        CountryProxy,verbose_name=_(u"Страна"),blank=True,
        help_text=_(u'Удерживайте "Control" (или "Command" на Mac), чтобы выбрать несколько значений'))
    ip = models.TextField(_(u'IP клиента'),blank=True, null=True)
    port = models.CharField(_(u"Порты"), max_length=1000, blank=True, null=True)
    city = models.CharField(_(u"Город"),max_length=255,blank=True,null=True)
    timeout = models.CharField(_(u'тайм-аут'),max_length=10,
        help_text=_(u'укажите время отклика в секундах,пример 2-5'),blank=True, null=True)
    count_max = models.IntegerField(_(u"Колличество"),
        help_text=_(u"укажите колличество соксов"))
    timelife = models.IntegerField(_(u"Минуты"),
        help_text=_(u"укажите время жизни сокса в минутах, например 30"),
        blank=True,null=True)
    gmt = models.IntegerField(_(u'Gmt'),blank=True, null=True,
        help_text=_(u'Укажите временную зону, например 3 без'))
    blacklist = models.ManyToManyField('vendor.Blacklist',verbose_name=_(u'Без Блека'), blank=True)
    blacklist_check = models.BooleanField(_(u"Точное совподение в блек"),default=False)
    blacklist_off = models.BooleanField(_(u"Игнорировать бэк"),default=False)
    paid = models.BooleanField(_(u'Оплачен'),default=False)
    url = models.URLField(_(u"url листа"))
    auth = models.BooleanField(_(u"Auth?"),default=False)
    out_sniff = models.CharField(_(u"Снифер"),max_length=300, blank=True, null=True)
    ipreal = models.BooleanField(_("показывать реальный ip ?"), default=False)

    def __str__(self):
        return (self.urladmin if self.urladmin else self.url)

    def country_list(self):
        link = list(set([x.country for x in Proxy.objects.all()]))
        return link

    country_list.short_description = u'страны'

    def gen_url(self):
        self.url = uuid.uuid1().hex

    def delete(self, *args, **kwargs):
        super(ClientlistBackBackPassword, self).delete(*args, **kwargs)
        n = IplistBackBackPassword.objects.get(namelist=self.urladmin)
        n.delete()

    def get_absolute_url(self):
        return reverse('client:result_back_passwd', args=[self.urladmin])

    class Meta:
        verbose_name = _(u'Бек через Бек с паролями')
        verbose_name_plural = _(u'Беки через Беки с паролями')



TERM = (
    (1, _('Один день')),
    (2, _('Два дня')),
    (3, _('Три дня')),
    (7, _('Неделю')),
    (30, _('Месяц')),
)

STATUS_CHOICES = (
        ('active', _('Активна')),
        ('complete', _('Выполнена')),
        ('cancel', _('Отменена')),
    )

PAY_CHOICES = (
        ('bitcoin',_('BitCoin')),
        ('webmoney', _('WebMoney')),
        ('perfect', _('Perfect')),
    )



@python_2_unicode_compatible
class Port(models.Model):
    dns = models.CharField(_("DNS"), max_length=255, blank=True, null=True)
    worker = models.CharField(
        _("Worker"),
        max_length=255,
        blank=True,
        null=True)
    task = models.CharField(_("id"), max_length=255, blank=True, null=True)
    password = models.CharField(
        _(u"пароль"),
        max_length=255,
        blank=True,
        null=True)
    port = models.IntegerField(_(u"Порт"), db_index=True, blank=True, null=True)
    use = models.BooleanField(_(u"используеться ?"), default=False)
    ipreal = models.CharField(
        _(u"реальный ip"),
        max_length=255,
        blank=True,
        null=True)
    created = models.DateTimeField('создан', auto_now_add=True)
    update = models.DateTimeField(_("обновлен"), auto_now=True)
    finish = models.DateTimeField(_("Активен до"), blank=True, null=True)
    socks = models.ForeignKey(
        Proxy,
        on_delete=models.SET_NULL,
        verbose_name=_("сокс"),
        related_name='port_socks',
        null=True)
    amount = models.DecimalField(
        _('цена'),
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True)
    type_pay = models.CharField(
        'Тип валюты',
        max_length=30,
        choices=PAY_CHOICES,
        default='webmoney')
    term = models.IntegerField(_("Срок"), choices=TERM, default=1)
    status = models.CharField(
        'status',
        max_length=70,
        choices=STATUS_CHOICES,
        default='active')
    online = models.BooleanField(_('online'), default=True)
    user = models.ForeignKey(
        User,
        verbose_name=_("Покупатель"),
        blank=True,
        null=True)

    def __str__(self):
        return "{}".format(self.port)

    def get_full(self):
        return "{}:{}@{}:{}".format(
            self.user.get_name_split(),
            self.password,
            self.worker,
            self.port)

    get_full.short_description = 'сокс'

    def buyer(self):
        return "{}".format(self.user)

    buyer.short_description = 'покупатель'

    @property
    def term_result(self):
        end = self.created + timedelta(days=int(self.term))
        now = timezone.now()
        total = end - now
        return "Осталось {} дней".format(total.days)


    def get_created(self):
        return self.created.strftime("%d.%m.%y %H:%M")


    def save(self, *args, **kwargs):
        self.finish = datetime.now() + timedelta(days=self.term)
        
        if self.status == 'complete':
            self.user.balance = self.amount
            self.user.save(update_fields=['balance'])

        super(Port, self).save(*args, **kwargs)
        try:
            h = History.objects.get(pay="сокс {} - порт {} - бэк {}".format(self.ipreal, self.port, self.worker),
                ipreal=self.ipreal)
        except:
            h = History(pay="сокс {} - порт {} - бэк {}".format(self.ipreal, self.port, self.worker),
                ipreal=self.ipreal)
        h.save()


    class Meta:
        verbose_name = _("Поштучный Бек")
        verbose_name_plural = _("Поштучные Беки")
        unique_together = ("ipreal", "user",)


class Browser(models.Model):
    agent = models.CharField(_('Браузеры'), max_length=1000, db_index=True)


    def __str__(self):
        return self.agent.split(' ')[0]

    class Meta:
        verbose_name = _('Браузер')
        verbose_name_plural = _('Браузеры')



class Headers(models.Model):
    user_agent = models.ForeignKey(Browser, verbose_name=_('Браузер'),
        help_text=_('Веберите один браузер из списка, для подмены вашего реального.'))
    socks = models.ForeignKey(Port, verbose_name=_('заголовок'),
        help_text=_('заголовок для сокса'))

    def __str__(self):
        return self.user_agent.split(' ')[0]

    class Meta:
        verbose_name = _('Хидеры к соксам')
        verbose_name_plural = _('Хидеры к соксам')



class History(models.Model):
    pay = models.CharField(_('начало'), max_length=255)
    end = models.CharField(_('конец'), max_length=255, blank=True, null=True)
    ipreal = models.CharField(_('ipreal'), max_length=255, blank=True, null=True)

    end_pay = models.CharField(_('end_pay'), max_length=255, blank=True, null=True)
    add = models.DateTimeField(_('Создан'), auto_now=True)
    status = models.DateTimeField(_("срок"), null=True)

    def __str__(self):
        return self.ipreal if self.ipreal else "Пусто"


    class Meta:
        verbose_name = _('История')
        verbose_name_plural = _('Истории')



import logging
from django.conf import settings
import time




@python_2_unicode_compatible
class Task(models.Model):
    task = models.CharField(_(u"ID бека"), max_length=255)

    def __str__(self):
        return self.task

    class Meta:
        verbose_name = _(u"id бека")
        verbose_name_plural = _(u"id'ы беков")



class ProxyPay(models.Model):
    client = models.ManyToManyField(User,verbose_name=_(u'Покупатель'))
    proxy = models.ManyToManyField(Proxy, verbose_name=_(u"Сокс"))
    port = models.ManyToManyField(Port, verbose_name=_(u"Порты"))

    class Meta:
        verbose_name = _(u"Оплата")
        verbose_name_plural = _(u"Оплата")


@python_2_unicode_compatible
class Server(models.Model):
    ip = models.CharField(_(u"IP Сервера"),max_length=128,null=True,db_index=True)

    def __str__(self):
        return self.ip

    class Meta:
        ordering = ('-ip',)
        verbose_name = _(u"Сервер")
        verbose_name_plural = _(u"Сервера")






def gen_pass(size=10, chars=string.ascii_letters + string.digits):
    login = ''.join(random.choice(chars) for _ in range(size))
    password = login
    return [login, password]



#from django.db.models.signals import post_save, pre_save, pre_delete, post_delete
from django.db.models import signals

from client.signals import (
    chunks, 
    port_client_delete,                 # Port
    post_client_delete,                 # Clientlist
    post_clientauth_delete,             # ClientlistAuth
    post_clientback_delete,             # ClientlistBack
    post_clientbackback_delete,         # ClientlistBackBack
    post_clientbackbackpassword_delete, # ClientlistBackBackPassword
    #
    do_client_list,                     # Clientlist
    do_client_list_auth,                # ClientlistAuth
    do_client_list_back,                # ClientlistBack
    do_client_list_backback,            # ClientlistBackBack
    do_client_list_backback_password,   # ClientlistBackBackPassword
    )



signals.post_delete.connect(port_client_delete, sender=Port)
signals.pre_delete.connect(post_client_delete, sender=Clientlist)
signals.pre_delete.connect(post_clientauth_delete, sender=ClientlistAuth)
signals.pre_delete.connect(post_clientback_delete, sender=ClientlistBack)
signals.pre_delete.connect(post_clientbackback_delete, sender=ClientlistBackBack)
signals.pre_delete.connect(post_clientbackbackpassword_delete, sender=ClientlistBackBackPassword)
signals.post_save.connect(do_client_list, sender=Clientlist)
signals.post_save.connect(do_client_list_auth, sender=ClientlistAuth)
signals.post_save.connect(do_client_list_back, sender=ClientlistBack)
signals.post_save.connect(do_client_list_backback, sender=ClientlistBackBack)
signals.post_save.connect(do_client_list_backback_password, sender=ClientlistBackBackPassword)

