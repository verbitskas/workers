import json
import random
from datetime import datetime, timedelta

from django.utils import timezone
from django.db.models import Q

import requests

from client.tasks import SendEmailError, KillSock


from client.models import Iplist, IplistBack, IplistBackBack, IplistBackBackPassword, IplistAuth, gen_pass

from proxy.models import Proxy, CountryProxy, ProxyAuth, Worker

def chunks(lst, count):
    """Группировка элементов последовательности по count элементов"""
    start = 0
    for i in range(count):
        stop = start + len(lst[i::count])
        yield lst[start:stop]
        start = stop



def port_client_delete(sender, instance, signal, *args, **kwargs):
    ident = instance.task
    queue = instance.worker
    KillSock().apply_async(args=(ident,), queue=queue)


def post_client_delete(sender, instance, signal, *args, **kwargs):
    n = Iplist.objects.get(namelist=instance.urladmin)
    n.delete()


def post_clientauth_delete(sender, instance, signal, *args, **kwargs):
    try:
        n = IplistAuth.objects.get(namelist=instance.urladmin)
        n.delete()
    except IplistAuth.DoesNotExist:
        return


def post_clientback_delete(sender, instance, signal, *args, **kwargs):
    try:
        n = IplistBack.objects.get(namelist=instance.urladmin)
        n.delete()
    except IplistBack.DoesNotExist:
        return


def post_clientbackback_delete(sender, instance, signal, *args, **kwargs):
    try:
        n = IplistBackBack.objects.get(namelist=instance.urladmin)
        n.delete()
    except IplistBackBack.DoesNotExist:
        return


def post_clientbackbackpassword_delete(sender, instance, signal, *args, **kwargs):
    try:
        n = IplistBackBackPassword.objects.get(namelist=instance.urladmin)
        n.delete()
    except IplistBackBackPassword.DoesNotExist:
        return


def do_client_list(sender, instance, signal, *args, **kwargs):
    # здесь генерируем ссылку на словарь из параметров kwargs['instance']
    # и создаем dicts с готовыми запросами к БД, так как потом будет проще
    # выводить проверенные соксы, и не проверять ее на онлайн каждый раз.
    qs = Q()
    dicts = {}
    obj = instance
    coun = [x.country for x in obj.country.all()]
    black = [x.title for x in obj.blacklist.all()]
    black_count = []

    if  obj.check_one:
        black_count.append(1)
        
    if obj.check_two:
        black_count.append(2)
    
    if obj.check_three:
        black_count.append(3)
    
    if obj.check_four:
        black_count.append(4)
    
    if obj.check_five:
        black_count.append(5)
    
    if obj.check_six:
        black_count.append(6)
    
    if obj.check_seven:
        black_count.append(7)

    if len(coun) > 0:
        dicts.update(country__in=coun)

    if obj.timeout:
        start,end = obj.timeout.split("-")[0],obj.timeout.split("-")[-1]
        dicts.update(timeout__range=(int(start), int(end)))

    if obj.port:
        start,end = obj.port.split("-")[0],obj.port.split("-")[-1]
        dicts.update(port__range=(int(start), int(end)))

    if obj.gmt:
        dicts.update(gmt=obj.gmt)
    if obj.timelife:
        dicts.update(update__gte=timezone.now() - datetime.timedelta(minutes=obj.timelife))
    else:
        dicts.update(checkers=True)

    if obj.ven:
        dicts.update(vendor_id=obj.ven.id)
    
    if obj.out_real:
        out_real = True
    else:
        out_real = False
    
    count_max = obj.count_max
    if obj.blacklist_check:
        if len(black) >= 2:
            s1 = " ".join([x for x in black])
        else:
            s1 = black[0]

        dicts.update(blacklist=s1)
    else:
        if len(black) >= 2:
            q = Q()
            [qs.add(Q(['%s' % "blacklist__icontains", v]),'OR') for v in black]

        elif len(black) == 1:
            dicts.update(blacklist__icontains=black[0])

        elif len(black) == 0:
            #dicts.update(blacklist__isnull=True)
            if obj.blacklist_off:
                pass
        else:
            dicts.update(blacklist__isnull=True)

    [qs.add(Q(['%s' % k, v]),'AND') for k,v in dicts.items()]

    try:
        ip = Iplist.objects.get(namelist=obj.urladmin)
        ip.link = obj.client
        ip.dicts = [qs,count_max]
        ip.black_count = black_count
        ip.pay = False
        ip.order = None
        ip.out_real = out_real
        ip.out_sniff = obj.out_sniff
        ip.save()

    except Iplist.DoesNotExist:
        Iplist.objects.create(
            link=obj.client,
            namelist=obj.urladmin,
            dicts=[qs,count_max],
            black_count=black_count,
            pay=False,
            order=None,
            out_real=out_real,
            out_sniff=obj.out_sniff)

    return (dicts,dir(obj))



def do_client_list_auth(sender, instance, signal, *args, **kwargs):
    # здесь генерируем ссылку на словарь из параметров kwargs['instance']
    # и создаем dicts с готовыми запросами к БД, так как потом будет проще
    # выводить проверенные соксы, и не проверять ее на онлайн каждый раз.
    qs = Q()
    dicts = {}
    obj = instance
    send = sender
    coun = [x.country for x in obj.country.all()]
    black = [x.title for x in obj.blacklist.all()]
    if obj.ip:
        client = obj.ip
    else:
        client = None
    if obj.port:
        port = obj.port
    else:
        port = None
    if len(coun) > 0:
        dicts.update(country__in=coun)
    if obj.timeout:
        start,end = obj.timeout.split("-")[0],obj.timeout.split("-")[-1]
        dicts.update(timeout__range=(int(start), int(end)))
    if obj.gmt:
        dicts.update(gmt=obj.gmt)
    if obj.ven:
        dicts.update(vendor_id=obj.ven.id)
    if obj.timelife:
        dicts.update(update__gte=timezone.now() - datetime.timedelta(minutes=obj.timelife))
    else:
        dicts.update(checkers=True)
    count_max = obj.count_max
    if obj.blacklist_check:
        if len(black) >= 2:
            s1 = " ".join([x for x in black])
        else:
            s1 = black[0]
        dicts.update(blacklist=s1)
    else:
        if len(black) >= 2:
            q = Q()
            [qs.add(Q(['%s' % "blacklist__icontains", v]),'OR') for v in black]
        elif len(black) == 1:
            dicts.update(blacklist__icontains=black[0])
        else:
            dicts.update(blacklist__isnull=True)

    [qs.add(Q(['%s' % k, v]),'AND') for k,v in dicts.items()]
    try:
        ip = IplistAuth.objects.get(namelist=obj.urladmin)
        ip.server = obj.server.ip
        ip.link = obj.client
        ip.dicts = [qs,count_max]
        ip.ip = client
        ip.portallow = port
        ip.pay = False
        ip.order = None
        ip.port = ip.port
        ip.out_sniff = obj.out_sniff
        ip.save()

    except IplistAuth.DoesNotExist:
        ol = IplistAuth.objects.filter(server=obj.server)
        if ol.count() == 0:
            num = min(obj.count_max,32000)
            local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
            prt = json.dumps(local_port)
            IplistAuth.objects.create(
                portallow=port,
                link=obj.client,
                server=obj.server,
                namelist=obj.urladmin,
                dicts=[qs,count_max],
                ip=client,
                port=prt,
                out_sniff=obj.out_sniff,
                pay=False,
                order=None)
            return
        if ol.count() == 1:
            ol = IplistAuth.objects.filter(server=obj.server)
            ol = IplistAuth.objects.filter(server=obj.server)[ol.count()-1]
            n = ol.port
            num = min(obj.count_max,32000)
            local_1 = [x for x in (range(eval(n)[-1]+1, eval(n)[-1] + num +1)) if x != 5432]
            prt1 = json.dumps(local_1)
            IplistAuth.objects.create(
                portallow=port,
                link=obj.client,
                server=obj.server,
                namelist=obj.urladmin,
                dicts=[qs,count_max],
                ip=client,
                port=prt1,
                out_sniff=obj.out_sniff,
                pay=False,
                order=None)
            return
        if ol.count() == 2:
            ol = IplistAuth.objects.filter(server=obj.server)
            ol = IplistAuth.objects.filter(server=obj.server)[ol.count()-1]
            n = ol.port
            num = min(obj.count_max,32000)
            local_1 = [x for x in (range(eval(n)[-1]+1, eval(n)[-1] + num +1)) if x != 5432]
            prt1 = json.dumps(local_1)
            IplistAuth.objects.create(
                portallow=port,
                link=obj.client,
                server=obj.server,
                namelist=obj.urladmin,
                dicts=[qs,count_max],
                ip=client,
                port=prt1,
                out_sniff=obj.out_sniff,
                pay=False,
                order=None)
            return
        if ol.count() == 3:
            ol = IplistAuth.objects.filter(server=obj.server)
            ol = IplistAuth.objects.filter(server=obj.server)[ol.count()-1]
            n = ol.port
            num = min(obj.count_max,32000)
            local_1 = [x for x in (range(eval(n)[-1]+1, eval(n)[-1] + num +1)) if x != 5432]
            prt1 = json.dumps(local_1)
            IplistAuth.objects.create(
                portallow=port,
                link=obj.client,
                server=obj.server,
                namelist=obj.urladmin,
                dicts=[qs,count_max],
                ip=client,
                port=prt1,
                out_sniff=obj.out_sniff,
                pay=False,
                order=None)
            return
        if ol.count() == 4:
            ol = IplistAuth.objects.filter(server=obj.server)
            ol = IplistAuth.objects.filter(server=obj.server)[ol.count()-1]
            n = ol.port
            num = min(obj.count_max,32000)
            local_1 = [x for x in (range(eval(n)[-1]+1, eval(n)[-1] + num +1)) if x != 5432]
            prt1 = json.dumps(local_1)
            IplistAuth.objects.create(
                portallow=port,
                link=obj.client,
                server=obj.server,
                namelist=obj.urladmin,
                dicts=[qs,count_max],
                ip=client,
                port=prt1,
                out_sniff=obj.out_sniff,
                pay=False,
                order=None)
            return
    return (dicts,send)



def do_client_list_back(sender, instance, signal, *args, **kwargs):
    # здесь генерируем ссылку на словарь из параметров kwargs['instance']
    # и создаем dicts с готовыми запросами к БД, так как потом будет проще
    # выводить проверенные соксы, и не проверять ее на онлайн каждый раз.
    qs = Q()
    dicts = {}
    obj = instance
    send = sender
    ipreal = obj.ipreal
    coun = [x.country for x in obj.country.all()]
    #servers = [x.ip for x in obj]
    servers = obj.server
    #raise ValueError(servers)

    black = [x.title for x in obj.blacklist.all()]
    if obj.ip:
        client = obj.ip
    else:
        client = None
    if obj.auth:
        auth = obj.auth
    else:
        auth = False
    if obj.port:
        port = obj.port
    else:
        port = None
    if len(coun) > 0:
        dicts.update(country__in=coun)
    if obj.timeout:
        start,end = obj.timeout.split("-")[0],obj.timeout.split("-")[-1]
        dicts.update(timeout__range=(int(start), int(end)))
    else:
        dicts.update(checkers=True)
    if obj.gmt:
        dicts.update(gmt=obj.gmt)
    if obj.ven:
        dicts.update(vendor_id=obj.ven.id)
    if obj.timelife:
        dicts.update(update__gte=timezone.now() - datetime.timedelta(minutes=obj.timelife))
    count_max = obj.count_max
    if obj.blacklist_check:
        if len(black) >= 2:
            s1 = " ".join([x for x in black])
        else:
            s1 = black[0]
        dicts.update(blacklist=s1)
    else:
        if len(black) >= 2:
            q = Q()
            [qs.add(Q(['%s' % "blacklist__icontains", v]),'OR') for v in black]
        elif len(black) == 1:
            dicts.update(blacklist__icontains=black[0])
        elif len(black) == 0:
            if obj.blacklist_off:
                pass
        else:
            dicts.update(blacklist__isnull=True)

    [qs.add(Q(['%s' % k, v]),'AND') for k,v in dicts.items()]
    num = min(obj.count_max,32000)
    local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
    prt = json.dumps(local_port)
    
    server_dicts = {}
    num = min(obj.count_max,32000)
    local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
    prt = json.dumps(local_port)
    server_dicts[servers.ip] = prt

    try:
        ip = IplistBack.objects.get(namelist=obj.urladmin)
        ip.link = obj.client
        ip.dicts = [qs,count_max]
        ip.server_dicts = server_dicts
        ip.ip = client
        ip.portallow = port
        ip.pay = False
        ip.port = prt
        ip.order = None
        ip.auth = auth
        ip.out_sniff = obj.out_sniff
        ip.ipreal = ipreal
        ip.save()

        ser1 = Worker.objects.get(ip=servers.ip)
        ip.server.add(ser1)
    
    except IplistBack.DoesNotExist:
        i = IplistBack(
            portallow=port,
            port=prt,
            link=obj.client,
            namelist=obj.urladmin,
            dicts=[qs,count_max],
            server_dicts=server_dicts,
            ip=client,
            pay=False,
            out_sniff=obj.out_sniff,
            order=None,
            ipreal=ipreal,
            auth=auth)

        i.save()
        ser1 = Worker.objects.get(ip=servers.ip)
        i.server.add(ser1)
    return (dicts,send)




def do_client_list_backback(sender, instance, signal, *args, **kwargs):
    # здесь генерируем ссылку на словарь из параметров kwargs['instance']
    # и создаем dicts с готовыми запросами к БД, так как потом будет проще
    # выводить проверенные соксы, и не проверять ее на онлайн каждый раз.
    qs = Q()
    dicts = {}
    obj = instance
    send = sender
    ipreal = obj.ipreal
    coun = [x.country for x in obj.country.all()]
    serv = obj.server
    black = [x.title for x in obj.blacklist.all()]
    
    if obj.forward:
        forward = obj.forward
    else:
        forward = None
    if obj.count_max_socks:
        count_max_socks = obj.count_max_socks
    else:
        count_max_socks = False
    if obj.ip:
        client = obj.ip
    else:
        client = None
    if obj.auth:
        auth = obj.auth
    else:
        auth = False
    if obj.port:
        port = obj.port
    else:
        port = None
    if len(coun) > 0:
        dicts.update(country__in=coun)
    if obj.timeout:
        start,end = obj.timeout.split("-")[0],obj.timeout.split("-")[-1]
        dicts.update(timeout__range=(int(start), int(end)))
    else:
        dicts.update(checkers=True)
    if obj.gmt:
        dicts.update(gmt=obj.gmt)
    if obj.ven:
        dicts.update(vendor_id=obj.ven.id)
    if obj.timelife:
        dicts.update(update__gte=timezone.now() - datetime.timedelta(minutes=obj.timelife))
    count_max = obj.count_max
    if obj.blacklist_check:
        if len(black) >= 2:
            s1 = " ".join([x for x in black])
        else:
            s1 = black[0]
        dicts.update(blacklist=s1)
    else:
        if len(black) >= 2:
            q = Q()
            [qs.add(Q(['%s' % "blacklist__icontains", v]),'OR') for v in black]
        elif len(black) == 1:
            dicts.update(blacklist__icontains=black[0])
        elif len(black) == 0:
            if obj.blacklist_off:
                pass
        else:
            dicts.update(blacklist__isnull=True)

    [qs.add(Q(['%s' % k, v]),'AND') for k,v in dicts.items()]
    num = min(obj.count_max,32000)
    local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
    prt = json.dumps(local_port)


    if obj.filter_url:
        filter_url = requests.get("http://hu-li.net/socks/logs/s1.txt").text.split()
        proxy_list = [(x.ip, x.port) for x in Proxy.objects.filter(qs) if x.ip in filter_url]
    else:
        proxy_list = [(x.ip, x.port) for x in Proxy.objects.filter(qs)]

    if obj.sort:
        if count_max_socks:
            forward_db = Proxy.objects.filter(checkers=True, vendor=forward, country="United States")[:count_max_socks]
            forward_save = [(x.ip, x.port) for x in forward_db]
            forward_list = forward_save * (len(proxy_list) // count_max_socks)
            forward_list.sort()
        else:
            forward_db = Proxy.objects.filter(checkers=True, vendor=forward)
            forward_list = [(x.ip, x.port) for x in forward_db]
    else:
        if count_max_socks:
            forward_db = Proxy.objects.filter(checkers=True, vendor=forward, country="United States").order_by('-created')[:count_max_socks]
            forward_save = [(x.ip, x.port) for x in forward_db]
            forward_list = forward_save * (len(proxy_list) // count_max_socks)
            forward_list.sort()
        else:
            forward_db = Proxy.objects.filter(checkers=True, vendor=forward).order_by('-created')
            forward_list = [(x.ip, x.port) for x in forward_db]

    #forward_list = [(x.ip, x.port) for x in Proxy.objects.filter(checkers=True, vendor=forward)
    
    server_dicts = {}
    num = min(obj.count_max,32000)
    startup = [x for x in range(1024, num + 1024 + 1) if x != 5432]

    local_port = [x for x in zip(startup, forward_list, proxy_list)]
    prt = json.dumps(local_port)
    ip_address = serv.ip
    server_dicts[ip_address] = prt

    try:
        ip = IplistBackBack.objects.get(namelist=obj.urladmin)
        ip.link = obj.client
        ip.dicts = [qs,count_max]
        #ip.server_dicts = server_dicts
        ip.ip = client
        ip.portallow = port
        ip.pay = False
        ip.port = prt
        ip.order = None
        ip.auth = auth
        ip.out_sniff=obj.out_sniff
        ip.ipreal=ipreal
        ip.filter_url = obj.filter_url
        ip.save()
        ip.server.clear()
        ser1 = Worker.objects.get(ip=ip_address)
        ip.server.add(ser1)
    
    except IplistBackBack.DoesNotExist:
        i = IplistBackBack(
            portallow=port,
            port=prt,
            link=obj.client,
            namelist=obj.urladmin,
            dicts=[qs,count_max],
            server_dicts=server_dicts,
            ip=client,
            pay=False,
            out_sniff=obj.out_sniff,
            order=None,
            ipreal=ipreal,
            auth=auth)
        i.save()
        
        ser1 = Worker.objects.get(ip=ip_address)
        i.server.add(ser1)
    return (dicts,send)




def do_client_list_backback_password(sender, instance, signal, *args, **kwargs):
    # здесь генерируем ссылку на словарь из параметров kwargs['instance']
    # и создаем dicts с готовыми запросами к БД, так как потом будет проще
    # выводить проверенные соксы, и не проверять ее на онлайн каждый раз.
    qs = Q()
    dicts = {}
    obj = instance
    send = sender
    ipreal = obj.ipreal
    coun = [x.country for x in obj.country.all()]
    #serv = [x.ip for x in obj.server.all()]
    serv = obj.server
    black = [x.title for x in obj.blacklist.all()]
    
    if obj.forward:
        forward = obj.forward
    else:
        forward = None

    if obj.ip:
        client = obj.ip
    else:
        client = None
    if obj.auth:
        auth = obj.auth
    else:
        auth = False
    if obj.port:
        port = obj.port
    else:
        port = None
    if len(coun) > 0:
        dicts.update(country__in=coun)
    if obj.timeout:
        start,end = obj.timeout.split("-")[0],obj.timeout.split("-")[-1]
        dicts.update(timeout__range=(int(start), int(end)))
    else:
        dicts.update(checkers=True)
    if obj.gmt:
        dicts.update(gmt=obj.gmt)
    if obj.ven:
        dicts.update(vendor_id=obj.ven.id)
    if obj.timelife:
        dicts.update(update__gte=timezone.now() - datetime.timedelta(minutes=obj.timelife))
    count_max = obj.count_max
    if obj.blacklist_check:
        if len(black) >= 2:
            s1 = " ".join([x for x in black])
        else:
            s1 = black[0]
        dicts.update(blacklist=s1)
    else:
        if len(black) >= 2:
            q = Q()
            [qs.add(Q(['%s' % "blacklist__icontains", v]),'OR') for v in black]
        elif len(black) == 1:
            dicts.update(blacklist__icontains=black[0])
        elif len(black) == 0:
            if obj.blacklist_off:
                pass
        else:
            dicts.update(blacklist__isnull=True)

    [qs.add(Q(['%s' % k, v]),'AND') for k,v in dicts.items()]
    num = min(obj.count_max, 32000)
    
    local_port = [x for x in (range(1024, num + 1024)) if x != 5432]
    prt = json.dumps(local_port)

    proxy_list = [(x.ip, x.port) for x in Proxy.objects.filter(qs)]
    forward_list = [(x.ip, x.port) for x in Proxy.objects.filter(checkers=True, vendor=forward)]

    server_dicts = {}

    number_port = len(proxy_list) // 5 # например 1000 / 5 = 200
    login_pass = [gen_pass() for _ in proxy_list]

    # порты
    startup = [random.randint(1024, num + 1024 + 1) for x in range(number_port)]

    #startup = [x for x in range(1024, num + 1024 + 1) if x != 5432]
    
    total = []

    pre_total = list(zip(login_pass, forward_list, proxy_list))

    pre_total_gen = chunks(pre_total, 5)
    
    for port_ in zip(startup, pre_total_gen):
        c1, x = port_
        for _ in x:
            login = _[0][0]
            password = _[0][1]
            forward_lst = _[1]
            proxy_lst = _[2]
            total.append([login, password, c1, forward_lst, proxy_lst])


    #total = [x for x in zip(login_pass, startup, forward_list, proxy_list)]
    prt = json.dumps(total)
    ip = serv.ip
    server_dicts[ip] = prt

    # генерация поролей
    
    try:
        ip = IplistBackBackPassword.objects.get(namelist=obj.urladmin)
        ip.link = obj.client
        ip.dicts = [qs,count_max]
        #ip.server_dicts = server_dicts
        ip.ip = client
        ip.portallow = port
        ip.pay = False
        ip.port = prt
        ip.order = None
        ip.auth = auth
        ip.out_sniff=obj.out_sniff
        ip.ipreal=ipreal
        ip.save()
        ip.server.clear()
        ser1 = Worker.objects.get(ip=ip)
        ip.server.add(ser1)

    except IplistBackBackPassword.DoesNotExist:
        i = IplistBackBackPassword(
            portallow=port,
            port=prt,
            link=obj.client,
            namelist=obj.urladmin,
            dicts=[qs,count_max],
            server_dicts=server_dicts,
            ip=client,
            pay=False,
            out_sniff=obj.out_sniff,
            order=None,
            ipreal=ipreal,
            auth=auth)
        i.save()
        ser1 = Worker.objects.get(ip=ip)
        i.server.add(ser1)
    return (dicts,send)