# coding: utf-8
from django import forms
from django.forms.extras.widgets import SelectDateWidget
from django.forms.widgets import CheckboxSelectMultiple, SelectMultiple, RadioSelect

from proxy.models import Proxy, CountryProxy, CityProxy, Worker
from vendor.models import Blacklist
from client.models import Clientlist, Server, ClientlistBack, ClientlistAuth


class ProxyForm(forms.ModelForm):
	country = forms.ModelMultipleChoiceField(queryset=CountryProxy.objects.all(),
		widget=forms.CheckboxSelectMultiple(attrs={'size':100}),label=u'Страна',required=False)
	
	#city = forms.ModelChoiceField(queryset=CityProxy.objects.all(), label=u'Город', required=False)
	#blacklist = forms.ModelChoiceField(queryset=Blacklist.objects.all(), label=u'Блек', required=False, empty_label="чистый")
	#port = forms.CharField(required=False)
	#timeout = forms.CharField(required=False)


	class Meta:
		model = Clientlist
		fields = (
			'client','ven','country',
			'timeout','port','paid','urladmin',
			'country','count_max',
			'timelife','gmt','blacklist')

		#widgets = {
		#	'country': forms.CheckboxSelectMultiple(attrs={'size': '400'}),
		#	}

	#def clean(self):
	#	cleaned_data = super(ProxyForm, self).clean()
	#	port = cleaned_data['port']
	#	start,end = port.split("-")[0],port.split("-")[-1]
	#	cleaned_data['port'] = start
	#	cleaned_data['start'] = start
	#	cleaned_data['end'] = end
	#	return cleaned_data

class ProxyFormBl(forms.ModelForm):
	country = forms.ModelMultipleChoiceField(queryset=CountryProxy.objects.all(),
		widget=forms.CheckboxSelectMultiple(attrs={'size':100}),label=u'Страна',required=False)

	server = forms.ModelMultipleChoiceField(queryset=Worker.objects.all(),
		widget=forms.CheckboxSelectMultiple(attrs={'size':100}),label=u'Страна',required=False)


	class Meta:
		model = ClientlistBack
		fields = (
			'client','ven','country','server',
			'timeout','port','paid','urladmin',
			'country','count_max','ip',
			'timelife','gmt','blacklist')