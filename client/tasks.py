import datetime
from multiprocessing.connection import Client, AuthenticationError

from celery import task
from celery.task.base import Task, PeriodicTask 
from celery.exceptions import TimeLimitExceeded, SoftTimeLimitExceeded

from django.core.mail import send_mail
from django.conf import settings

import netifaces


def _get_ip_address():
    for interf in netifaces.interfaces():
        if not interf.startswith('lo'):
            address = netifaces.ifaddresses(interf)
            if netifaces.AF_INET in address:
                for addr in netifaces.ifaddresses(interf)[netifaces.AF_INET]:
                    return addr['addr']




class SendEmailError(Task):
    name = 'send-email-error'

    def send_email_error(self, msg):
        if not isinstance(msg, str):
            log.info("не строка")
            return
        send_mail(
            'ERROR-TASKS',
            msg,
            settings.EMAIL_HOST_USER,
            settings.LIST_OF_EMAIL_RECIPIENTS,
            fail_silently=False,
            )

    def run(self, msg, *args, **kwargs):
        self.send_email_error(msg)
        return True


class KillSock(Task):
    name = "УДИТЬ-СОКС"
    soft_time_limit = 60

    def _sock_kill(self, ident):
        obj = ('kill', '{}'.format(ident))
        try:
            address = ('localhost', 49151)
            password = b"password"
            _sock = Client(address, authkey=password)
            _sock.send(obj)
            data = _sock.recv()
            return data
        except AuthenticationError:
            pass
        except EOFError:
            return
        except ConnectionResetError:
            return


    def run(self, ident, *args, **kwargs):
        try:
            self._sock_kill(ident)
        except SoftTimeLimitExceeded:
            queue = _get_ip_address()
            msg = "Время вышло, Воркер: %s" % queue
            SendEmailError().apply_async(args=(msg,), queue=queue)