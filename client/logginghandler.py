import logging

from sleekxmpp import ClientXMPP



class JabberHandler(logging.Handler):
	def __init__(self, jid, password, reception):
		logging.Handler.__init__(self)
		self.jid = jid
		self.password = password
		self.reception = reception

		class Jabber(ClientXMPP):
			def __init__(self, jid, password):
				ClientXMPP.__init__(self, jid, password)

				self.add_event_handler("session_start", self.session_start)
				self.add_event_handler("message", self.message)
				#self.add_event_handler ("ssl_invalid_cert", self.discard)

			def session_start(self, event):
				self.send_presence()
				self.get_roster()

			def message(self, msg):
				pass

			def close(self):
				pass

			def discard(self, event, cert, direct):
				return

		
		self.jabb = Jabber(self.jid, self.password)
		try:
			self.jabb.connect()
			self.jabb.process(threaded=True)
			self.connected = True
		
		except Exception as exc:
			self.connected = False



	def emit(self, record):
		if self.connected:
			try:
				msg = self.format(record)
				self.jabb.send_message(mto=self.reception, mbody=msg)
			except:
				self.handleError(record)

	def close(self):
		self.jabb.disconnect()
		logging.StreamHandler.close(self)